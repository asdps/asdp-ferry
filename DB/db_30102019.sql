-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `armada_elments_log`;
CREATE TABLE `armada_elments_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `armada_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `armada_log`;
CREATE TABLE `armada_log` (
  `noted` text,
  `form_type` varchar(255) DEFAULT 'Armada',
  `cabang_id` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `item` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `fulluri` text,
  `flag` text,
  `created_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `armada_log_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pelabuhans_log`;
CREATE TABLE `pelabuhans_log` (
  `form_type` varchar(255) DEFAULT 'Pelabuhan',
  `noted` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `item` text,
  `deskripsi` text,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `cabang_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `fulluri` text,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `pelabuhans_log_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `photo_log`;
CREATE TABLE `photo_log` (
  `form_type` varchar(255) DEFAULT 'Photo',
  `noted` text,
  `flag` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `fulluri` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `item` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `cabang_id` varchar(255) DEFAULT NULL,
  `path_file` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `photo_log_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `video_log`;
CREATE TABLE `video_log` (
  `noted` text,
  `form_type` varchar(255) DEFAULT 'Video',
  `flag` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `fulluri` text,
  `deskripsi` text,
  `item` varchar(255) DEFAULT NULL,
  `cabang_id` varchar(255) DEFAULT NULL,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `video_log_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `armada`
ADD COLUMN `status` varchar(255) DEFAULT NULL;

ALTER TABLE `pelabuhans`
ADD COLUMN `status` varchar(255) DEFAULT NULL;

ALTER TABLE `photo`
ADD COLUMN `status` varchar(255) DEFAULT NULL,
ADD COLUMN `item` varchar(255) DEFAULT 1 NULL;

ALTER TABLE `video`
ADD COLUMN `status` varchar(255) DEFAULT NULL,
ADD COLUMN `item` varchar(255) DEFAULT 1 NULL,
ADD COLUMN `filename` text DEFAULT NULL,
DROP COLUMN `name`;



-- 2019-10-30 07:49:39
