
ALTER TABLE `pelabuhans`
ADD COLUMN `deskripsi_zonasi` text DEFAULT NULL;

ALTER TABLE `pelabuhans`
ADD COLUMN `deskripsi_sirkular` text DEFAULT NULL;

ALTER TABLE `pelabuhans`
ADD COLUMN `path_zonasi` text DEFAULT NULL;

ALTER TABLE `pelabuhans`
ADD COLUMN `path_sirkular` text DEFAULT NULL;

ALTER TABLE `pelabuhans_log`
ADD COLUMN `deskripsi_zonasi` text DEFAULT NULL;

ALTER TABLE `pelabuhans_log`
ADD COLUMN `deskripsi_sirkular` text DEFAULT NULL;

ALTER TABLE `pelabuhans_log`
ADD COLUMN `path_zonasi` text DEFAULT NULL;

ALTER TABLE `pelabuhans_log`
ADD COLUMN `path_sirkular` text DEFAULT NULL;
