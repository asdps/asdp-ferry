ALTER TABLE `trans_armada_hasil_foto_log` DROP FOREIGN KEY `trans_armada_hasil_foto_log_ibfk_1`;
ALTER TABLE `trans_armada_hasil_foto_log` DROP FOREIGN KEY `trans_armada_hasil_foto_log_ibfk_2`;

ALTER TABLE `trans_armada_hasil_foto_log` DROP INDEX  `filename`;
ALTER TABLE `trans_armada_hasil_foto_log` DROP INDEX  `fileurl`;

SET FOREIGN_KEY_CHECKS=0;