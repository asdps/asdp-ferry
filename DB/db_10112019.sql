-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `armada_canvas`;
CREATE TABLE `armada_canvas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_armada` int(11) NOT NULL,
  `id_armada_elments` int(11) NOT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url_canvas` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_armada` (`id_armada`),
  KEY `id_armada_elments` (`id_armada_elments`),
  CONSTRAINT `armada_canvas_ibfk_1` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `armada_canvas_ibfk_2` FOREIGN KEY (`id_armada_elments`) REFERENCES `armada_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `armada_canvas_log`;
CREATE TABLE `armada_canvas_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_armada` int(11) NOT NULL,
  `id_armada_elments` int(11) NOT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url_canvas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_armada` (`id_armada`),
  KEY `id_armada_elments` (`id_armada_elments`),
  CONSTRAINT `armada_canvas_log_ibfk_1` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `armada_canvas_log_ibfk_2` FOREIGN KEY (`id_armada_elments`) REFERENCES `armada_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pelabuhans_canvas`;
CREATE TABLE `pelabuhans_canvas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(11) NOT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url_canvas` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `pelabuhans_canvas_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pelabuhans_canvas_log`;
CREATE TABLE `pelabuhans_canvas_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(11) NOT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url_canvas` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `pelabuhans_canvas_log_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_armada_hasil_log`;
CREATE TABLE `trans_armada_hasil_log` (
  `icon_id` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` varchar(255) DEFAULT NULL,
  `id_armada_elments` varchar(255) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` text,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `form_type` varchar(255) DEFAULT 'Hasil_Armada',
  `fulluri` varchar(255) DEFAULT NULL,
  `noted` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-11-07 20:06:37
