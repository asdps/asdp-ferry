-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `armada_file`;
CREATE TABLE `armada_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `fileurl` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `armada_file_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pelabuhans`
ADD COLUMN `foto_drag` text DEFAULT NULL;

ALTER TABLE `pelabuhans`
ADD COLUMN `simetris` text DEFAULT NULL;

ALTER TABLE `pelabuhans_log`
ADD COLUMN `foto_drag` text DEFAULT NULL;

ALTER TABLE `pelabuhans_log`
ADD COLUMN `simetris` text DEFAULT NULL;
-- 2019-11-06 07:46:50
