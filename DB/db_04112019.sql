-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `armada`;
CREATE TABLE `armada` (
  `simetris` text,
  `cabang_id` bigint(20) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pelabuhan_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_user` varchar(20) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `armada_elements`;
CREATE TABLE `armada_elements` (
  `url_canvas` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `armada_id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `deskripsi` text NOT NULL,
  `path_file` varchar(255) DEFAULT NULL,
  `created_user` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `armada_elments_log`;
CREATE TABLE `armada_elments_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `armada_id` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT '1',
  `filename` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `armada_log`;
CREATE TABLE `armada_log` (
  `simetris` text,
  `noted` text,
  `form_type` varchar(255) DEFAULT 'Armada',
  `cabang_id` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `item` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `fulluri` text,
  `flag` text,
  `created_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `armada_log_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pelabuhans`;
CREATE TABLE `pelabuhans` (
  `simetris` text,
  `url_canvas` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cabang_id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_user` varchar(20) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pelabuhans_log`;
CREATE TABLE `pelabuhans_log` (
  `form_type` varchar(255) DEFAULT 'Pelabuhan',
  `noted` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `item` text,
  `deskripsi` text,
  `path_file` text,
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `cabang_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `fulluri` text,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `pelabuhans_log_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_armada_hasil`;
CREATE TABLE `trans_armada_hasil` (
  `status` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT '30',
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` longtext,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` int(10) NOT NULL,
  `id_armada_elments` int(10) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `icon_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_armada` (`id_armada`),
  CONSTRAINT `trans_armada_hasil_ibfk_1` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_armada_hasil_foto`;
CREATE TABLE `trans_armada_hasil_foto` (
  `filename` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `fileurl` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_armada_hasil_foto_log`;
CREATE TABLE `trans_armada_hasil_foto_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` bigint(20) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `fileurl` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filename` (`filename`),
  KEY `fileurl` (`fileurl`),
  CONSTRAINT `trans_armada_hasil_foto_log_ibfk_1` FOREIGN KEY (`filename`) REFERENCES `file` (`id`),
  CONSTRAINT `trans_armada_hasil_foto_log_ibfk_2` FOREIGN KEY (`fileurl`) REFERENCES `file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_armada_hasil_log`;
CREATE TABLE `trans_armada_hasil_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` varchar(255) DEFAULT NULL,
  `id_armada_elments` varchar(255) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` text,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `form_type` varchar(255) DEFAULT 'Armada',
  `fulluri` varchar(255) DEFAULT NULL,
  `noted` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_pelabuhans_hasil`;
CREATE TABLE `trans_pelabuhans_hasil` (
  `simetris` text,
  `status` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT '30',
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` longtext,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(10) NOT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `icon_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `trans_pelabuhans_hasil_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_pelabuhans_hasil_foto`;
CREATE TABLE `trans_pelabuhans_hasil_foto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `fileurl` longtext,
  `trans_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`),
  CONSTRAINT `trans_pelabuhans_hasil_foto_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `trans_pelabuhans_hasil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_pelabuhans_hasil_foto_log`;
CREATE TABLE `trans_pelabuhans_hasil_foto_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `fileurl` text,
  `trans_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_pelabuhans_hasil_log`;
CREATE TABLE `trans_pelabuhans_hasil_log` (
  `cabang_id` varchar(255) DEFAULT NULL,
  `form_type` varchar(255) DEFAULT 'Hasil_Pelabuhan',
  `noted` varchar(255) DEFAULT NULL,
  `fulluri` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` longtext,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(11) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `icon_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `trans_pelabuhans_hasil_log_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `trans_pelabuhans_hasil_sub`;
CREATE TABLE `trans_pelabuhans_hasil_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(10) NOT NULL,
  `id_icon` varchar(255) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `trans_pelabuhans_hasil_sub_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-11-04 09:47:20
