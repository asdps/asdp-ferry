
ALTER TABLE `sub_aspeks_icon`
ADD COLUMN `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `sub_aspeks_icon`
ADD COLUMN `units` text DEFAULT NULL;
