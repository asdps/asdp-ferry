
ALTER TABLE `trans_pelabuhans_hasil`
ADD COLUMN `height` varchar(255) DEFAULT NULL;

ALTER TABLE `trans_pelabuhans_hasil_log`
ADD COLUMN `height` varchar(255) DEFAULT NULL;

ALTER TABLE `trans_armada_hasil`
ADD COLUMN `height` varchar(255) DEFAULT NULL;

ALTER TABLE `trans_armada_hasil_log`
ADD COLUMN `height` varchar(255) DEFAULT NULL;