/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : proj_filepriview

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 11/11/2019 04:08:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for trans_armada_hasil
-- ----------------------------
DROP TABLE IF EXISTS `trans_armada_hasil`;
CREATE TABLE `trans_armada_hasil` (
  `status` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT '30',
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` longtext DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` int(10) NOT NULL,
  `id_armada_elments` int(10) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `icon_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `total_icon` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_armada` (`id_armada`),
  CONSTRAINT `trans_armada_hasil_ibfk_1` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for trans_armada_hasil_log
-- ----------------------------
DROP TABLE IF EXISTS `trans_armada_hasil_log`;
CREATE TABLE `trans_armada_hasil_log` (
  `icon_id` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` varchar(255) DEFAULT NULL,
  `id_armada_elments` varchar(255) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` text DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `form_type` varchar(255) DEFAULT 'Hasil_Armada',
  `fulluri` varchar(255) DEFAULT NULL,
  `noted` text DEFAULT NULL,
  `total_icon` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for trans_pelabuhans_hasil
-- ----------------------------
DROP TABLE IF EXISTS `trans_pelabuhans_hasil`;
CREATE TABLE `trans_pelabuhans_hasil` (
  `simetris` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT '30',
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` longtext DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(10) NOT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `icon_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `total_icon` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `trans_pelabuhans_hasil_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for trans_pelabuhans_hasil_log
-- ----------------------------
DROP TABLE IF EXISTS `trans_pelabuhans_hasil_log`;
CREATE TABLE `trans_pelabuhans_hasil_log` (
  `cabang_id` varchar(255) DEFAULT NULL,
  `form_type` varchar(255) DEFAULT 'Hasil_Pelabuhan',
  `noted` varchar(255) DEFAULT NULL,
  `fulluri` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `id_sub_jenis_aspek` varchar(255) DEFAULT NULL,
  `fileurl` longtext DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pelabuhan` int(11) DEFAULT NULL,
  `id_jenis_aspek` varchar(255) DEFAULT NULL,
  `icon_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pointer_x` varchar(255) DEFAULT NULL,
  `pointer_y` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `aspek` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `total_icon` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `trans_pelabuhans_hasil_log_ibfk_1` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
