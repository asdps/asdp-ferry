<?php
$lang['Categories']="Categories";
$lang['Category']="Category";
$lang['Search']="Search";
$lang['Products']="Products";
$lang['Suppliers']="Suppliers";
$lang['msg_search']="What are your looking for?";
$lang['msg_search_mobile']="Pencarian";
$lang['Menu']="Menu";
$lang['caption_lang']="EN";
$lang['lang_id']="Bahasa";
$lang['lang_en']="English";
$lang['Profile']="Profile";
$lang['My Profile']="My Profile";
$lang['Orders']="Orders";
$lang['Orders List']="Orders List";
$lang['Wishlist']="Wishlist";
$lang['Chat']="Chat";
$lang['Logout']="Logout";
$lang['Sign In / Up']="Sign In / Up";
$lang['msg_sign_in']="Sign in to your account or register new one to have full control over your orders, receive bonuses and more.";
$lang['Sign In']="Sign In";
$lang['New customer']="New customer";
$lang['Register']="Register";
$lang['Cart']="Cart";
$lang['msg_cart']="Your cart is empty, get shopping now!";
$lang['Cart']="Shop";
$lang['Shop']="Shop";
$lang['cap_flag']="EN";
$lang['View Cart']="View Cart";
$lang['Checkout']="Checkout";
$lang['Subtotal']="Subtotal";
$lang['Address Label']="Address Label";
$lang['Address']="Address";
$lang['Recipients Name']="Recipient's Name";
$lang['Province']="Province";
$lang['City']="City";
$lang['District']="District";
$lang['Postal Code']="Postal Code";
$lang['Select']="Select";
$lang['ex : alamat kantor']="ex : office address";
$lang['Fullname']="Fullname";
$lang['Phone Number']="Phone Number";
$lang['Back To Cart']="Back To Cart";
$lang['Continue']="Continue";
$lang['Order Summary']="Order Summary";
$lang['Voucher']="Voucher";
$lang['Shipping']="Shipping";
$lang['Cart Subtotal']="Cart Subtotal";
$lang['Quantity']="Quantity";
$lang['Remove item']="Remove item";
$lang['Back to Shopping']="Back to Shopping";
$lang['Clear Cart']="Clear Cart";
$lang['Update Cart']="Update Cart";
$lang['msg_empty_cart']="Troli Kosong";
$lang['msg_enjoy_shop']="Enjoy Your Shopping";
$lang['Shipping Method']="Shipping Method";
$lang['Back to Address']="Back to Address";
$lang['Payment Method']="Payment Method";
$lang['Pay with Credit Card']="Pay with Credit Card";
$lang['Pay with PayPal']="Pay with PayPal";
$lang['Bank Transfer']="Bank Transfer";
$lang['Payment by Bank Transfer']="Payment by Bank Transfer";
$lang['Back']="Back";
$lang['Please check']="Please check";
$lang['Review Your Order']="Review Your Order";
$lang['msg_store_not_fount']="Store not found";
$lang['Shipping to']="Shipping to";
$lang['Complete Order']="Complete Order";
$lang['Payment Completing']="Payment Completing";
$lang['Name']="Name";
$lang['Phone']="Phone";
$lang['Home']="Home";
$lang['msg_thankyou_order']='<h3 class="card-title">Thank you for your order</h3>
            <p class="card-text">Your order has been placed and will be processed as soon as possible.</p>
            <p class="card-text">Make sure you make note of your order number';
$lang['msg_thankyou_order2']='<p class="card-text">You will be receiving an email shortly with confirmation of your order.</p>';
$lang['msg_failed_payment_cc']='<h3 class="card-title">Oooppsss!</h3>
            <p class="card-text">Sorry, Paying Orders with a Credit Card Failed.</p>
            <p class="card-text"> Your order will not be processed.';
$lang['msg_failed_payment_cc2']='<p class="card-text">
              <u>You can now:</u>
            </p>';
$lang['Go Back Shopping']="Go Back Shopping";
$lang['Track order']="Track order";
$lang['Review']="Review";
$lang['Discuss']="Discuss";
$lang['Payment']="Payment";
$lang['Type a message']="Type a message";
$lang['No Chat']="No Chat";
$lang['Recent']="Recent";
$lang['Me']="Me";
$lang['Customer Services']="Customer Services";
$lang['Contact Us']="Contact Us";
$lang['About Us']="About Us";
$lang['About']="About";
$lang['Terms & Conditions']="Terms & Conditions";
$lang['Buy on']="Buy on";
$lang['All Categories']="All Categories";
$lang['Get In Touch With Us']="Get In Touch With Us";
$lang['Add']="Add";
$lang['Save']="Save";
$lang['Address Name']="Address Label";
$lang['Action']="Action";
$lang['Edit']="Edit";
$lang['Delete']="Delete";
$lang['msg_empty_address']="Your address empty!";
$lang['Go shopping now, add new address']="Go shopping now, add new address";
$lang['Go Shopping']="Go Shopping";
$lang['Date']="Date";
$lang['Status']="Status";
$lang['Total']="Total";
$lang['Order History']="Order History";
$lang['msg_empty_order']="Your order history is empty!";
$lang['msg_go_shopping_to_history_order']="Go shopping now to get order history";
$lang['Pending']="Pending";
$lang['Complete']="Complete";
$lang['Paid']="Paid";
$lang['Detail']="Detail";
$lang['Canceled']="Canceled";
$lang['Rejected']="Rejected";
$lang['Cancle']="Cancel";
$lang['Cancel Order']='Cancel Order';
$lang['Process Order']='Process Order';
$lang['Shipped']="Shipped";
$lang['Process']="Process";
$lang['Order Details']="Order Details";
$lang['Shipment Details']="Shipment Details";
$lang['Shipment']="Shipment";
$lang['Order Status']="Order Status";
$lang['Order No']="Order No";
$lang['Courier']="Courier";
$lang['Confirm Received']="Confirm Received";
$lang['Review Products']="Review Products";
$lang['Product Name']="Product Name";
$lang['Reference from']="Reference from";
$lang['Email']="Email";
$lang['Birthday']="Birthday";
$lang['Gender']="Gender";
$lang['Female']="Female";
$lang['Male']="Male";
$lang['Update']="Update";
$lang['Max Size']="Max Size";
$lang['Password']="Password";
$lang['New']="New";
$lang['Retype']="Retype";
$lang['Old']="Old";
$lang['Old Password']="Old Password";
$lang['New Password']="New Password";
$lang['Password Recovery']="Password Recovery";
$lang['Add to Cart']="Add to Cart";
$lang['msg_wishlist']="Your wishlist is empty!";
$lang['Rating & Review']="Rating & Review";
$lang['msg_fill_new_password']="Please fill the Retype Password same with New Password!";
$lang['Form New Password']="Form New Password";
$lang['View More']="View More";
$lang['Top Categories']="Top Categories";
$lang['View Products']="View Products";
$lang['Featured Products']="Featured Products";
$lang['Sell']="Sell";
$lang['New Products']="New Products";
$lang['Best Seller Products']="Best Seller Products";
$lang['Most Popular Products']="Most Popular Products";
$lang['Popular Brands']="Popular Brands";
$lang['Update Password Successed']="Update Password Successed!";
$lang['Register Success You can signin using your account']=" Register Success! You can signin using your account.";
$lang['Something wrong Please check your data and try again']="Something wrong! Please check your data and try again.";
$lang['Choose One']="Choose One";
$lang['msg_klik_image_to_signin_up']="Click image round to Signin Or Signup";
$lang['Login']="Login";
$lang['As Buyer']="As Buyer";
$lang['As Supplier']="As Supplier";
$lang['As Marketer']="As Marketer";
$lang['Login']="Masuk";
$lang['Buyer']="Buyer";
$lang['Supplier']="Supplier";
$lang['Markter']="Marketer";

$lang['msg_take_to_register']="Registration takes less than a minute";
$lang['msg_login_to_access']="Login to access your account.";
$lang['Remember me']="Remember me";
$lang['Forgot password']="Forgot Password";
$lang['msg_regis_akun']="Registration takes less than a minute but gives you full control over your orders.";
$lang['Forgot your password']="Forgot your password";
$lang['msg_Forgot_password']='<p>Change your password in two easy steps. This helps to keep your new password secure.</p>
            <ol class="list-unstyled">
              <li><span class="text-primary text-medium">1. </span>Fill in your email address below.</li>
              <li><span class="text-primary text-medium">2. </span>We\'ll email you a temporary link to change your password.</li>
            </ol>';
$lang['Enter your email address']="Enter your email address";
$lang['msg_Forgot_password_2']='<small class="form-text text-muted">Type in the email address you used when you registered with <b>Bazaarplace</b>. Then we\'ll email a code to this address.</small>';
$lang['Request Reset Password']="Request Reset Password";
$lang['title_sent_request']="Request Reset Password";
$lang['done']="Done";
$lang['failed']="Failed";
$lang['msg_done_sent_request']='<p class="card-text">
              Your request has been sent to your email, and you will be recieve the link to reset your password.
            </p>
            <p class="card-text">
              We hope you can access your account again.
            </p>
            <h1 class="card-title">Thank you!</h1>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="<?= base_url();?>">Home</a>
            </div>';
$lang['msg_failed_not_regis']='<p class="card-text">
              Your request can\'t to process because your email not registered on <b>Bazaarplace</b>
            </p>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Home</a>
              <a class="btn btn-outline-primary" href="'.base_url('page/login').'">
                <i class="icon-location"></i>&nbsp;Register
              </a>
            </div>';
$lang['msg_failed_somtehing_wrong']='<p class="card-text">
              Your request can\'t to process because something wrong.
            </p>
            <p class="card-text">
              Please try again!
            </p>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Home</a>
              <a class="btn btn-outline-primary" href="'.base_url('page/forgetpassword').'">
                Forgot Password
              </a>
            </div>';
$lang['msg_success_reset']='<p class="card-text">
              Your password has been reset, you can try to login on <b>Bazaarplace</b> again.
              <span>
                This page automatically refresh in 5 seconds.
              </span>
            </p>
            <h3>
              Enjoy Your Shopping - <b>Bazaarplace</b>
            </h3>';
$lang['msg_failed_reset']='<p class="card-text">
              Sorry your request can\'t to process because something wrong.
            </p>
            <p class="card-text">
              Please try again!
              <span>
                This page automatically refresh in 5 seconds.
              </span>
            </p>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Home</a>
              <a class="btn btn-outline-primary" href="'.site_url('page/login').'">
                <i class="icon-location"></i>&nbsp;Register
              </a>
            </div>';
$lang['msg_reset_token_expire']='<h1 class="card-title text-center">Something Wrong!</h1>
          <p>
            This link has been expired.
            <br>
            Please make new request to reset password!
            <br>
            <a class="btn btn-outline-secondary" href="'.site_url('page/forgetpassword').'">Forgot Password</a>
          </p>';
$lang['title_verify_email']='<h3 class="text-center padding-top-2x">Email Verification</h3>
          <p class="text-center">one more step to activated your account<br><i class="text-info">Please wait the e-mail, maybe several time minutes.</i></p>';
$lang['Enter activation code']='Enter activation code';
$lang['caption_verify_email']='Type verification code in the email address you used when you registered with Bazaarplace.';
$lang['Submit']='Submit';
$lang['your email']='your email';
$lang['has been verified']='has been verified';
$lang['you can login using your account']='you can login using your account';
$lang['Terima kasih atas verifikasi email Anda. Silahkan login']='Thank you for verifying your email. Please login this <a href="'.base_url().'page/login">here.</a>';
$lang['Terima kasih atas verifikasi email Anda.']='Thank you for verifying your email.';
$lang['Stok']='Stock';
$lang['Weight']='Weight';
$lang['Sell this product']='Sell this product';
$lang['Description']='Description';
$lang['Reviews']='Reviews';
$lang['cap_Reviews']='Be first, give review this product!';
$lang['cap_Discuss']='Let\'s ask the product on here!';
$lang['Inbox']='Chat';
$lang['You May Also Like']='You May Also Like';
$lang['Showing']='Showing';
$lang['items']='items';
$lang['Sort by']='Sort by';
$lang['LH']='Low - High Price';
$lang['HL']='High - Low Price';
$lang['AZ']='A - Z';
$lang['ZA']='Z - A';
$lang['Product Catalog']='Product Catalog';
$lang['Details']='Details';
$lang['msg_no_order']='Your dont have orders yet!';
$lang['Create Store']='Create Store';
$lang['Store']='Store';
$lang['Withdraw']='Withdraw';
$lang['Withdrawal']='Withdrawal';
$lang['Report']='Report';
$lang['Orders Report']='Orders Report';
$lang['No Orders']='No Orders';
$lang['Price']='Selling Price';
$lang['Store Information']='Store Information';
$lang['Store Name']='Store Name';
$lang['Store URL']='Store URL';
$lang['Brand Name']='Brand Name';
$lang['Store Logo']='Store Logo';
$lang['Store Banner']='Store Banner';
$lang['Save Store']='Save Store';
$lang['Edit Store']='Edit Store';
$lang['Store Description']='Store Description';
$lang['You dont have store']='You dont have store!';
$lang['Save Product']='Save Product';
$lang['Product Description']='Product Description';
$lang['Basic Price']='Basic Price';
$lang['Category Child']='Category Child';
$lang['Sub Category']='Sub Category';
$lang['Category Parent']='Category Parent';
$lang['Product Title']='Product Title';
$lang['Images']='Images';
$lang['Add Product']='Add Product';
$lang['Your store data will show here']='Your store data will show here';
$lang['Your product data will show here']='Your product data will show here';
$lang['Your product is empty!']='Your product is empty!';
$lang['Edit Profile']='Edit Profile';
$lang['Update Profile']='Update Profile';
$lang['Update Password']='Update Password';
$lang['Product']='Product';
$lang['Rek Number']='Account Number';
$lang['Rek Name']='Account Name';
$lang['Bank Name']='Bank Name';
$lang['Foto KTP']='Photo ID Card';
$lang['No KTP']='Number ID Card';
$lang['Nama (Sesuai KTP)']='Name (accroding to ID Card)';
$lang['History']='History';
$lang['msg_no_History']='You don\'t have history withdrawal';
$lang['Request Withdrawal']='Request Withdrawal';
$lang['Your Wallet']='Your Wallet';
$lang['Add Bank Account']='Add Bank Account';
$lang['msg_add_bank']='Add bank account first before request withdrawal';
$lang['Nama Pemilik']='Name Account';
$lang['Withdrawal Amount']='Withdrawal Amount';
$lang['Send Request']='Send Request';
$lang['Profit']='Profit';
$lang['Profit Value']='Profit Value';
$lang['Percent']='Percent';
$lang['Profit Type']='Profit Type';
$lang['Sell Product']='Sell Product';
$lang['Set Profit Value']='Set Profit Value';
$lang['membership']='Membership';
$lang['coming-soon']='Coming Soon!';
$lang['no_offering-membership']='Offering of membership seller unavailable!';
$lang['subscribe']='Subscribe';
$lang['bulan']='Month(s)';
$lang['Membership']='Membership';
$lang['Period']='Period';
$lang['Unpaid']='Unpaid';
$lang['Paid']='Paid';
$lang['Unactived']='Unactived';
$lang['Actived']='Actived';
$lang['Expired']='Expired';
$lang['Confirm']="Confirm";
$lang['free']="Free";
$lang['hot_list']="Hot List";
$lang['msg_no_History_membership']='You don\'t have history';
$lang['waiting approval']='Waiting Approval';
$lang['approved']='Approved';
$lang['approve']='Approve';
$lang['unapproved']='Unapproved';
$lang['confirm_approve']='Are your sure to approve?';
$lang['confirm_unapproved']='Are your sure to reject?';
$lang['Pay with Payment Gatway']='Pay with Payment Gatway';
$lang['msg_limit_product']='Limited adding product, Please Subscribe Membership!';
$lang['msg_limit_hotlist']='Limited adding Hot List!';
$lang['limit_time_received']='Limit Time of Received';
$lang['msg_remove_product_failed']='Failed to remove product';
$lang['confirm_delete']='Are you sure to Delete data?';
$lang['msg_potong_20_percent']='The profit is deducted by 20% for service fees for providers.';
$lang['User Guide']='User Guide';
//---------------------------------------------- link/path file if need change path file when language is change ----------------------------//
$lang['link_img_flag']="assets/frontend/img/flags/GB.png";

$lang['Register Seller']='Register as Seller';
$lang['choose_type_store']='Choose Store Type';
$lang['Registration Seller Successed']='Registration as a Seller is successful';
$lang['Registration Seller Successed not all']='Registration as a Seller is successful, please log in again';
$lang['Registration Seller Failed']='Registration as a Seller failed';

$lang['Opps....']='Oops, Product not found...';
$lang['Opps_cap']='Try other keywords.';
$lang['keterangan']='Note';
$lang['note_caption']='Size: XL, Color: Matte Blue, Variant: A, etc';
$lang['valid for a lifetime']='Valid for a Lifetime!';
$lang['removed_voucher']='Voucher Canceled';
$lang['delete_product']='Product Deleted';
$lang['failed_remove_voucher']='Voucher Failed to Cancel';
$lang['download_csv']="Download Data Product(CSV)";
$lang['upload_csv']="Upload Data Product(CSV)";
$lang['download']="Download";
$lang['upload']="Upload";
$lang['Choose file']="Choose File";
$lang['update']="Update";
$lang['cap_downloadFirstCSV']="Please download first the CSV File for updat data.";
$lang['Foto Buku Tabung']="Photo Passbook";
$lang['Confirmation Payment']="Confirmation Payment";
$lang['Confirmed']="Confirmed";
$lang['Paid']="Paid";
$lang['Unpaid']="Unpaid";
$lang['Payment Status']="Payment Status";
$lang['ListSupplier']="List Supplier";
$lang['ListMarketer']="List Marketer";
$lang['no marketer to sell your product']="No Marketer to Sell Your Product";
$lang['Opps....mkt']='Oops, Marketer not found...';
$lang['Opps....spl']='Oops, Supplier not found...';
?>