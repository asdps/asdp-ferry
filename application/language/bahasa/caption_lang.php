<?php
$lang['Categories']="Kategori";
$lang['Category']="Kategori";
$lang['Search']="Pencarian";
$lang['Products']="Produk";
$lang['Suppliers']="Supplier";
$lang['msg_search']="Apa yang anda cari?";
$lang['msg_search_mobile']="Pencarian";
$lang['Menu']="Menu";
$lang['caption_lang']="ID";
$lang['lang_id']="Bahasa";
$lang['lang_en']="English";
$lang['Profile']="Profil";
$lang['My Profile']="Profil Saya";
$lang['Orders']="Pesanan";
$lang['Orders List']="Daftar Pesanan";
$lang['Wishlist']="Wishlist";
$lang['Chat']="Obrolan";
$lang['Logout']="Keluar";
$lang['Sign In / Up']="Masuk/Daftar";
$lang['msg_sign_in']="Masuk ke akun Anda atau daftarkan yang baru untuk memiliki kontrol penuh atas pesanan Anda, terima bonus dan banyak lagi.";
$lang['Sign In']="Masuk";
$lang['New customer']="Pelanggan baru";
$lang['Register']="Daftar";
$lang['Cart']="Troli";
$lang['msg_cart']="Troli Anda kosong, beli sekarang!";
$lang['Shop']="Belanja";
$lang['cap_flag']="ID";
$lang['View Cart']="Lihat Troli";
$lang['Checkout']="Checkout";
$lang['Subtotal']="Subtotal";
$lang['Address Label']="Label Alamat";
$lang['Address']="Alamat";
$lang['Recipients Name']="Nama Penerima";
$lang['Province']="Provinsi";
$lang['City']="Kota";
$lang['District']="Kecamatan";
$lang['Postal Code']="Kode Pos";
$lang['Select']="Pilih";
$lang['ex : alamat kantor']="ex : alamat kantor";
$lang['Fullname']="Nama Lengkap";
$lang['Phone Number']="Nomor Telepon";
$lang['Back To Cart']="Kembali ke Troli";
$lang['Continue']="Lanjut";
$lang['Order Summary']="Rincian Pesanan";
$lang['Voucher']="Voucher";
$lang['Shipping']="Pengiriman";
$lang['Cart Subtotal']="Subtotal Troli";
$lang['Quantity']="Jumlah";
$lang['Remove item']="Hapus Barang";
$lang['Back to Shopping']="Kembali Belanja";
$lang['Clear Cart']="Bersihkan Troli";
$lang['Update Cart']="Perbarui Troli";
$lang['msg_empty_cart']="Troli Kosong";
$lang['msg_enjoy_shop']="Nikmati Belanja Anda";
$lang['Shipping Method']="Metode Pengiriman";
$lang['Back to Address']="Kembali ke Alamat";
$lang['Payment Method']="Metode Pembayaran";
$lang['Pay with Credit Card']="Pembayaran dengan Kartu Kredit";
$lang['Pay with Visa / Mastercard']="Pembayaran dengan Visa/Mastercard";
$lang['Pay with PayPal']="Pembayaran dengan Paypal";
$lang['Pay with Payment Gatway']="Pembayaran dengan Payment Gateway";
$lang['Bank Transfer']="Transfer Bank";
$lang['Payment by Bank Transfer']="Pembayaran dengan Transfer Bank";
$lang['Back']="Kembali";
$lang['Please check']="Cek Voucher";
$lang['Review Your Order']="Tinjau Kembali Pesanan Anda";
$lang['msg_store_not_fount']="Toko Tidak Ada";
$lang['Shipping to']="Pengiriman ke";
$lang['Complete Order']="Pesanan Selesai";
$lang['Payment Completing']="Selesaikan Pembayaran";
$lang['Name']="Nama";
$lang['Phone']="Telepon";
$lang['Home']="Beranda";
$lang['msg_thankyou_order']='<h3 class="card-title">Terima kasih atas pesanan Anda</h3>
            <p class="card-text">Pesanan Anda telah masuk dan akan diproses sesegera mungkin.</p>
            <p class="card-text">Pastikan Anda mencatat nomor pesanan Anda';
$lang['msg_thankyou_order2']='<p class="card-text">Anda akan segera menerima email dengan konfirmasi pesanan Anda.</p>';
$lang['msg_failed_payment_cc']='<h3 class="card-title">Oooppsss!</h3>
            <p class="card-text">Maaf Pembayaran Pesanan dengan Kartu Kredit Gagal.</p>
            <p class="card-text">Pesanan Anda tidak akan diproses.';
$lang['msg_failed_payment_cc2']='<p class="card-text"> 
              <u>Sekarang Anda dapat:</u></p>';
$lang['Go Back Shopping']="Kembali Belanja";
$lang['Track order']="Lacak Pesanan";
$lang['Review']="Ulasan";
$lang['Discuss']="Diskusi";
$lang['Payment']="Pembayaran";
$lang['Type a message']="Ketik sebuah pesan";
$lang['No Chat']="Tidak ada Obrolan";
$lang['Recent']="Obrolan Terbaru";
$lang['Me']="Saya";
$lang['Customer Services']="Layanan Pelanggan";
$lang['Contact Us']="Hubungi Kami";
$lang['About Us']="Tentang Kami";
$lang['About']="Tentang";
$lang['Terms & Conditions']="Syarat & Ketentuan";
$lang['Buy on']="Beli di";
$lang['All Categories']="Semua Kategori";
$lang['Get In Touch With Us']="Hubungi Kami";
$lang['Add']="Tambah";
$lang['Save']="Simpan";
$lang['Address Name']="Label Alamat";
$lang['Action']="Tindakan";
$lang['Edit']="Ubah";
$lang['Delete']="Hapus";
$lang['msg_empty_address']="Alamat Anda kosong!";
$lang['Go shopping now, add new address']="Belanja Sekarang, dan Tambah Alamat Anda";
$lang['Go Shopping']="Pergi Belanja";
$lang['Date']="Tanggal";
$lang['Status']="Status";
$lang['Total']="Total";
$lang['Order History']="Riwayat Pesanan";
$lang['msg_empty_order']="Riwayat Pesanan Anda Kosong!";
$lang['msg_go_shopping_to_history_order']="Belanja sekarang untuk menambah riwayat pesanan Anda";
$lang['Pending']="Tertunda";
$lang['Complete']="Selesai";
$lang['Paid']="Lunas";
$lang['Detail']="Detail";
$lang['Canceled']="Dibatalkan";
$lang['Rejected']="Ditolak";
$lang['Cancle']="Batalkan";
$lang['Shipped']="Dikirim";
$lang['Process']="Diproses";
$lang['Order Details']="Detail Pesanan";
$lang['Shipment Details']="Detail Pengiriman";
$lang['Shipment']="Pengiriman";
$lang['Order Status']="Status Pesanan";
$lang['Order No']="Nomor Order";
$lang['Courier']="Kurir";
$lang['Confirm Received']="Konfirmasi Sampai";
$lang['Review Products']="Ulasan Produk";
$lang['Product Name']="Nama Produk";
$lang['Reference from']="Referensi dari";
$lang['Email']="Email";
$lang['Birthday']="Tanggal Lahir";
$lang['Gender']="Jenis Kelamin";
$lang['Female']="Perempuan";
$lang['Male']="Laki-laki";
$lang['Update']="Update";
$lang['Max Size']="Maks. Ukuran";
$lang['Password']="Kata Sandi";
$lang['New']="Baru";
$lang['Retype']="Ulangi";
$lang['Old']="Lama";
$lang['Old Password']="Kata Sandi Lama";
$lang['New Password']="Kata Sandi Baru";
$lang['Password Recovery']="Pemulihan Kata Sandi";
$lang['Add to Cart']="Tambah ke Troli";
$lang['msg_wishlist']="Wishlist Anda Kosong!";
$lang['Rating & Review']="Peringkat & Ulasan";
$lang['Form New Password']="Formulir Password Baru";
$lang['msg_fill_new_password']="Tolong isi Ulangi Kata Sandi harus sama dengan Kata Sandi Baru!";
$lang['View More']="Lihat Selengkapnya";
$lang['Top Categories']="Kategori Teratas";
$lang['View Products']="Lihat Produk";
$lang['Featured Products']="Produk Unggulan";
$lang['Sell']="Jual";
$lang['Sell Product']="Jual Produk";
$lang['New Products']="Produk Terbaru";
$lang['Best Seller Products']="Produk Terlaris";
$lang['Most Popular Products']="Produk Terpopuler";
$lang['Popular Brands']="Merek Populer";
$lang['Update Password Successed']="Update Kata Sandi Berhasil!";
$lang['Register Success You can signin using your account']="Pendaftaran Berhasil! Anda dapat sudah masuk menggunakan akun anda.";
$lang['Something wrong Please check your data and try again']="Terjadi kesalahan, Tolong cek kembali data Anda dan coba kembali.";
$lang['Choose One']="Pilih Salah Satu";
$lang['msg_klik_image_to_signin_up']="Klik Gambar untuk Masuk atau Mendaftar";
$lang['As Buyer']="sebagai Buyer";
$lang['As Supplier']="sebagai Supplier";
$lang['As Marketer']="sebagai Marketer";
$lang['msg_take_to_register']="Pendaftaran hanya memerlukan waktu sebentar";
$lang['Login']="Masuk";
$lang['Buyer']="Buyer";
$lang['Supplier']="Supplier";
$lang['Markter']="Marketer";
$lang['Remember me']="Ingat Kata Sandi";
$lang['Forgot password']="Lupa Kata Sandi";
$lang['msg_regis_akun']="Pendaftaran membutuhkan waktu sebentar tetapi memberi Anda kendali penuh atas pesanan Anda.";
$lang['Forgot your password']="Lupa kata sandi Anda";
$lang['msg_Forgot_password']='<p>Ubah kata sandi Anda dalam dua langkah mudah. Ini membantu menjaga keamanan kata sandi baru Anda.</p>
            <ol class="list-unstyled">
              <li><span class="text-primary text-medium">1. </span>Masukan alamat email Anda dibawah ini.</li>
              <li><span class="text-primary text-medium">2. </span>Kami akan mengirim email kepada Anda sebuah alamat link sementara untuk mengubah kata sandi Anda.</li>
            </ol>';
$lang['Enter your email address']="Masukan alamat email Anda";

$lang['msg_Forgot_password_2']='<small class="form-text text-muted">Ketikan alamat email yang Anda gunakan saat mendaftar di <b>Bazaarplace</b>. Maka kami akan mengirimkan email sebuah alamat link sementara ke alamat ini.</small>';
$lang['Request Reset Password']="Permintaan Mengulang Kata Sandi";
$lang['done']="Selesai";
$lang['failed']="Gagal";
$lang['title_sent_request']="Permintaan Mengulang Password";
$lang['msg_done_sent_request']='<p class="card-text">Permintaan Anda sudah dikirimkan ke email Anda, dan Anda akan menerima alamat link untuk mengulang kata sandi Anda.
            </p>
            <p class="card-text">
            	Kami berharap Anda dapat mengakses akun Anda lagi.
            </p>
            <h1 class="card-title">Terima Kasih!</h1>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Beranda</a>
            </div>';
$lang['msg_failed_not_regis']='<p class="card-text"> Permintaan Anda tidak dapat diproses karena email Anda tidak terdaftar di <b>Bazaarplace</b>
            </p>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Beranda</a>
              <a class="btn btn-outline-primary" href="'.base_url('page/login').'">
                <i class="icon-location"></i>&nbsp;Daftar
              </a>
            </div>';
$lang['msg_failed_somtehing_wrong']='<p class="card-text">
				Permintaan Anda tidak dapat diproses karena terjadi kesalahan.
            </p>
            <p class="card-text">
            	Silahkan coba lagi!
            </p>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Beranda</a>
              <a class="btn btn-outline-primary" href="'.base_url('page/forgetpassword').'">
                Lupa Kata Sandi
              </a>
            </div>';
$lang['msg_success_reset']='<p class="card-text">
				Kata Sandi Anda sudah dipulihkan, Anda dapat mencoba masuk ke <b>Bazaarplace</b> again.
              <span>
              	Halaman ini akan automatis refresh dalam 5 detik.
              </span>
            </p>
            <h3>
            	Nikmati Belanja Anda - <b>Bazaarplace</b>
            </h3>';
$lang['msg_failed_reset']='<p class="card-text">
				Maaf permintaan anda tidak dapat di proses karena terjadi kesalahan.
            </p>
            <p class="card-text">
              Silahkan coba lagi!
              <span>
                Halaman ini akan automatis refresh dalam 5 detik.
              </span>
            </p>
            <div class="padding-top-1x padding-bottom-1x">
              <a class="btn btn-outline-secondary" href="'.base_url().'">Beranda</a>
              <a class="btn btn-outline-primary" href="'.site_url('page/login').'">
                <i class="icon-location"></i>&nbsp;Daftar
              </a>
            </div>';
$lang['msg_reset_token_expire']='<h1 class="card-title text-center">Terjadi Kesalahan!</h1>
          <p>
            Link ini sudah tidak berlaku.
            <br>
            Silahkan buat permintaan baru untuk mengulang kata sandi!
            <br>
            <a class="btn btn-outline-secondary" href="'.site_url('page/forgetpassword').'">Lupa Kata Sandi</a>
          </p>';
$lang['title_verify_email']='<h3 class="text-center padding-top-2x">Verifikasi Email</h3>
          <p class="text-center">satu langkah lagi untuk mengaktifkan akun Anda.<br><i>Silahkan tunggu verifikasi email, mungkin beberapa menit.</i></p>';
$lang['Enter activation code']='Masukan kode verifikasi';
$lang['caption_verify_email']='Ketikan kode verifikasi yang ada di alamat email yang didaftarkan di Bazaarplace.';

$lang['Submit']='Submit';
$lang['your email']='email anda';
$lang['has been verified']='telah terverifikasi';
$lang['you can login using your account']='anda dapat masuk menggunakan akun anda sekarang';
$lang['Terima kasih atas verifikasi email Anda. Silahkan login']='Terima kasih atas verifikasi email Anda. Silahkan <a href="'.base_url().'page/login">login.</a>';
$lang['Terima kasih atas verifikasi email Anda.']='Terima kasih atas verifikasi email Anda.';
$lang['Stok']='Stok';
$lang['Weight']='Berat';
$lang['Sell this product']='Jual produk ini';
$lang['Description']='Deskripsi';
$lang['Reviews']='Ulasan';
$lang['cap_Reviews']='Ayo menjadi yang pertama, memberi ulasan produk ini!';
$lang['cap_Discuss']='Ayo tanya produknya disini !';
$lang['Inbox']='Chat';
$lang['You May Also Like']='Anda Mungkin Juga Suka';
$lang['Showing']='Menampilkan';
$lang['items']='barang';
$lang['Sort by']='Sotir berdasarkan';
$lang['LH']='Harga Terendah - Tertinggi';
$lang['HL']='Harga Tertinggi - Terendah';
$lang['AZ']='A - Z';
$lang['ZA']='Z - A';
$lang['Product Catalog']='Katalog Produk';
$lang['Details']='Detail';
$lang['Cancel Order']='Batalkan Pesanan';
$lang['Process Order']='Proses Pesanan';
$lang['msg_no_order']='Anda belum memiliki pesanan masuk!';
$lang['Create Store']='Buat Toko';
$lang['Store']='Toko';
$lang['Withdraw']='Withdraw';
$lang['Withdrawal']='Withdrawal';
$lang['Report']='Laporan';
$lang['Orders Report']='Laporan Pesanan';
$lang['No Orders']='Tidak ada Pesanan';
$lang['Price']='Harga Jual';
$lang['Store Information']='Informasi Toko';
$lang['Store Name']='Nama Toko';
$lang['Store URL']='URL Toko';
$lang['Brand Name']='Nama Merek';
$lang['Store Logo']='Logo Toko';
$lang['Store Banner']='Spanduk Toko';
$lang['Save Store']='Simpan Toko';
$lang['Edit Store']='Ubah Toko';
$lang['Store Description']='Deskripsi Toko';
$lang['You dont have store']='Anda tidak memiliki toko!';
$lang['Save Product']='Simpan Produk';
$lang['Product Description']='Deksripsi Produk';
$lang['Basic Price']='Harga Dasar';
$lang['Category Child']='Anak Kategori';
$lang['Sub Category']='Sub Kategori';
$lang['Category Parent']='Induk Kategori';
$lang['Product Title']='Judul Produk';
$lang['Images']='Gambar';
$lang['Add Product']='Tambah Produk';
$lang['Your store data will show here']='Toko Anda akan muncul disini';
$lang['Your product data will show here']='Produk Anda akan muncul disini';
$lang['Your product is empty!']='Tidak ada Produk!';
$lang['Edit Profile']='Ubah Profil';
$lang['Update Profile']='Update Profil';
$lang['Update Password']='Update Kata Sandi';
$lang['Product']='Produk';
$lang['Rek Number']='Nomor Rek.';
$lang['Rek Name']='Nama Rek.';
$lang['Bank Name']='Nama Bank';
$lang['Foto KTP']='Foto KTP';
$lang['No KTP']='Nomor KTP';
$lang['Nama (Sesuai KTP)']='Nama (Sesuai KTP)';
$lang['History']='Riwayat';
$lang['msg_no_History']='Tidak ada riwayat penarikan';
$lang['Request Withdrawal']='Permintaan Penarikan';
$lang['Your Wallet']='Dompet Anda';
$lang['Add Bank Account']='Tambah Rekening Bank';
$lang['msg_add_bank']='Tambah rekening bank dulu sebelum melakukan permintaan penarikan';
$lang['Nama Pemilik']='Nama Pemilik';
$lang['Withdrawal Amount']='Jumlah Penarikan';
$lang['Send Request']='Kirim Permintaan';
$lang['Profit']='Keuntungan';
$lang['Profit Value']='Nilai Keuntungan';
$lang['Percent']='Persen';
$lang['Profit Type']='Tipe Keuntungan';
$lang['Set Profit Value']='Pasang Nilai Keuntungan';
$lang['membership']='Membership';
$lang['no_offering-membership']='Penawaran membership (penjual) tidak tersedia!';
$lang['coming-soon']='Segera hadir!';
$lang['subscribe']='Berlangganan';
$lang['bulan']='Bulan';
$lang['Membership']='Membership';
$lang['Period']='Periode';
$lang['Unpaid']='Belum dibayar';
$lang['Unactived']='Tidak Aktif';
$lang['Actived']='Aktif';
$lang['Expired']='Kedaluwarsa';
$lang['Confirm']="Konfirmasi";
$lang['free']="Gratis";
$lang['hot_list']="Hot List";
$lang['msg_no_History_membership']='Tidak ada riwayat';
$lang['waiting approval']='Menunggu Persetujuan';
$lang['approved']='Disetujui';
$lang['approve']='Menyetujui';
$lang['unapproved']='Tidak Disetujui';
$lang['confirm_approve']='Apakah anda yakin Menyetujui ini?';
$lang['confirm_unapproved']='Apakah anda yakin Tidak Menyetujui ini?';
$lang['msg_limit_product']='Batas penambahan product, Silahkan belangganan membership!';
$lang['msg_limit_hotlist']='Batas penambahan Hot List!';
$lang['limit_time_received']='Batas Waktu Penerimaan';
$lang['msg_remove_product_failed']='Gagal menghapus produk';
$lang['confirm_delete']='Apa kamu yakin ingin menghapus data ini?';
$lang['msg_potong_20_percent']='Keuntungan dipotong 20% untuk biaya layanan kepada penyedia layanan.';
$lang['User Guide']='Panduan Pengguna';
//---------------------------------------------- link/path file if need change path file when language is change ----------------------------//
$lang['link_img_flag']="assets/frontend/img/flags/ind.png";

$lang['Register Seller']='Daftar sebagai Penjual';
$lang['choose_type_store']='Pilih Jenis Toko';
$lang['Registration Seller Successed']='Pendaftaran sebagai Penjual berhasil';
$lang['Registration Seller Successed not all']='Pendaftaran sebagai Penjual berhasil, silahkan Masuk Kembali';
$lang['Registration Seller Failed']='Pendaftaran sebagai Penjual gagal';

$lang['Opps....']='Oops, Produk tidak ditemukan...';
$lang['Opps_cap']='Coba kata kunci lain.';
$lang['keterangan']='Keterangan';
$lang['note_caption']='Ukuran: XL, Warna: Matte Blue, Jenis: A, dan lain-lain';
$lang['valid for a lifetime']='Berlaku seumur hidup!';
$lang['removed_voucher']='Voucher Dibatalkan';
$lang['delete_product']='Produk Dihapus';
$lang['failed_remove_voucher']='Voucher Gagal Dibatalkan';
$lang['download_csv']="Unduh Data Produk (CSV)";
$lang['upload_csv']="Unggah Data Produk (CSV)";
$lang['Choose file']="Pilih File";
$lang['download']="Unduh";
$lang['upload']="Unggah";
$lang['update']="Perbaruhi";
$lang['cap_downloadFirstCSV']="Harap unduh terlebih dulu File CVS untuk mengupdate data";
$lang['Foto Buku Tabung']="Foto Buku Tabung";
$lang['Confirmation Payment']="Konfirmasi Pembayaran";
$lang['Confirmed']="Diterima";
$lang['Paid']="Dibayar";
$lang['Unpaid']="Belum Dibayar";
$lang['Payment Status']="Status Pembayaran";
$lang['ListSupplier']="Daftar Supplier";
$lang['ListMarketer']="Daftar Marketer";
$lang['no marketer to sell your product']="Tidak Ada Marketer yang menjual Produk Anda";
$lang['Opps....mkt']='Oops, Marketer not found...';
$lang['Opps....spl']='Oops, Supplier not found...';
?>