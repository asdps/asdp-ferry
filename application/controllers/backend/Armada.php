<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;
use Dompdf\Options;
class Armada extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_model');
    }

    protected function ci()
    {
        return get_instance();
    }

    public function show($url,$id,$idSebelum){
        // print_r($url,$id);
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/armada-show',[
                'page' => $this->input->get('page'),
                'title' => 'Armada',
                'pageUrl' => base_url('panel/armada'), 
                'bcrumb' => 'Master Data > Detail Armada',
                'armadass' => $this->m_model->selectOne('id',$idSebelum, 'armada'),
                'record' => $this->m_model->selectOne('id',$id,'jenis_aspeks'),
                'records' => $this->m_model->selectcustom('
                    select jenis_aspeks.id, jenis_aspeks.nama_aspek,jenis_aspeks.created_at, sub_aspeks.id,sub_aspeks.name 
                    from jenis_aspeks 
                    inner join sub_aspeks on jenis_aspeks.id = sub_aspeks.jenis_aspek_id 
                    where (jenis_aspeks.id="'.$id.'" AND status="Armada") order by jenis_aspeks.created_at ASC
                ')
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function showDetail($bigId,$id=NULL,$armada){
        if($id == '-'){
            redirect('panel/armada', 'refresh');
        }else{
            $record = $this->load->view('backend/armada-deck',[
                'pageUrl' => base_url('panel/armada'), 
                'armadaElments' => $this->m_model->selectOne('id',$id, 'armada_elements'),
                'armadass' => $this->m_model->selectOne('id',$armada, 'armada'),
                'record' => $this->m_model->selectOne('id',$bigId,'jenis_aspeks'),
                'records' => $this->m_model->selectcustom('
                    select jenis_aspeks.id, jenis_aspeks.nama_aspek, jenis_aspeks.created_at, sub_aspeks.id,sub_aspeks.name 
                    from jenis_aspeks 
                    inner join sub_aspeks on jenis_aspeks.id = sub_aspeks.jenis_aspek_id 
                    where (jenis_aspeks.id="'.$bigId.'" AND status="Armada") order by jenis_aspeks.created_at ASC
                ')
            ]);

            return $record;
        }
    }

    public function showEdit($bigId,$id=NULL,$armada){
        if($id == '-'){
            redirect('panel/armada', 'refresh');
        }else{
            $record = $this->load->view('backend/armada-deck-edit',[
                'pageUrl' => base_url('panel/armada'), 
                'armadaElments' => $this->m_model->selectOne('id',$id, 'armada_elements'),
                'armadass' => $this->m_model->selectOne('id',$armada, 'armada'),
                'record' => $this->m_model->selectOne('id',$bigId,'jenis_aspeks'),
                'records' => $this->m_model->selectcustom('
                    select jenis_aspeks.id, jenis_aspeks.nama_aspek, jenis_aspeks.created_at, sub_aspeks.id,sub_aspeks.name 
                    from jenis_aspeks 
                    inner join sub_aspeks on jenis_aspeks.id = sub_aspeks.jenis_aspek_id 
                    where (jenis_aspeks.id="'.$bigId.'" AND status="Armada") order by jenis_aspeks.created_at ASC
                ')
            ]);

            return $record;
        }
    }

    public function store(){
        $input = $this->input->post('url_canvas');
        if(is_dir('images/armada_result/'.date("d_m_Y"))){
            $namePath = 'images/armada_result/'.date("d_m_Y").'/';
        }else{
            mkdir('images/armada_result/',0777);
            mkdir('images/armada_result/'.date("d_m_Y"),0777);
            $namePath = 'images/armada_result/'.date("d_m_Y").'/';
        }
        $output = $namePath.date('YmdHis').'.png';
        file_put_contents($output, file_get_contents($input));

        $cekData = $this->m_model->selectOne('id',$this->input->post('id'),'trans_armada_hasil');
        if($cekData){
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                $saveArr = array(
                    'id_armada' => $this->input->post('id_armada'),
                    'id_armada_elments' => $this->input->post('id_armada_elments'),
                    'id_jenis_aspek' => $this->input->post('id_jenis_aspek'),
                    'id_sub_jenis_aspek' => $this->input->post('id_sub_jenis_aspek'),
                    'icon_id' => $this->input->post('icon_id'),
                    'url' => $this->input->post('url'),
                    'pointer_x' => $this->input->post('pointer_x'),
                    'pointer_y' => $this->input->post('pointer_y'),
                    'primary_key' => $this->input->post('primary_key'),
                    'kategori' => $this->input->post('kategori'),
                    'nama' => $this->input->post('nama'),
                    'aspek' => $this->input->post('aspek'),
                    'nomor' => $this->input->post('nomor'),
                    'kondisi' => $this->input->post('kondisi'),
                    'posisi' => $this->input->post('posisi'),
                    'tahun' => $this->input->post('tahun'),
                    'width' => $this->input->post('width'),
                    'height' => $this->input->post('height'),
                    'total_icon' => $this->input->post('total_icon'),
                    // 'fileurl' => $pathfile,
                );
                $this->m_model->updateas('id', $this->input->post('id'), $saveArr, 'trans_armada_hasil');
                // $this->m_model->updateas5(
                //     'id_armada', $this->input->post('id_armada'), 
                //     'id_armada_elments', $this->input->post('id_armada_elments'), 
                //     'id_jenis_aspek', $this->input->post('id_jenis_aspek'), 
                //     'id_sub_jenis_aspek', $this->input->post('id_sub_jenis_aspek'), 
                //     'icon_id', $this->input->post('icon_id'), 
                //     array('total_icon' => $this->input->post('total_icon')), 'trans_armada_hasil');
                
                $cekPC = $this->m_model->selectOneWhere3('id_armada',$this->input->post('id_armada'),'id_armada_elments',$this->input->post('id_armada_elments'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'armada_canvas');
                if($cekPC){
                    $this->m_model->updateas('id', $cekPC->id, ['url_canvas' => $output], 'armada_canvas');
                }else{
                    $this->m_model->create([
                        'url_canvas' => $output,
                        'id_armada' => $this->input->post('id_armada'),
                        'id_jenis_aspek' => $this->input->post('id_jenis_aspek'), 
                        'id_armada_elments' => $this->input->post('id_armada_elments')
                    ],'armada_canvas');
                }

                $arr = [];
                $pathfile = '';
                // print_r($_FILES['icon']['name'][0]);
                // die();
                if (!empty($_FILES['icon']['name'])) {
                    if(count($_FILES['icon']['name']) > 0){
                        foreach ($_FILES['icon']['name'] as $k => $value) {
                            if(($value != '')){
                                if(is_dir('images/armada_result_modal/'.date("d_m_Y"))){
                                    $namePath = 'images/armada_result_modal/'.date("d_m_Y").'/';
                                }else{
                                    mkdir('images/armada_result_modal/',0777);
                                    mkdir('images/armada_result_modal/'.date("d_m_Y"),0777);
                                    $namePath = 'images/armada_result_modal/'.date("d_m_Y").'/';
                                }
                                $_FILES['file']['name']     = $_FILES['icon']['name'][$k];
                                $_FILES['file']['type']     = $_FILES['icon']['type'][$k];
                                $_FILES['file']['tmp_name'] = $_FILES['icon']['tmp_name'][$k];
                                $_FILES['file']['error']     = $_FILES['icon']['error'][$k];
                                $_FILES['file']['size']     = $_FILES['icon']['size'][$k];
                                unset($config);
                                $config['upload_path']   = FCPATH.$namePath;
                                $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                                $config['overwrite']     = FALSE;
                                // $config['max_size'] = 3000000;
                                // $config['max_size'] = 3000000;
                                $config['file_name'] = $value;
                                $this->load->library('upload',$config);
                                $this->upload->initialize($config);
                                $this->upload->do_upload('file');
                                $pathfile=$namePath.$this->upload->data('file_name');
                                if(is_file($pathfile)){
                                    $arr[$k]['fileurl'] = $pathfile;
                                    $arr[$k]['trans_id'] = $this->input->post('id');
                                    $arr[$k]['filename'] = $this->upload->data('file_name');
                                }
                            }
                        }
                        if(count($arr) > 0){
                            $this->db->insert_batch('trans_armada_hasil_foto',$arr);
                        }
                    }
                    // print_r($pathfile);
                    // die();
                }
            }else{
                $cekPC = $this->m_model->selectOneWhere3('id_armada',$this->input->post('id_armada'),'id_armada_elments',$this->input->post('id_armada_elments'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'armada_canvas_log');
                if($cekPC){
                    $this->m_model->updateas('id', $cekPC->id, ['url_canvas' => $output], 'armada_canvas_log');
                }else{
                    $this->m_model->create([
                        'url_canvas' => $output,
                        'id_armada' => $this->input->post('id_armada'),
                        'id_jenis_aspek' => $this->input->post('id_jenis_aspek'), 
                        'id_armada_elments' => $this->input->post('id_armada_elments')
                    ],'armada_canvas_log');
                }

                $saveArr = array(
                    'id_armada' => $this->input->post('id_armada'),
                    'id_armada_elments' => $this->input->post('id_armada_elments'),
                    'id_jenis_aspek' => $this->input->post('id_jenis_aspek'),
                    'id_sub_jenis_aspek' => $this->input->post('id_sub_jenis_aspek'),
                    'icon_id' => $this->input->post('icon_id'),
                    'url' => $this->input->post('url'),
                    'pointer_x' => $this->input->post('pointer_x'),
                    'pointer_y' => $this->input->post('pointer_y'),
                    'primary_key' => $this->input->post('primary_key'),
                    'kategori' => $this->input->post('kategori'),
                    'nama' => $this->input->post('nama'),
                    'aspek' => $this->input->post('aspek'),
                    'nomor' => $this->input->post('nomor'),
                    'kondisi' => $this->input->post('kondisi'),
                    'posisi' => $this->input->post('posisi'),
                    'tahun' => $this->input->post('tahun'),
                    'width' => $this->input->post('width'),
                    'height' => $this->input->post('height'),
                    'total_icon' => $this->input->post('total_icon'),
                    'trans_id' => $this->input->post('id')
                );
                $saveArr['status']        = 2;
                $saveArr['flag']        = 'Edit';
                $saveArr['created_at']  = date('Y-m-d H:i:s');
                $saveArr['created_by']  = cleartext($this->session->userdata('admin_data')->id);
                $saveArr['fulluri']     = base_url('backend/armada/showEdit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_armada_elments').'/'.$this->input->post('id_armada'));

                $this->m_model->updateas('id', $this->input->post('id'), array('status' => 2), 'trans_armada_hasil');
                $this->m_model->create($saveArr, 'trans_armada_hasil_log');
                // $this->m_model->updateas5(
                //     'id_armada', $this->input->post('id_armada'), 
                //     'id_armada_elments', $this->input->post('id_armada_elments'), 
                //     'id_jenis_aspek', $this->input->post('id_jenis_aspek'), 
                //     'id_sub_jenis_aspek', $this->input->post('id_sub_jenis_aspek'), 
                //     'icon_id', $this->input->post('icon_id'), 
                //     array('total_icon' => $this->input->post('total_icon')), 'trans_armada_hasil_log');

                $cekArmada = $this->m_model->selectOne('id',$this->input->post('id_armada'),'armada');
                $dataMailCab = $this->m_model->selectOne('id',$cekArmada->cabang_id,'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Edit Data - Hasil Armada',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }

                $arr = [];
                $pathfile = '';
                if (!empty($_FILES['icon']['name'][0])) {
                    if(count($_FILES['icon']['name']) > 0){
                        foreach ($_FILES['icon']['name'] as $k => $value) {
                            if(is_dir('images/armada_result_modal/'.date("d_m_Y"))){
                                $namePath = 'images/armada_result_modal/'.date("d_m_Y").'/';
                            }else{
                                mkdir('images/armada_result_modal/',0777);
                                mkdir('images/armada_result_modal/'.date("d_m_Y"),0777);
                                $namePath = 'images/armada_result_modal/'.date("d_m_Y").'/';
                            }
                            $_FILES['file']['name']     = $_FILES['icon']['name'][$k];
                            $_FILES['file']['type']     = $_FILES['icon']['type'][$k];
                            $_FILES['file']['tmp_name'] = $_FILES['icon']['tmp_name'][$k];
                            $_FILES['file']['error']     = $_FILES['icon']['error'][$k];
                            $_FILES['file']['size']     = $_FILES['icon']['size'][$k];
                            unset($config);
                            $config['upload_path']   = FCPATH.$namePath;
                            $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                            $config['overwrite']     = FALSE;
                            // $config['max_size'] = 3000000;
                            $config['file_name'] = $value;
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('file');
                            $pathfile=$namePath.$this->upload->data('file_name');
                            if(is_file($pathfile)){
                                $arr[$k]['fileurl'] = $pathfile;
                                $arr[$k]['trans_id'] = $this->input->post('id');
                                $arr[$k]['filename'] = $this->upload->data('file_name');
                            }
                        }

                        if(count($arr) > 0){
                            $this->db->insert_batch('trans_armada_hasil_foto',$arr);
                        }
                    }else{
                        $getFoto = $this->m_model->selectas('trans_id',$this->input->post('id'),'trans_armada_hasil_foto');
                        $arrs = [];
                        if(count($getFoto) > 0){
                            foreach ($getFoto as $k => $value) {
                                $arrs[$k]['fileurl'] = $value->fileurl;
                                $arrs[$k]['trans_id'] = $value->trans_id;
                                $arrs[$k]['filename'] = $value->filename;
                            }
                        $this->db->insert_batch('trans_armada_hasil_foto_log',$arrs);
                        }
                        
                    }
                }else{
                    $getFoto = $this->m_model->selectas('trans_id',$this->input->post('id'),'trans_armada_hasil_foto');
                    $arrs = [];
                    if(count($getFoto) > 0){
                        foreach ($getFoto as $k => $value) {
                            $arrs[$k]['fileurl'] = $value->fileurl;
                            $arrs[$k]['trans_id'] = $value->trans_id;
                            $arrs[$k]['filename'] = $value->filename;
                        }
                    $this->db->insert_batch('trans_armada_hasil_foto_log',$arrs);
                    }
                }
            }
            
            return redirect('backend/armada/showEdit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_armada_elments').'/'.$this->input->post('id_armada'));
        }else{
            $cekPC = $this->m_model->selectOneWhere3('id_armada',$this->input->post('id_armada'),'id_armada_elments',$this->input->post('id_armada_elments'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'armada_canvas_log');
            if($cekPC){
                $this->m_model->updateas('id', $cekPC->id, ['url_canvas' => $output], 'armada_canvas');
            }else{
                $this->m_model->create([
                    'url_canvas' => $output,
                    'id_armada' => $this->input->post('id_armada'),
                    'id_jenis_aspek' => $this->input->post('id_jenis_aspek'), 
                    'id_armada_elments' => $this->input->post('id_armada_elments')
                ],'armada_canvas');
            }

            $saveArr = array(
                'id_armada' => $this->input->post('id_armada'),
                'id_armada_elments' => $this->input->post('id_armada_elments'),
                'id_jenis_aspek' => $this->input->post('id_jenis_aspek'),
                'id_sub_jenis_aspek' => $this->input->post('id_sub_jenis_aspek'),
                'icon_id' => $this->input->post('icon_id'),
                'url' => $this->input->post('url'),
                'pointer_x' => $this->input->post('pointer_x'),
                'pointer_y' => $this->input->post('pointer_y'),
                'primary_key' => $this->input->post('primary_key'),
                'kategori' => $this->input->post('kategori'),
                'nama' => $this->input->post('nama'),
                'aspek' => $this->input->post('aspek'),
                'nomor' => $this->input->post('nomor'),
                'kondisi' => $this->input->post('kondisi'),
                'posisi' => $this->input->post('posisi'),
                'tahun' => $this->input->post('tahun'),
                'width' => $this->input->post('width'),
                'height' => $this->input->post('height'),
                'total_icon' => $this->input->post('total_icon')
            );
            // $this->m_model->updateas('id', $this->input->post('id_armada_elments'), ['url_canvas' => $output], 'armada_elements');
            if ($this->m_model->create($saveArr, 'trans_armada_hasil') == 1) {
                // $this->m_model->updateas5(
                //     'id_armada', $this->input->post('id_armada'), 
                //     'id_armada_elments', $this->input->post('id_armada_elments'), 
                //     'id_jenis_aspek', $this->input->post('id_jenis_aspek'), 
                //     'id_sub_jenis_aspek', $this->input->post('id_sub_jenis_aspek'), 
                //     'icon_id', $this->input->post('icon_id'), 
                //     array('total_icon' => $this->input->post('total_icon')), 'trans_armada_hasil');

                $arr = [];
                $pathfile = '';

                if (!empty($_FILES['icon']['name'])) {
                    if(count($_FILES['icon']['name']) > 0){
                        foreach ($_FILES['icon']['name'] as $k => $value) {
                            if(is_dir('images/armada_result_modal/'.date("d_m_Y"))){
                                $namePath = 'images/armada_result_modal/'.date("d_m_Y").'/';
                            }else{
                                mkdir('images/armada_result_modal/',0777);
                                mkdir('images/armada_result_modal/'.date("d_m_Y"),0777);
                                $namePath = 'images/armada_result_modal/'.date("d_m_Y").'/';
                            }
                            $_FILES['file']['name']     = $_FILES['icon']['name'][$k];
                            $_FILES['file']['type']     = $_FILES['icon']['type'][$k];
                            $_FILES['file']['tmp_name'] = $_FILES['icon']['tmp_name'][$k];
                            $_FILES['file']['error']     = $_FILES['icon']['error'][$k];
                            $_FILES['file']['size']     = $_FILES['icon']['size'][$k];
                            unset($config);
                            $config['upload_path']   = FCPATH.$namePath;
                            $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                            $config['overwrite']     = FALSE;
                            // $config['max_size'] = 3000000;
                            $config['file_name'] = $value;
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('file');
                            $pathfile=$namePath.$this->upload->data('file_name');
                            if(is_file($pathfile)){
                                $arr[$k]['fileurl'] = $pathfile;
                                $arr[$k]['trans_id'] = $this->db->insert_id();
                                $arr[$k]['filename'] = $this->upload->data('file_name');
                            }

                        }
                        if(count($arr) > 0){
                            $this->db->insert_batch('trans_armada_hasil_foto',$arr);
                        }
                    }
                    // print_r($pathfile);
                    // die();
                }
                return redirect('backend/armada/showEdit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_armada_elments').'/'.$this->input->post('id_armada'));
            }
        }
    }

    public function delete(){
        $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
        if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
             $input = $this->input->post('url_canvas');
            $output = 'images/armada/files'.date('YmdHis').'.png';
            file_put_contents($output, file_get_contents($input));
            // $this->m_model->updateas('id', $this->input->post('id_armada_elments'), ['url_canvas' => $output], 'armada_elements');
            $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'trans_armada_hasil');
            $cekPC = $this->m_model->selectOneWhere3('id_armada',$selectOne->id_armada,'id_armada_elments',$selectOne->id_armada_elments,'id_jenis_aspek',$selectOne->id_jenis_aspek,'armada_canvas');
            if($cekPC){
                $this->m_model->updateas('id', $cekPC->id, ['url_canvas' => $output], 'armada_canvas');
            }else{
                $this->m_model->create([
                    'url_canvas' => $output,
                    'id_armada' => $selectOne->id_armada,
                    'id_jenis_aspek' => $selectOne->id_jenis_aspek, 
                    'id_armada_elments' => $selectOne->id_armada_elments
                ],'armada_canvas');
            }

            $this->db->delete('trans_armada_hasil', array('id' => $this->input->post('id')));
            header('Content-Type: application/json');
                echo json_encode([
                    'status' => true,
                    'message' => 'Sukses Menghapus Data'
            ]);
        }else{
            $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'trans_armada_hasil');
            $saveArr = array(
                'trans_id' => $selectOne->id,
                'id_armada' => $selectOne->id_armada,
                'id_armada_elments' => $selectOne->id_armada_elments,
                'id_jenis_aspek' => $selectOne->id_jenis_aspek,
                'id_sub_jenis_aspek' => $selectOne->id_sub_jenis_aspek,
                'icon_id' => $selectOne->icon_id,
                'url' => $selectOne->url,
                'pointer_x' => $selectOne->pointer_x,
                'pointer_y' => $selectOne->pointer_y,
                'primary_key' => $selectOne->primary_key,
                'kategori' => $selectOne->kategori,
                'nama' => $selectOne->nama,
                'aspek' => $selectOne->aspek,
                'nomor' => $selectOne->nomor,
                'kondisi' => $selectOne->kondisi,
                'posisi' => $selectOne->posisi,
                'tahun' => $selectOne->tahun,
                'width' => $selectOne->width,
                'height' => $selectOne->height,
                'total_icon' => $selectOne->total_icon
            );
            $saveArr['status']        = 2;
            $saveArr['flag']        = 'Delete';
            $saveArr['created_at']  = date('Y-m-d H:i:s');
            $saveArr['created_by']  = cleartext($this->session->userdata('admin_data')->id);
            $saveArr['fulluri']     = base_url('backend/armada/showEdit/'.$selectOne->id_jenis_aspek.'/'.$selectOne->id_armada_elments.'/'.$selectOne->id_armada);

            $this->m_model->updateas('id', $this->input->post('id'), array('status' => 2), 'trans_armada_hasil');
            $this->m_model->create($saveArr, 'trans_armada_hasil_log');
            // $this->m_model->updateas5(
            //         'id_armada', $selectOne->id_armada, 
            //         'id_armada_elments', $selectOne->id_armada_elments, 
            //         'id_jenis_aspek', $selectOne->id_jenis_aspek, 
            //         'id_sub_jenis_aspek', $selectOne->id_sub_jenis_aspek, 
            //         'icon_id', $selectOne->icon_id, 
            //         array('total_icon' => $selectOne->total_icon), 'trans_armada_hasil_log');


            $cekPelabuhans = $this->m_model->selectOne('id',$selectOne->id_armada,'armada');
            $dataMailCab = $this->m_model->selectOne('id',$cekPelabuhans->cabang_id,'cabangs');
            $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
            if(count($dataMailSp) > 0){
                foreach ($dataMailSp as $value) {
                    $dataEmailNya = array(
                        "pesan" => "Ada Approval Terbaru Untuk Anda",
                        "PIC Peminta" => $this->session->userdata('admin_data')->username,
                        "email" => $value->email,
                        "Cabang" => $dataMailCab->name,
                        "Tipe Permintaan" => 'Hapus Data - Hasil Pelabuhan',
                        "Status" => "Waiting For Approval"
                    );
                    sendEMAIL($dataEmailNya);
                }
            }

            header('Content-Type: application/json');
            echo json_encode([
                'status' => true,
                'message' => 'Sukses Menghapus Data, Silahkan Tunggu Approval Dari Admin'
            ]);
        }
    }

    public function getData(){
        echo json_encode($this->m_model->selectWhere3('id_armada',$this->input->post('id_armada'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'id_armada_elments',$this->input->post('id_armada_elments'),'trans_armada_hasil'));
    }

    public function getDataOne(){
        $record = $this->m_model->selectOneWhere6('id_armada',$this->input->post('id_armada'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'id_armada_elments',$this->input->post('id_armada_elments'),'primary_key',$this->input->post('primary_key'),'pointer_x',$this->input->post('pointer_x'),'pointer_y',$this->input->post('pointer_y'),'trans_armada_hasil');
        $record1 = $this->m_model->selectas('trans_id',$record->id,'trans_armada_hasil_foto');
        $data = array(
            'record' => $record,
            'record_file' => $record1
        );
        echo json_encode($data);
    }

    public function laporan($jenisAspek, $idSub, $id){

        // ini_set('max_execution_time', 0);
        // ini_set('memory_limit', "10G");

         $data = array(
            "data" => array(
                "judul" => 'ARMADA',
                "record" => $this->m_model->selectOne('id',$id,'armada'),
            )
        );
        
        $options = new Options();
        // $options->set('isRemoteEnabled',true);      
        // $options->set('defaultFont', 'Courier');
        $options->set('isRemoteEnabled', TRUE);
        $options->set('debugKeepTemp', TRUE);
        $options->set('isHtml5ParserEnabled', TRUE);
        $options->set('isPhpEnabled', TRUE);
        $dompdf = new Dompdf( $options );

        $dompdf->setPaper('A4', 'potrait');
        $dompdf->set_option('isRemoteEnabled', TRUE);
        $dompdf->filename = 'laporan'.$this->m_model->selectOne('id',$id,'armada')->name.".pdf";
        $html = $this->load->view('backend/armada-laporan', $data, TRUE);
        $dompdf->load_html($html);
        // Render the PDF
        // $dompdf->set_base_path(base_url());
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream($dompdf->filename, array("Attachment" => false));
        header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
    }

    public function getDataNum(){
        $record = $this->m_model->selectas3('id_armada',$this->input->post('id_armada'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'id_armada_elments',$this->input->post('id_armada_elments'),'trans_armada_hasil');
        header('Content-Type: application/json');
        echo json_encode(count($record)+1);
    }

    public function getDataNumWithUrl(){
        $record = $this->m_model->selectas5(
            'id_armada',$this->input->post('id_armada'),
            'id_jenis_aspek',$this->input->post('id_jenis_aspek'),
            'id_armada_elments',$this->input->post('id_armada_elments'),
            'id_sub_jenis_aspek',$this->input->post('id_sub_jenis_aspek'),
            'icon_id',$this->input->post('icon_id'),
        'trans_armada_hasil');
        header('Content-Type: application/json');
         echo json_encode([
            'count' => count($record)+1,
            // 'icons' => $recordIcon
        ]);
    }
}