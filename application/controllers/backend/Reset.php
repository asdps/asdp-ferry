<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_model');
    }

     public function index() {
        if ($this->session->userdata('admin')) {
            redirect('panel', 'refresh');
        } else {
            $this->load->view('backend/reset');
        }

        if (!is_null($this->input->post('email')) && !is_null($this->input->post('password'))) {
            $selectData = $this->m_model->selectOne('email',$this->input->post('email'),'users');
            if($selectData){
                $data = array(
                    'email' => $this->input->post('email'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                );
                $dataJ = array(
                    "email" => $this->input->post('email'),
                    "username" => $selectData->username,
                    "password" => $this->input->post('password'),
                    "pesan" => "Ada Perubahan Akun Baru Untuk Anda"
                );
                $update = $this->m_model->updateas('id', $selectData->id, $data,
                    'users');
                if ($update == 1) {
                    sendEMAIL($dataJ);
                    $this->session->set_flashdata('sukses', 'Sukses Reset Password, Silahkan Login');
                    redirect('panel/login', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Gagal Merubah Akun');
                    redirect('backend/reset', 'refresh');
                }
            }else{
                $this->session->set_flashdata('error', 'Email Tidak Ditemukan');
                redirect('backend/reset', 'refresh');
            }
        }
    }

}