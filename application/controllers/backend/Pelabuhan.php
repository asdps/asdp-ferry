<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;
use Dompdf\Options;

class Pelabuhan extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_model');
    }

    protected function ci()
    {
        return get_instance();
    }

    public function show($url,$id,$idSebelum){
        // print_r($this->input->get('page'));
        // die();
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/pelabuhan-show',[
                'page' => $this->input->get('page'),
                'title' => 'Pelabuhan',
                'pageUrl' => base_url('panel/pelabuhan'),
                'bcrumb' => 'Master Data > Detail Pelabuhan',
                'pelabuhanss' => $this->m_model->selectOne('id',$idSebelum, 'pelabuhans'),
                'record' => $this->m_model->selectOne('id',$id,'jenis_aspeks'),
                'records' => $this->m_model->selectcustom('
                    select jenis_aspeks.id, jenis_aspeks.nama_aspek, sub_aspeks.id,sub_aspeks.name 
                    from jenis_aspeks 
                    inner join sub_aspeks on jenis_aspeks.id = sub_aspeks.jenis_aspek_id 
                    where (jenis_aspeks.id="'.$id.'" AND status="Pelabuhan") 
                ')
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function edit($url,$id,$idSebelum){
        // print_r($url,$id);
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/pelabuhan-edit',[
                'page' => $this->input->get('page'),
                'title' => 'Pelabuhan',
                'pageUrl' => base_url('panel/pelabuhan'),
                'bcrumb' => 'Master Data > Edit Pelabuhan',
                // 'pelabuhanDrag' => $this->m_model->selectWhere2('id_pelabuhan',$idSebelum,'id_jenis_aspek',$id,'trans_pelabuhans_hasil'),
                'pelabuhanss' => $this->m_model->selectOne('id',$idSebelum, 'pelabuhans'),
                'record' => $this->m_model->selectOne('id',$id,'jenis_aspeks'),
                'records' => $this->m_model->selectcustom('
                    select jenis_aspeks.id, jenis_aspeks.nama_aspek, sub_aspeks.id,sub_aspeks.name 
                    from jenis_aspeks 
                    inner join sub_aspeks on jenis_aspeks.id = sub_aspeks.jenis_aspek_id 
                    where (jenis_aspeks.id="'.$id.'" AND status="Pelabuhan") 
                ')
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function getData(){
        echo json_encode($this->m_model->selectWhere2('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'trans_pelabuhans_hasil'));
    }

    public function getDataOne(){
        $record = $this->m_model->selectOneWhere5('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'primary_key',$this->input->post('primary_key'),'pointer_x',$this->input->post('pointer_x'),'pointer_y',$this->input->post('pointer_y'),'trans_pelabuhans_hasil');
        $record1 = $this->m_model->selectas('trans_id',$record->id,'trans_pelabuhans_hasil_foto');
        $data = array(
            'record' => $record,
            'record_foto' => $record1
        );
        echo json_encode($data);
    }

    public function create(){
        if ($this->input->post('add')) {
            $param = array(
                'roles'  => cleartext($this->input->post('roles')),
            );
            $create=$this->m_model->insertgetid($param, 'roles');
            $this->session->set_flashdata('sukses', 'Data Berhasil Di Buat');
            redirect('backend/pelabuhan', 'refresh');
        }
    }

    public function store(){
        try {
            header('Content-Type: application/json');
            $input = $this->input->post('url_canvas');
            if(is_dir('images/pelabuhan_result/'.date("d_m_Y"))){
                $namePath = 'images/pelabuhan_result/'.date("d_m_Y").'/';
            }else{
                mkdir('images/pelabuhan_result/',0777);
                mkdir('images/pelabuhan_result/'.date("d_m_Y"),0777);
                $namePath = 'images/pelabuhan_result/'.date("d_m_Y").'/';
            }
            $output = $namePath.date('YmdHis').'.png';
            file_put_contents($output, file_get_contents($input));
            // print_r($this->m_model->selectOne('id',$id,'pelabuhans')->url_canvas);
            // die();

            $cekData = $this->m_model->selectOne('id',$this->input->post('id'),'trans_pelabuhans_hasil');
            if($cekData){
                $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
                if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                    $saveArr = array(
                        'id_pelabuhan' => $this->input->post('id_pelabuhan'),
                        'id_jenis_aspek' => $this->input->post('id_jenis_aspek'),
                        'id_sub_jenis_aspek' => $this->input->post('id_sub_jenis_aspek'),
                        'icon_id' => $this->input->post('icon_id'),
                        'url' => $this->input->post('url'),
                        'pointer_x' => $this->input->post('pointer_x'),
                        'pointer_y' => $this->input->post('pointer_y'),
                        'primary_key' => $this->input->post('primary_key'),
                        'kategori' => $this->input->post('kategori'),
                        'nama' => $this->input->post('nama'),
                        'aspek' => $this->input->post('aspek'),
                        'nomor' => $this->input->post('nomor'),
                        'kondisi' => $this->input->post('kondisi'),
                        'posisi' => $this->input->post('posisi'),
                        'tahun' => $this->input->post('tahun'),
                        'total_icon' => $this->input->post('total_icon'),
                        'width' => $this->input->post('width'),
                        'height' => $this->input->post('height'),
                    );
                    $this->m_model->updateas('id', $this->input->post('id'), $saveArr, 'trans_pelabuhans_hasil');
                    // $this->m_model->updateas4(
                    //     'id_pelabuhan', $this->input->post('id_pelabuhan'), 
                    //     'id_jenis_aspek', $this->input->post('id_jenis_aspek'), 
                    //     'id_sub_jenis_aspek', $this->input->post('id_sub_jenis_aspek'), 
                    //     'icon_id', $this->input->post('icon_id'), 
                    //     array('total_icon' => $this->input->post('total_icon')), 'trans_pelabuhans_hasil');
                    // $this->m_model->updateas('id', $this->input->post('id_pelabuhan'), ['url_canvas' => $output], 'pelabuhans');
                    $cekPC = $this->m_model->selectOneWhere2('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'pelabuhans_canvas');
                    if($cekPC){
                        $this->m_model->updateas('id', $cekPC['id'], ['url_canvas' => $output], 'pelabuhans_canvas');
                    }else{
                        $this->m_model->create(['url_canvas' => $output,'id_pelabuhan' => $this->input->post('id_pelabuhan'),'id_jenis_aspek' => $this->input->post('id_jenis_aspek')], 'pelabuhans_canvas');
                    }


                    $arr = [];
                    $pathfile = '';
                    if (!empty($_FILES['icon']['name'])) {
                        if(count($_FILES['icon']['name']) > 0){
                            foreach ($_FILES['icon']['name'] as $k => $value) {
                                if(is_dir('images/pelabuhan_result_modal/'.date("d_m_Y"))){
                                    $namePath = 'images/pelabuhan_result_modal/'.date("d_m_Y").'/';
                                }else{
                                    mkdir('images/pelabuhan_result_modal/',0777);
                                    mkdir('images/pelabuhan_result_modal/'.date("d_m_Y"),0777);
                                    $namePath = 'images/pelabuhan_result_modal/'.date("d_m_Y").'/';
                                }
                                $_FILES['file']['name']     = $_FILES['icon']['name'][$k];
                                $_FILES['file']['type']     = $_FILES['icon']['type'][$k];
                                $_FILES['file']['tmp_name'] = $_FILES['icon']['tmp_name'][$k];
                                $_FILES['file']['error']     = $_FILES['icon']['error'][$k];
                                $_FILES['file']['size']     = $_FILES['icon']['size'][$k];
                                unset($config);
                                $config['upload_path']   = FCPATH.$namePath;
                                $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                                $config['overwrite']     = FALSE;
                                // $config['max_size'] = 3000000;
                                $config['file_name'] = $value;
                                $this->load->library('upload',$config);
                                $this->upload->initialize($config);
                                $this->upload->do_upload('file');
                                $pathfile=$namePath.$this->upload->data('file_name');
                                if(is_file($pathfile)){
                                    $arr[$k]['fileurl'] = $pathfile;
                                    $arr[$k]['trans_id'] = $this->input->post('id');
                                    $arr[$k]['filename'] = $this->upload->data('file_name');
                                }

                            }
                            if(count($arr) > 0){
                                $this->db->insert_batch('trans_pelabuhans_hasil_foto',$arr);
                            }
                        }
                    }
                }else{
                    
                    $cekPC = $this->m_model->selectOneWhere2('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'pelabuhans_canvas_log');
                    if($cekPC){
                        $this->m_model->updateas('id', $cekPC['id'], ['url_canvas' => $output], 'pelabuhans_canvas_log');
                    }else{
                        $this->m_model->create(['url_canvas' => $output,'id_pelabuhan' => $this->input->post('id_pelabuhan'),'id_jenis_aspek' => $this->input->post('id_jenis_aspek')], 'pelabuhans_canvas_log');
                    }

                    $saveArr = array(
                        'trans_id' => $this->input->post('id'),
                        'id_pelabuhan' => $this->input->post('id_pelabuhan'),
                        'id_jenis_aspek' => $this->input->post('id_jenis_aspek'),
                        'id_sub_jenis_aspek' => $this->input->post('id_sub_jenis_aspek'),
                        'icon_id' => $this->input->post('icon_id'),
                        'url' => $this->input->post('url'),
                        'pointer_x' => $this->input->post('pointer_x'),
                        'pointer_y' => $this->input->post('pointer_y'),
                        'primary_key' => $this->input->post('primary_key'),
                        'kategori' => $this->input->post('kategori'),
                        'nama' => $this->input->post('nama'),
                        'aspek' => $this->input->post('aspek'),
                        'nomor' => $this->input->post('nomor'),
                        'kondisi' => $this->input->post('kondisi'),
                        'posisi' => $this->input->post('posisi'),
                        'tahun' => $this->input->post('tahun'),
                        'width' => $this->input->post('width'),
                        'height' => $this->input->post('height'),
                        'total_icon' => $this->input->post('total_icon'),
                        'status' => 2,
                    );
                    $this->m_model->updateas('id', $this->input->post('id'), array('status' => 2), 'trans_pelabuhans_hasil');
                    

                    $saveArr['flag']        = 'Edit';
                    $saveArr['created_at']  = date('Y-m-d H:i:s');
                    $saveArr['created_by']  = cleartext($this->session->userdata('admin_data')->id);
                    $saveArr['fulluri']     = base_url('backend/pelabuhan/edit/edit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_pelabuhan'));

                    $this->m_model->insertgetid($saveArr, 'trans_pelabuhans_hasil_log');
                    // $this->m_model->updateas4(
                    //     'id_pelabuhan', $this->input->post('id_pelabuhan'), 
                    //     'id_jenis_aspek', $this->input->post('id_jenis_aspek'), 
                    //     'id_sub_jenis_aspek', $this->input->post('id_sub_jenis_aspek'), 
                    //     'icon_id', $this->input->post('icon_id'), 
                    //     array('total_icon' => $this->input->post('total_icon')), 'trans_pelabuhans_hasil_log');
                    // EMAIL
                    $cekPelabuhans = $this->m_model->selectOne('id',$this->input->post('id_pelabuhan'),'pelabuhans');
                    $dataMailCab = $this->m_model->selectOne('id',$cekPelabuhans->cabang_id,'cabangs');
                    $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                    if(count($dataMailSp) > 0){
                        foreach ($dataMailSp as $value) {
                            $dataEmailNya = array(
                                "pesan" => "Ada Approval Terbaru Untuk Anda",
                                "PIC Peminta" => $this->session->userdata('admin_data')->username,
                                "email" => $value->email,
                                "Cabang" => $dataMailCab->name,
                                "Tipe Permintaan" => 'Edit Data - Hasil Pelabuhan',
                                "Status" => "Waiting For Approval"
                            );
                            sendEMAIL($dataEmailNya);
                        }
                    }
                    // print_r($saveArr);

                    $arr = [];
                    $pathfile = '';
                    $this->m_model->deleteas('trans_id',$this->input->post('id'),'trans_pelabuhans_hasil_foto_log');
                    // print_r($_FILES['icon']['name']);
                    // die();
                    if (!empty($_FILES['icon']['name'][0])) {
                        if(count($_FILES['icon']['name']) > 0){
                            foreach ($_FILES['icon']['name'] as $k => $value) {
                                if(is_dir('images/pelabuhan_result_modal/'.date("d_m_Y"))){
                                    $namePath = 'images/pelabuhan_result_modal/'.date("d_m_Y").'/';
                                }else{
                                    mkdir('images/pelabuhan_result_modal/',0777);
                                    mkdir('images/pelabuhan_result_modal/'.date("d_m_Y"),0777);
                                    $namePath = 'images/pelabuhan_result_modal/'.date("d_m_Y").'/';
                                }
                                $_FILES['file']['name']     = $_FILES['icon']['name'][$k];
                                $_FILES['file']['type']     = $_FILES['icon']['type'][$k];
                                $_FILES['file']['tmp_name'] = $_FILES['icon']['tmp_name'][$k];
                                $_FILES['file']['error']     = $_FILES['icon']['error'][$k];
                                $_FILES['file']['size']     = $_FILES['icon']['size'][$k];
                                unset($config);
                                $config['upload_path']   = FCPATH.$namePath;
                                $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                                $config['overwrite']     = FALSE;
                                // $config['max_size'] = 3000000;
                                $config['file_name'] = $value;
                                $this->load->library('upload',$config);
                                $this->upload->initialize($config);
                                $this->upload->do_upload('file');
                                $pathfile=$namePath.$this->upload->data('file_name');
                                if(is_file($pathfile)){
                                    $arr[$k]['fileurl'] = $pathfile;
                                    $arr[$k]['trans_id'] = $this->input->post('id');
                                    $arr[$k]['filename'] = $this->upload->data('file_name');
                                }

                            }
                            if(count($arr) > 0){
                                $this->db->insert_batch('trans_pelabuhans_hasil_foto_log',$arr);
                            }
                        }else{
                            $getFoto = $this->m_model->selectas('trans_id',$this->input->post('id'),'trans_pelabuhans_hasil_foto');
                            $arrs = [];
                            if(count($getFoto) > 0){
                                foreach ($getFoto as $k => $value) {
                                    $arrs[$k]['fileurl'] = $value->fileurl;
                                    $arrs[$k]['trans_id'] = $value->trans_id;
                                    $arrs[$k]['filename'] = $value->filename;
                                }
                            $this->db->insert_batch('trans_pelabuhans_hasil_foto_log',$arrs);
                            }
                            
                        }
                    }else{
                        $getFoto = $this->m_model->selectas('trans_id',$this->input->post('id'),'trans_pelabuhans_hasil_foto');
                        $arrs = [];
                        if(count($getFoto) > 0){
                            foreach ($getFoto as $k => $value) {
                                $arrs[$k]['fileurl'] = $value->fileurl;
                                $arrs[$k]['trans_id'] = $value->trans_id;
                                $arrs[$k]['filename'] = $value->filename;
                            }
                        $this->db->insert_batch('trans_pelabuhans_hasil_foto_log',$arrs);
                        }
                    }
                }
                redirect('backend/pelabuhan/edit/edit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_pelabuhan'));
            }else{
                $cekPC = $this->m_model->selectOneWhere2('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'pelabuhans_canvas');
                if($cekPC){
                    $this->m_model->updateas('id', $cekPC['id'], ['url_canvas' => $output], 'pelabuhans_canvas');
                }else{
                    $this->m_model->create(['url_canvas' => $output,'id_pelabuhan' => $this->input->post('id_pelabuhan'),'id_jenis_aspek' => $this->input->post('id_jenis_aspek')], 'pelabuhans_canvas');
                }

                $saveArr = array(
                    'id_pelabuhan' => $this->input->post('id_pelabuhan'),
                    'id_jenis_aspek' => $this->input->post('id_jenis_aspek'),
                    'id_sub_jenis_aspek' => $this->input->post('id_sub_jenis_aspek'),
                    'icon_id' => $this->input->post('icon_id'),
                    'url' => $this->input->post('url'),
                    'pointer_x' => $this->input->post('pointer_x'),
                    'pointer_y' => $this->input->post('pointer_y'),
                    'primary_key' => $this->input->post('primary_key'),
                    'kategori' => $this->input->post('kategori'),
                    'nama' => $this->input->post('nama'),
                    'aspek' => $this->input->post('aspek'),
                    'nomor' => $this->input->post('nomor'),
                    'kondisi' => $this->input->post('kondisi'),
                    'posisi' => $this->input->post('posisi'),
                    'tahun' => $this->input->post('tahun'),
                    'width' => $this->input->post('width'),
                    'height' => $this->input->post('height'),
                    'total_icon' => $this->input->post('total_icon'),
                    
                    // 'fileurl' => $pathfile,
                );
                
                // $this->m_model->updateas4(
                //         'id_pelabuhan', $this->input->post('id_pelabuhan'), 
                //         'id_jenis_aspek', $this->input->post('id_jenis_aspek'), 
                //         'id_sub_jenis_aspek', $this->input->post('id_sub_jenis_aspek'), 
                //         'icon_id', $this->input->post('icon_id'), 
                //         array('total_icon' => $this->input->post('total_icon')), 'trans_pelabuhans_hasil');
                if ($this->m_model->create($saveArr, 'trans_pelabuhans_hasil') == 1) {
                     
                    $idss = $this->db->insert_id();
                    $arr = [];
                    $pathfile = '';
                        // print_r($this->input->post());
                        // die();
                    if (!empty($_FILES['icon']['name'])) {
                        if(count($_FILES['icon']['name']) > 0){
                            foreach ($_FILES['icon']['name'] as $k => $value) {
                                if(is_dir('images/pelabuhan_result_modal/'.date("d_m_Y"))){
                                    $namePath = 'images/pelabuhan_result_modal/'.date("d_m_Y").'/';
                                }else{
                                    mkdir('images/pelabuhan_result_modal/',0777);
                                    mkdir('images/pelabuhan_result_modal/'.date("d_m_Y"),0777);
                                    $namePath = 'images/pelabuhan_result_modal/'.date("d_m_Y").'/';
                                }
                    //              print_r($value);
                    // die();
                                $_FILES['file']['name']     = $_FILES['icon']['name'][$k];
                                $_FILES['file']['type']     = $_FILES['icon']['type'][$k];
                                $_FILES['file']['tmp_name'] = $_FILES['icon']['tmp_name'][$k];
                                $_FILES['file']['error']     = $_FILES['icon']['error'][$k];
                                $_FILES['file']['size']     = $_FILES['icon']['size'][$k];
                                unset($config);
                                $config['upload_path']   = FCPATH.$namePath;
                                $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                                $config['overwrite']     = FALSE;
                                // $config['max_size'] = 3000000;
                                $config['file_name'] = $value;
                                $this->load->library('upload',$config);
                                $this->upload->initialize($config);
                                $this->upload->do_upload('file');
                                $pathfile=$namePath.$this->upload->data('file_name');
                                if(is_file($pathfile)){
                                    $arr[$k]['fileurl'] = $pathfile;
                                    $arr[$k]['trans_id'] = $idss;
                                    $arr[$k]['filename'] = $this->upload->data('file_name');
                                }

                            }
                            if(count($arr) > 0){
                                $this->db->insert_batch('trans_pelabuhans_hasil_foto',$arr);
                            }
                        }
                        // print_r($pathfile);
                        // die();
                    }
                    redirect('backend/pelabuhan/edit/edit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_pelabuhan'));
                }
            }   
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Terjadi Kesalahan Sistem Silahkan Upload Ulang '.$e->getMessage());
            $this->load->helper('url');
            redirect('backend/pelabuhan/edit/edit/'.$this->input->post('id_jenis_aspek').'/'.$this->input->post('id_pelabuhan'));
        }
    }

    public function delete(){
        $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
                    
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                    $input = $this->input->post('url_canvas');
                    $output = 'images/pelabuhan/files'.date('YmdHis').'.png';
                    file_put_contents($output, file_get_contents($input));

                    $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'trans_pelabuhans_hasil');
                    $cekPC = $this->m_model->selectOneWhere2('id_pelabuhan',$selectOne->id_pelabuhan,'id_jenis_aspek',$selectOne->id_jenis_aspek,'pelabuhans_canvas');
                    if($cekPC){
                        $this->m_model->updateas('id', $cekPC['id'], ['url_canvas' => $output], 'pelabuhans_canvas');
                    }else{
                        $this->m_model->create(['url_canvas' => $output,'id_pelabuhan' => $selectOne->id_pelabuhan,'id_jenis_aspek' => $selectOne->id_jenis_aspek], 'pelabuhans_canvas');
                    }

                    $this->m_model->updateas('id', $this->input->post('id_pelabuhan'), ['url_canvas' => $output], 'pelabuhans');

                    $this->db->delete('trans_pelabuhans_hasil', array('id' => $this->input->post('id')));
                    header('Content-Type: application/json');
                    echo json_encode([
                        'status' => true,
                        'message' => 'Sukses Menghapus Data'
                    ]);
            }else{
                $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'trans_pelabuhans_hasil');
                $saveArr = array(
                    'trans_id' => $selectOne->id,
                    'id_pelabuhan' => $selectOne->id_pelabuhan,
                    'id_jenis_aspek' => $selectOne->id_jenis_aspek,
                    'id_sub_jenis_aspek' => $selectOne->id_sub_jenis_aspek,
                    'icon_id' => $selectOne->icon_id,
                    'url' => $selectOne->url,
                    'pointer_x' => $selectOne->pointer_x,
                    'pointer_y' => $selectOne->pointer_y,
                    'primary_key' => $selectOne->primary_key,
                    'kategori' => $selectOne->kategori,
                    'nama' => $selectOne->nama,
                    'aspek' => $selectOne->aspek,
                    'nomor' => $selectOne->nomor,
                    'kondisi' => $selectOne->kondisi,
                    'posisi' => $selectOne->posisi,
                    'tahun' => $selectOne->tahun,
                    'width' => $selectOne->width,
                    'height' => $selectOne->height,
                    'total_icon' => $selectOne->total_icon,
                    'status' => 2,
                );
                $this->m_model->updateas('id', $this->input->post('id'), array('status' => 2), 'trans_pelabuhans_hasil');

                $saveArr['flag']        = 'Delete';
                $saveArr['created_at']  = date('Y-m-d H:i:s');
                $saveArr['created_by']  = cleartext($this->session->userdata('admin_data')->id);
                $saveArr['fulluri']     = base_url('backend/pelabuhan/edit/edit/'.$selectOne->id_jenis_aspek.'/'.$selectOne->id_pelabuhan);

                $this->m_model->create($saveArr, 'trans_pelabuhans_hasil_log');
                // $this->m_model->updateas4(
                //     'id_pelabuhan', $selectOne->id_pelabuhan, 
                //     'id_jenis_aspek', $selectOne->id_jenis_aspek, 
                //     'id_sub_jenis_aspek', $selectOne->id_sub_jenis_aspek, 
                //     'icon_id', $selectOne->icon_id, 
                //     array('total_icon' => $selectOne->total_icon), 'trans_pelabuhans_hasil_log');
                
                $cekPelabuhans = $this->m_model->selectOne('id',$selectOne->id_pelabuhan,'pelabuhans');
                $dataMailCab = $this->m_model->selectOne('id',$cekPelabuhans->cabang_id,'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Hapus Data - Hasil Pelabuhan',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }

                header('Content-Type: application/json');
                echo json_encode([
                    'status' => true,
                    'message' => 'Sukses Menghapus Data, Silahkan Tunggu Approval Dari Admin'
                ]);
            }
    }

     public function subtitle(){
        $cekData = $this->m_model->selectOneWhere3('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('idaspek'),'id_icon',$this->input->post('id_icon'),'trans_pelabuhans_hasil_sub');
        if($cekData){
            $data = array(
                'id_pelabuhan' => $this->input->post('id_pelabuhan') ,
                'id_jenis_aspek' => $this->input->post('idaspek') ,
                'id_icon' => $this->input->post('id_icon') ,
                'title' => $this->input->post('title')
            );
            $this->m_model->updateas('id', $cekData->id, $data, 'trans_pelabuhans_hasil_sub');
        }else{
            $data = array(
                'id_pelabuhan' => $this->input->post('id_pelabuhan') ,
                'id_jenis_aspek' => $this->input->post('idaspek') ,
                'id_icon' => $this->input->post('id_icon') ,
                'title' => $this->input->post('title')
            );
            $this->m_model->create($data, 'trans_pelabuhans_hasil_sub');
        }
    }

    public function subvalue(){
        $cekData = $this->m_model->selectOneWhere3('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('idaspek'),'id_icon',$this->input->post('id_icon'),'trans_pelabuhans_hasil_sub');
        if($cekData){
            $data = array(
                'id_pelabuhan' => $this->input->post('id_pelabuhan') ,
                'id_jenis_aspek' => $this->input->post('idaspek') ,
                'id_icon' => $this->input->post('id_icon') ,
                'value' => $this->input->post('value')
            );
            $this->m_model->updateas('id', $cekData->id, $data, 'trans_pelabuhans_hasil_sub');
        }else{
            $data = array(
                'id_pelabuhan' => $this->input->post('id_pelabuhan') ,
                'id_jenis_aspek' => $this->input->post('idaspek') ,
                'id_icon' => $this->input->post('id_icon') ,
                'value' => $this->input->post('value')
            );
            $this->m_model->create($data, 'trans_pelabuhans_hasil_sub');
        }
    }

    public function laporan($jenisAspek, $id){
        
        $data = array(
            "data" => array(
                "judul" => 'PELABUHAN',
                "record" => $this->m_model->selectOne('id',$id,'pelabuhans'),
            )
        );
        $context = stream_context_create(array(
          'ssl' => array(
            'verify_peer' => FALSE,
            'verify_peer_name' => FALSE,
            // 'allow_self_signed'=> TRUE
          )
        ));

        $options = new Options();
        // $options->set('isRemoteEnabled',true);      
        $options->set('defaultFont', 'Courier');
        $options->set('isRemoteEnabled', TRUE);
        $options->set('enable_remote', TRUE);
        $options->set('debugKeepTemp', TRUE);
        $options->set('isHtml5ParserEnabled', TRUE);
        $options->set('isPhpEnabled', TRUE);
        $dompdf = new Dompdf( $options );

        $dompdf->setPaper('A4', 'potrait');
        $dompdf->set_option('isRemoteEnabled', TRUE);
        $dompdf->filename = 'laporan'.$this->m_model->selectOne('id',$id,'pelabuhans')->name.".pdf";
        $html = $this->load->view('backend/pelabuhan-laporan', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->setHttpContext($context);
        
        // Render the PDF
        $dompdf->set_base_path(base_url());
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream($dompdf->filename, array("Attachment" => false));
        header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
    }

    public function getDataNum(){
        $record = $this->m_model->selectas2('id_pelabuhan',$this->input->post('id_pelabuhan'),'id_jenis_aspek',$this->input->post('id_jenis_aspek'),'trans_pelabuhans_hasil');
        header('Content-Type: application/json');
        echo json_encode(count($record)+1);
    }

    public function getDataNumWithUrl(){
        $record = $this->m_model->selectas4(
            'id_pelabuhan',$this->input->post('id_pelabuhan'),
            'id_jenis_aspek',$this->input->post('id_jenis_aspek'),
            'id_sub_jenis_aspek',$this->input->post('id_sub_jenis_aspek'),
            'icon_id',$this->input->post('icon_id'),
        'trans_pelabuhans_hasil');
        // $recordIcon = $this->m_model->selectOne('path_file',$this->input->post('itemUrlCopy'),'icon');

        header('Content-Type: application/json');
        echo json_encode([
            'count' => count($record)+1,
            // 'icons' => $recordIcon
        ]);
    }
}