<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_model');
    }

    public function index(){
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/slider',[
                'title' => 'Slider',
                'bcrumb' => 'Master Data > Slider'
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function create(){
        $this->load->view('backend/slider-create',[
            'title' => 'Slider',
            'bcrumb' => 'Master Data > Slider'
        ]);
    }

    public function store(){
    	$pathfile=NULL;
    	$param['name'] = $this->input->post('name');
        if (!empty($_FILES['cover']['name'])) {

            if(is_dir('images/sliders/'.date("d_m_Y"))){
                $namePathCover = 'images/sliders/'.date("d_m_Y").'/';
            }else{
                mkdir('images/sliders/',0777);
                mkdir('images/sliders/'.date("d_m_Y"),0777);
                $namePathCover = 'images/sliders/'.date("d_m_Y").'/';
            }

            $config['upload_path']   = FCPATH.$namePathCover;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 50000000;
            $config['min_size'] = 500;
            
            $config['file_name'] = uniqid();
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('cover')) {
                $pathfile=$namePathCover.$this->upload->data('file_name');
                $param['url']=$pathfile;
            }else{
                if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                    $this->session->set_flashdata('error', 'Slider '.$this->upload->display_errors());
                    $this->load->helper('url');
                    redirect('backend/slider/create', 'refresh'); 
                }
            }
        }

        $create=$this->m_model->insertgetid($param, 'slider');
        $this->session->set_flashdata('sukses', 'Sukses Menyimpan Data');
        $this->load->helper('url');
        redirect('backend/slider', 'refresh');
    }

    public function edit($id){
    	$record = $this->m_model->selectOne('id',$id,'slider');
    	$this->load->view('backend/slider-edit',[
            'title' => 'Slider',
            'bcrumb' => 'Master Data > Slider',
            'record' => $record
        ]);
    }

    public function update($id){
    	$pathfile=NULL;
    	$param['name'] = $this->input->post('name');
        if (!empty($_FILES['cover']['name'])) {

            if(is_dir('images/sliders/'.date("d_m_Y"))){
                $namePathCover = 'images/sliders/'.date("d_m_Y").'/';
            }else{
                mkdir('images/sliders/',0777);
                mkdir('images/sliders/'.date("d_m_Y"),0777);
                $namePathCover = 'images/sliders/'.date("d_m_Y").'/';
            }

            $config['upload_path']   = FCPATH.$namePathCover;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 50000000;
            $config['min_size'] = 500;
            
            $config['file_name'] = uniqid();
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('cover')) {
                $pathfile=$namePathCover.$this->upload->data('file_name');
                $param['url']=$pathfile;
            }else{
                if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                    $this->session->set_flashdata('error', 'Slider '.$this->upload->display_errors());
                    $this->load->helper('url');
                    redirect('backend/slider/edit/'.$this->input->post('id'), 'refresh'); 
                }
            }
        }

        $create=$this->m_model->updateas('id', $id, $param, 'slider');
        $this->session->set_flashdata('sukses', 'Sukses Mengubah Data');
        $this->load->helper('url');
        redirect('backend/slider', 'refresh');
    }

    public function destroy($id){
        $delete=$this->m_model->deleteas('id', $id, 'slider');
        $this->session->set_flashdata('sukses', 'Sukses Menghapus Data');
        $this->load->helper('url');
        redirect('backend/slider', 'refresh');
    }
}