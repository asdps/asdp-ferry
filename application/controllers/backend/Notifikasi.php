<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_model');
    }

    public function index(){
        if ($this->session->userdata('admin')) {
            $array = [];
            
            // print_r($this->session->userdata('admin_data')->roles);
            // die();
            if(($this->session->userdata('admin_data')->roles == 1) || ($this->session->userdata('admin_data')->roles == 2)){
                $trueAdm = 'salah';
                $photo = $this->m_model->all('photo_log');
                if(count($photo) > 0){
                    foreach ($photo as $k => $value) {
                        array_push($array,$value);
                    }
                }

                $video = $this->m_model->all('video_log');
                if(count($video) > 0){
                    foreach ($video as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $armada = $this->m_model->all('armada_log');
               
                if(count($armada) > 0){
                    foreach ($armada as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhan = $this->m_model->all('pelabuhans_log');
                if(count($pelabuhan) > 0){
                    foreach ($pelabuhan as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhanHasil = $this->m_model->all('trans_pelabuhans_hasil_log');
                if(count($pelabuhanHasil) > 0){
                    foreach ($pelabuhanHasil as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $armadaHasil = $this->m_model->all('trans_armada_hasil_log');
                if(count($armadaHasil) > 0){
                    foreach ($armadaHasil as $k => $value) {
                        array_push($array,$value);   
                    }
                }
            }else{
                $trueAdm = 'benar';
                $photo = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'photo_log');
                if(count($photo) > 0){
                    foreach ($photo as $k => $value) {
                        array_push($array,$value);
                    }
                }

                $video = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'video_log');
                if(count($video) > 0){
                    foreach ($video as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $armada = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'armada_log');
                if(count($armada) > 0){
                    foreach ($armada as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhan = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'pelabuhans_log');
                if(count($pelabuhan) > 0){
                    foreach ($pelabuhan as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhanHasil = $this->m_model->all('trans_pelabuhans_hasil_log');
                if(count($pelabuhanHasil) > 0){
                    foreach ($pelabuhanHasil as $k => $value) {
                        $selOne = $this->m_model->selectOne('id',$value->id_pelabuhan,'pelabuhans');
                        if($selOne->cabang_id == $this->session->userdata('admin_data')->id_cabang){
                            array_push($array,$value);   
                        }
                    }
                }

                $ArmadaHasil = $this->m_model->all('trans_armada_hasil_log');
                if(count($ArmadaHasil) > 0){
                    foreach ($ArmadaHasil as $k => $value) {
                        $selOne = $this->m_model->selectOne('id',$value->id_armada,'armada');
                        if($selOne->cabang_id == $this->session->userdata('admin_data')->id_cabang){
                            array_push($array,$value);   
                        }
                    }
                }
            }
            // print_r($array);
            // die();
            $this->load->view('backend/notifikasi',[
                'title' => 'Notifikasi',
                'bcrumb' => 'Notifikasi',
                'record' => $array,
                'trueAdm' => $trueAdm,
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function approve($id,$form,$flag){
        $data = array(
            'status'    => 3,
        );

        if($form == 'Photo'){
            $create = $this->m_model->updateas('id', $id, $data, 'photo_log');
            $selectOnes = $this->m_model->selectOne('id', $id,'photo_log');
            $dataUp = array(
                'item' => $selectOnes->item,
                'status' => $selectOnes->status,
                'cabang_id' => $selectOnes->cabang_id,
                'deskripsi' => $selectOnes->deskripsi,
                'path_file' => $selectOnes->path_file,
            );
            $createUp = $this->m_model->updateas('id', $selectOnes->trans_id, $dataUp, 'photo');
        }elseif($form == 'Video'){
            $create = $this->m_model->updateas('id', $id, $data, 'video_log');
            $selectOnes = $this->m_model->selectOne('id', $id,'video_log');
            $dataUp = array(
                'filename' => $selectOnes->filename,
                'item' => $selectOnes->item,
                'status' => $selectOnes->status,
                'cabang_id' => $selectOnes->cabang_id,
                'deskripsi' => $selectOnes->deskripsi,
                'path_file' => $selectOnes->path_file,
            );
            $createUp = $this->m_model->updateas('id', $selectOnes->trans_id, $dataUp, 'video');
        }elseif($form == 'Pelabuhan'){
            $create = $this->m_model->updateas('id', $id, $data, 'pelabuhans_log');
            $selectOnes = $this->m_model->selectOne('id', $id,'pelabuhans_log');

            $dataUp = array(
                'name' => $selectOnes->item,
                'status' => $selectOnes->status,
                'cabang_id' => $selectOnes->cabang_id,
                'deskripsi' => $selectOnes->deskripsi,
                'foto' => $selectOnes->path_file,
                'simetris' => $selectOnes->simetris,
                'foto_drag' => $selectOnes->foto_drag,
            );
            $createUp = $this->m_model->updateas('id', $selectOnes->trans_id, $dataUp, 'pelabuhans');

        }elseif($form == 'Armada'){
            $create = $this->m_model->updateas('id', $id, $data, 'armada_log');
            $selectOnes = $this->m_model->selectOne('id', $id,'armada_log');

            $dataUp = array(
                'name' => $selectOnes->item,
                'status' => $selectOnes->status,
                'cabang_id' => $selectOnes->cabang_id,
                'deskripsi' => $selectOnes->deskripsi,
                'foto' => $selectOnes->path_file,
                'simetris' => $selectOnes->simetris,
            );
            $createUp = $this->m_model->updateas('id', $selectOnes->trans_id, $dataUp, 'armada');
        }elseif($form == 'Hasil_Armada'){
            $create = $this->m_model->updateas('id', $id, $data, 'trans_armada_hasil_log');
            $selectOnes = $this->m_model->selectOne('id', $id,'trans_armada_hasil_log');
            $selectOness = $this->m_model->selectOne('id', $id,'trans_armada_hasil_log');
            $trans_id = $selectOness->trans_id;
            unset($selectOness->flag);
            unset($selectOness->form_type);
            unset($selectOness->fulluri);
            unset($selectOness->noted);
            unset($selectOness->trans_id);
            // print_r($selectOness->trans_id);
            // die();
            $createUp = $this->m_model->updateas('id', $trans_id, $selectOness, 'trans_armada_hasil');
        }elseif($form == 'Hasil_Pelabuhan'){
            $create = $this->m_model->updateas('id', $id, $data, 'trans_pelabuhans_hasil_log');
            $selectOnes = $this->m_model->selectOne('id', $id,'trans_pelabuhans_hasil_log');
            $selectOness = $this->m_model->selectOne('id', $id,'trans_pelabuhans_hasil_log');
            $trans_id = $selectOness->trans_id;
            unset($selectOness->flag);
            unset($selectOness->form_type);
            unset($selectOness->fulluri);
            unset($selectOness->noted);
            unset($selectOness->cabang_id);
            unset($selectOness->trans_id);
            $createUp = $this->m_model->updateas('id', $trans_id, $selectOness, 'trans_pelabuhans_hasil');
        }
        
        // $transApprove = $this->m_model->selectOne('id',$id,'trans_approval');
        $usrId = $this->m_model->selectOne('id',$selectOnes->created_by,'users');
        $cab = $this->m_model->selectOne('id',$usrId->id_cabang,'cabangs');
        $dataJ = array(
            "PIC Peminta" => $usrId->username,
            "email" => $usrId->email,
            "Cabang" => $cab->name,
            "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
            "Status" => "Approved",
            "pesan" => "Ada Approval Terbaru Untuk Anda"
        );
        sendEMAIL($dataJ);
        $admSuper = $this->m_model->selectOne('roles','1','users');
        $admCab = $this->m_model->selectOne('roles','2','users');
        $collAdm = [$admSuper->email,$admCab->email];
        if(count($collAdm) > 0){
            foreach ($collAdm as $value) {
                $dataJ = array(
                    "PIC Peminta" => $usrId->username,
                    "email" => $value,
                    "Cabang" => $cab->name,
                    "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                    "Status" => "Approved",
                    "pesan" => "Ada Approval Terbaru Untuk Anda"
                );
                sendEMAIL($dataJ);
            }
        }

        if ($create == 1) {
            redirect('backend/notifikasi', 'refresh');
        }else{
            // $this->session->set_flashdata('gagal', 'Kesalahan Approve');
            redirect('backend/notifikasi', 'refresh');
        }
    }

    public function reject($id,$form,$flag){
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/notifikasi-reject',[
                'title' => 'Notifikasi',
                'bcrumb' => 'Notifikasi',
                'id' => $id,
                'form' => $form,
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function saveReject(){
        $data = array(
            'status'    => 4,
            'noted'    => $this->input->post('deskripsi'),
        );

        if($this->input->post('form') == 'Photo'){
            $create = $this->m_model->updateas('id', $this->input->post('id'), $data, 'photo_log');
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'photo_log');
        }elseif($this->input->post('form') == 'Video'){
            $create = $this->m_model->updateas('id', $this->input->post('id'), $data, 'video_log');
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'video_log');
        }elseif($this->input->post('form') == 'Pelabuhan'){
            $create = $this->m_model->updateas('id', $this->input->post('id'), $data, 'pelabuhans_log');
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'pelabuhans_log');
        }elseif($this->input->post('form') == 'Armada'){
            $create = $this->m_model->updateas('id', $this->input->post('id'), $data, 'armada_log');
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'armada_log');
        }elseif($this->input->post('form') == 'Hasil_Armada'){
            $create = $this->m_model->updateas('id', $this->input->post('id'), $data, 'trans_armada_hasil_log');
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'trans_armada_hasil_log');
        }elseif($this->input->post('form') == 'Hasil_Pelabuhan'){
            $create = $this->m_model->updateas('id', $this->input->post('id'), $data, 'trans_pelabuhans_hasil_log');
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'trans_pelabuhans_hasil_log');
        }

        // $transApprove = $this->m_model->selectOne('id',$id,'trans_approval');
        $usrId = $this->m_model->selectOne('id',$selectOnes->created_by,'users');
        $cab = $this->m_model->selectOne('id',$usrId->id_cabang,'cabangs');
        $dataJ = array(
            "PIC Peminta" => $usrId->username,
            "email" => $usrId->email,
            "Cabang" => $cab->name,
            "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
            "Status" => "Rejected",
            "Keterangan" => $selectOnes->deskripsi,
            "pesan" => "Ada Approval Yang Di Reject"
        );
        sendEMAIL($dataJ);
        $admSuper = $this->m_model->selectOne('roles','1','users');
        $admCab = $this->m_model->selectOne('roles','2','users');
        $collAdm = [$admSuper->email,$admCab->email];
        if(count($collAdm) > 0){
            foreach ($collAdm as $value) {
                $dataJ = array(
                    "PIC Peminta" => $usrId->username,
                    "email" => $value,
                    "Cabang" => $cab->name,
                    "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                    "Status" => "Rejected",
                    "Keterangan" => $selectOnes->deskripsi,
                    "pesan" => "Ada Approval Yang Di Reject"
                );
                sendEMAIL($dataJ);
            }
        }

        // $create = $this->m_model->updateas('id', $id, $data, 'trans_approval');
        if ($create == 1) {
            redirect('backend/notifikasi', 'refresh');
        }else{
            redirect('backend/notifikasi', 'refresh');
        }
    }

    public function delete($id,$form,$flag){
        if($form == 'Photo'){
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'photo_log');
            
            $usrId = $this->m_model->selectOne('id',$selectOnes->created_by,'users');
            $cab = $this->m_model->selectOne('id',$usrId->id_cabang,'cabangs');
            $dataJ = array(
                "PIC Peminta" => $usrId->username,
                "email" => $usrId->email,
                "Cabang" => $cab->name,
                "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                "Status" => "Deleted",
                "Keterangan" => $selectOnes->deskripsi,
                "pesan" => "Ada Approval Yang Di Hapus"
            );
            sendEMAIL($dataJ);
            $admSuper = $this->m_model->selectOne('roles','1','users');
            $admCab = $this->m_model->selectOne('roles','2','users');
            $collAdm = [$admSuper->email,$admCab->email];
            if(count($collAdm) > 0){
                foreach ($collAdm as $value) {
                    $dataJ = array(
                        "PIC Peminta" => $usrId->username,
                        "email" => $value,
                        "Cabang" => $cab->name,
                        "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                        "Status" => "Deleted",
                        "Keterangan" => $selectOnes->deskripsi,
                        "pesan" => "Ada Approval Yang Di Hapus"
                    );
                    sendEMAIL($dataJ);
                }
            }
            $create = $this->m_model->destroy($id,'photo_log');
        }elseif($form == 'Video'){
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'video_log');
            $usrId = $this->m_model->selectOne('id',$selectOnes->created_by,'users');
            $cab = $this->m_model->selectOne('id',$usrId->id_cabang,'cabangs');
            $dataJ = array(
                "PIC Peminta" => $usrId->username,
                "email" => $usrId->email,
                "Cabang" => $cab->name,
                "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                "Status" => "Deleted",
                "Keterangan" => $selectOnes->deskripsi,
                "pesan" => "Ada Approval Yang Di Hapus"
            );
            sendEMAIL($dataJ);
            $admSuper = $this->m_model->selectOne('roles','1','users');
            $admCab = $this->m_model->selectOne('roles','2','users');
            $collAdm = [$admSuper->email,$admCab->email];
            if(count($collAdm) > 0){
                foreach ($collAdm as $value) {
                    $dataJ = array(
                        "PIC Peminta" => $usrId->username,
                        "email" => $value,
                        "Cabang" => $cab->name,
                        "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                        "Status" => "Deleted",
                        "Keterangan" => $selectOnes->deskripsi,
                        "pesan" => "Ada Approval Yang Di Hapus"
                    );
                    sendEMAIL($dataJ);
                }
            }
            $create = $this->m_model->destroy($id,'video_log');
        }elseif($form == 'Pelabuhan'){
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'pelabuhans_log');
            $usrId = $this->m_model->selectOne('id',$selectOnes->created_by,'users');
            $cab = $this->m_model->selectOne('id',$usrId->id_cabang,'cabangs');
            $dataJ = array(
                "PIC Peminta" => $usrId->username,
                "email" => $usrId->email,
                "Cabang" => $cab->name,
                "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                "Status" => "Deleted",
                "Keterangan" => $selectOnes->deskripsi,
                "pesan" => "Ada Approval Yang Di Hapus"
            );
            sendEMAIL($dataJ);
            $admSuper = $this->m_model->selectOne('roles','1','users');
            $admCab = $this->m_model->selectOne('roles','2','users');
            $collAdm = [$admSuper->email,$admCab->email];
            if(count($collAdm) > 0){
                foreach ($collAdm as $value) {
                    $dataJ = array(
                        "PIC Peminta" => $usrId->username,
                        "email" => $value,
                        "Cabang" => $cab->name,
                        "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                        "Status" => "Deleted",
                        "Keterangan" => $selectOnes->deskripsi,
                        "pesan" => "Ada Approval Yang Di Hapus"
                    );
                    sendEMAIL($dataJ);
                }
            }
            $create = $this->m_model->destroy($id,'pelabuhans_log');
        }elseif($form == 'Armada'){
            $selectOnes = $this->m_model->selectOne('id', $this->input->post('id'),'armada_log');
            $usrId = $this->m_model->selectOne('id',$selectOnes->created_by,'users');
            $cab = $this->m_model->selectOne('id',$usrId->id_cabang,'cabangs');
            $dataJ = array(
                "PIC Peminta" => $usrId->username,
                "email" => $usrId->email,
                "Cabang" => $cab->name,
                "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                "Status" => "Deleted",
                "Keterangan" => $selectOnes->deskripsi,
                "pesan" => "Ada Approval Yang Di Hapus"
            );
            sendEMAIL($dataJ);
            $admSuper = $this->m_model->selectOne('roles','1','users');
            $admCab = $this->m_model->selectOne('roles','2','users');
            $collAdm = [$admSuper->email,$admCab->email];
            if(count($collAdm) > 0){
                foreach ($collAdm as $value) {
                    $dataJ = array(
                        "PIC Peminta" => $usrId->username,
                        "email" => $value,
                        "Cabang" => $cab->name,
                        "Tipe Permintaan" => $selectOnes->flag.' - '.$selectOnes->form_type,
                        "Status" => "Deleted",
                        "Keterangan" => $selectOnes->deskripsi,
                        "pesan" => "Ada Approval Yang Di Hapus"
                    );
                    sendEMAIL($dataJ);
                }
            }
            $create = $this->m_model->destroy($id,'armada_log');
        }
        // $this->m_model->destroy($id,'trans_approval');
        redirect('backend/notifikasi', 'refresh');
    }
}