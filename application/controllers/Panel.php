<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('m_model');
        
        if($this->session->userdata('language') !=null){
            $lang_active=$this->session->userdata('language');
            $this->lang->load('caption', $lang_active);
        }
        else{
            $this->lang->load('caption', 'bahasa');
        }
    }

    protected $_ci;    
    protected $email_pengirim = 'dbfasilitas@indonesiaferry.co.id'; 
    protected $nama_pengirim = 'Fasilitas ASDP Indonesia Ferry';
    protected $password = '@5dp-123';
        
    public function send(){        
        $datak['email'] = 'adriyanaputra017@gmail.com';
        $datak['emails'] = 'adriyanaputra017@gmail.com';  
        sendEMAIL($datak);  
    }

    public function index() {
        if ($this->session->userdata('admin')) {
            $slider = $this->m_model->select('slider');
            $this->load->view('backend/home',[
                'slider' => $slider
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
    }
   
    public function login() {
        if ($this->session->userdata('admin')) {
            redirect('panel', 'refresh');
        } else {
            $this->load->view('backend/login');
        }

        $checkAcc = $this->m_model->selectOne(
            'username',cleartext($this->input->post('email')),
            'users'
        );

        $Pas = ($this->input->post('password')) ? $this->input->post('password') : '';
        $CkPas = isset($checkAcc->password) ? $checkAcc->password : '';
        $checkPass = password_verify($Pas, $CkPas);
            // print_r($checkPass);
            // die;
        
        $data = array(
            'email'    => $this->input->post('email'),
            'password' => $this->input->post('password'),
        );

        if(($checkPass == 1) && ($checkAcc->blocked > 3)){
            $sess_data = array(
            'status' => 'blocked'
            );
            $this->session->set_userdata($sess_data);
            redirect('panel', 'refresh');
        }else{
            if (($checkAcc) && ($checkPass == 1)) {
                $this->session->unset_userdata('admin');
                $this->session->unset_userdata('admin_data');
                $this->m_model->loginadmin($data);
                redirect('panel', 'refresh');
            }else{
                if($checkAcc){
                    $this->m_model->updateas(
                        'id', 
                        $checkAcc->id, 
                        ['blocked' => $checkAcc->blocked+1], 
                        'users'
                    );
                    $sess_data = array(
                    'status' => 'failedLogin'
                    );

                    $this->session->set_userdata($sess_data);
                }else{
                    $sess_data = array(
                    'status' => 'failedLogin'
                    );

                    $this->session->set_userdata($sess_data);
                }
            }
        }
    }

    public function logout() {
        $this->session->unset_userdata('admin');
        $this->session->unset_userdata('admin_data');
        redirect('panel/login', 'refresh');
    }

/*---------------------------------------------------------------ASDP----------------------------*/
    public function users() {
        // if ($this->session->userdata('admin') &&  $this->session->userdata('admin_data')->roles==5) {
            $this->load->view('backend/users',['title' => 'Users']);
        // } else {
            // redirect('panel/login', 'refresh');
        // }
        //---
        if ($this->input->post('add')) {
            // print_r($this->input->post());
            // die();
            $seo = str_replace('--', '-', str_replace('--', '-', strtolower(str_replace(' ', '-',
                preg_replace('/[^A-Za-z0-9\-]/', ' ', $this->input->post('name'))))));
            $idCab = NULL;

            if($this->input->post('id_cabang') != null){
                $idCab = $this->input->post('id_cabang');
            }

            $pathfile=NULL;
            if (!empty($_FILES['foto_user']['name'])) {
                $config['upload_path']   = FCPATH.'images/file/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                
                // $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('foto_user')) {
                    $pathfile='images/file/'.$this->upload->data('file_name');
                    $data['foto_user']=$pathfile;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/users?add=true', 'refresh'); 
                    }
                }
            }

            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $data['roles'] = $this->input->post('roles');
            $data['id_cabang'] = $idCab;
            // print_r($data);
            // die();
            /*if (!empty($_FILES['logo']['name'])) {
                $config['upload_path']   = FCPATH.'/assets/frontend/img/users/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 100;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('logo')){
                    $data['logo']='assets/frontend/img/users/'.$this->upload->data('file_name');

                    $config_kompres['image_library'] = 'gd2';
                    $config_kompres['source_image'] = $this->upload->data('full_path');
                    $config_kompres['new_image'] = FCPATH.'assets/frontend/img/brands/compress_'.$this->upload->data('file_name');
                    $config_kompres['maintain_ratio'] = TRUE;
                    $config_kompres['width']         = 110;

                    $this->load->library('image_lib', $config_kompres);
                    if($this->image_lib->resize()){
                        $data['thumbnail_logo']='assets/frontend/img/brands/compress_'.$this->upload->data('file_name');
                    }
                }
            }*/
             $dataJ = array(
                    "email" => $this->input->post('email'),
                    "username" => $this->input->post('username'),
                    "password" => $this->input->post('password'),
                    "pesan" => "Email Registrasi Baru Untuk Anda"
                );
            if ($this->m_model->create($data, 'users') == 1) {
                sendEMAIL($dataJ);
                redirect('panel/users', 'refresh');
            } else {
                redirect('panel/users', 'refresh');
            }
        }

        if ($this->input->post('save')) {
            $seo = str_replace('--', '-', str_replace('--', '-', strtolower(str_replace(' ', '-',
                preg_replace('/[^A-Za-z0-9\-]/', ' ', $this->input->post('name'))))));

             $idCab = NULL;

            if(!is_null($this->input->post('id_cabang'))){
                $idCab = $this->input->post('id_cabang');
            }

            $pathfile=NULL;
            if (!empty($_FILES['foto_user']['name'])) {
                $config['upload_path']   = FCPATH.'images/file/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                
                // $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('foto_user')) {
                    $pathfile='images/file/'.$this->upload->data('file_name');
                    $data['foto_user']=$pathfile;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/users?edit='.$this->input->post('id'), 'refresh'); 
                    }
                }
            }

            if($this->input->post('password')){
                $data['username'] = $this->input->post('username');
                $data['email'] = $this->input->post('email');
                $data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $data['roles'] = $this->input->post('roles');
                $data['id_cabang'] = $idCab;
                $dataJ = array(
                    "email" => $this->input->post('email'),
                    "username" => $this->input->post('username'),
                    "password" => $this->input->post('password'),
                    "pesan" => "Ada Perubahan Akun Baru Untuk Anda"
                );
                // sendEMAIL($dataJ);
            }else{
                $data['username'] = $this->input->post('username');
                $data['email'] = $this->input->post('email');
                $data['roles'] = $this->input->post('roles');
                $data['id_cabang'] = $idCab;

                $dataJ = array(
                    "email" => $this->input->post('email'),
                    "username" => $this->input->post('username'),
                    "pesan" => "Ada Perubahan Akun Baru Untuk Anda"
                );
                    // sendEMAIL($dataJ);
            }
            
            /*if (!empty($_FILES['logo']['name'])) {
                $config['upload_path']   = FCPATH.'/assets/frontend/img/brands/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 100;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('logo')){
                    $data['logo']='assets/frontend/img/brands/'.$this->upload->data('file_name');

                    $config_kompres['image_library'] = 'gd2';
                    $config_kompres['source_image'] = $this->upload->data('full_path');
                    $config_kompres['new_image'] = FCPATH.'assets/frontend/img/brands/compress_'.$this->upload->data('file_name');
                    $config_kompres['maintain_ratio'] = TRUE;
                    $config_kompres['width']         = 110;

                    $this->load->library('image_lib', $config_kompres);
                    if($this->image_lib->resize()){
                        $data['thumbnail_logo']='assets/frontend/img/brands/compress_'.$this->upload->data('file_name');
                        if($this->input->post('old_logo')!=null){
                            unlink(FCPATH.$this->input->post('old_logo'));
                        }
                        if($this->input->post('old_thumbnail_logo')!=null){
                            unlink(FCPATH.$this->input->post('old_thumbnail_logo'));
                        }
                    }
                }
            }*/

            $update = $this->m_model->updateas('id', $this->input->post('id'), $data,
                'users');
            if ($update == 1) {
                sendEMAIL($dataJ);
                redirect('panel/users', 'refresh');
            } else {
                redirect('panel/users', 'refresh');
            }
        }
        if ($this->input->get('remove')) {
            $this->db->delete('users', array('id' => $this->input->get('remove')));
            redirect('panel/users', 'refresh');
        }

        if ($this->input->get('unblock')) {
            $this->m_model->updateas('id', $this->input->get('unblock'), ['blocked' => 0], 'users');
            redirect('panel/users', 'refresh');
        }
    }

    public function pelabuhan() {
        if ($this->session->userdata('admin')) {
            if($this->input->get('nama')){
                if(isset($this->session->userdata('admin_data')->id_cabang) && ($this->session->userdata('admin_data')->id_cabang != 0)){
                    $query = "SELECT * FROM pelabuhans where cabang_id='".$this->session->userdata('admin_data')->id_cabang."' and name LIKE '%".$this->input->get('nama')."%'";  
                }else{
                    $query = "SELECT * FROM pelabuhans where name LIKE '%".$this->input->get('nama')."%'";  
                }
            }else{
                if(isset($this->session->userdata('admin_data')->id_cabang) && ($this->session->userdata('admin_data')->id_cabang != 0)){
                    $query = "SELECT * FROM pelabuhans where cabang_id='".$this->session->userdata('admin_data')->id_cabang."'";  
                }else{
                    $query = "SELECT * FROM pelabuhans";  
                }
            }
                 
            $this->load->library('pagination'); 
            $config['base_url'] = base_url('panel/pelabuhan');    
            $config['total_rows'] = $this->db->query($query)->num_rows();    
            $config['per_page'] = 12;    
            $config['uri_segment'] = 3;    

            $config['first_link']       = 'First';
            $config['last_link']        = 'Last';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link page-link-pelabuhan">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link page-link-pelabuhan">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link page-link-pelabuhan">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link page-link-pelabuhan">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link page-link-pelabuhan">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link page-link-pelabuhan">';
            $config['last_tagl_close']  = '</span></li>';
        
            $this->pagination->initialize($config); // Set konfigurasi paginationnya        
            $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
            $query .= " LIMIT ".$page.", ".$config['per_page'];
            $data['page'] = $page;
            $data['limit'] = $config['per_page'];
            $data['total_rows'] = $config['total_rows'];
            $data['pagination'] = $this->pagination->create_links();
            $data['siswa'] = $this->db->query($query)->result();
            $this->load->view('backend/pelabuhan',['title' => 'Pelabuhan', 'pageUrl' => base_url('panel/pelabuhan'), 'data' => $data]);
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $pathfile=NULL;
            if (!empty($_FILES['cover']['name'])) {

                if(is_dir('images/pelabuhan_cover/'.date("d_m_Y"))){
                    $namePathCover = 'images/pelabuhan_cover/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pelabuhan_cover')){
                        mkdir('images/pelabuhan_cover/',0777);
                    }
                    mkdir('images/pelabuhan_cover/'.date("d_m_Y"),0777);
                    $namePathCover = 'images/pelabuhan_cover/'.date("d_m_Y").'/';
                }

                $config['upload_path']   = FCPATH.$namePathCover;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                
                 // $config['min_width']  = '1000';
                // // $config['min_height']  = '450';
                $config['max_width']  = '5000';
                $config['max_height']  = '5000';
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('cover')) {
                    $pathfile=$namePathCover.$this->upload->data('file_name');
                    $param['foto']=$pathfile;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img Cover '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/pelabuhan?add=true', 'refresh'); 
                    }
                }
            }
            // print_r($param);
            // die();
            $pathSimet = NULL;
            if (!empty($_FILES['simetris']['name'])) {
                if(is_dir('images/pelabuhan_isometri/'.date("d_m_Y"))){
                    $namePathSimet = 'images/pelabuhan_isometri/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pelabuhan_isometri')){
                        mkdir('images/pelabuhan_isometri/',0777);
                    }
                    mkdir('images/pelabuhan_isometri/'.date("d_m_Y"),0777);
                    $namePathSimet = 'images/pelabuhan_isometri/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePathSimet;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();

                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('simetris')) {
                    $pathSimet=$namePathSimet.$this->upload->data('file_name');
                    $param['simetris'] = $pathSimet;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img isometri '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/pelabuhan?add=true', 'refresh'); 
                    }
                }
            }

            $pathFotoDrag = NULL;
            if (!empty($_FILES['foto_drag']['name'])) {
                if(is_dir('images/pelabuhan_drag/'.date("d_m_Y"))){
                    $namePathDrag = 'images/pelabuhan_drag/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pelabuhan_drag')){
                        mkdir('images/pelabuhan_drag/',0777);
                    }
                    mkdir('images/pelabuhan_drag/'.date("d_m_Y"),0777);
                    $namePathDrag = 'images/pelabuhan_drag/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePathDrag;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['max_width']  = '5000';
                $config['max_height']  = '5000';
                $config['file_name'] = uniqid();

                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('foto_drag')) {
                    $pathFotoDrag=$namePathDrag.$this->upload->data('file_name');
                    $param['foto_drag'] = $pathFotoDrag;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img drag'.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/pelabuhan?add=true', 'refresh'); 
                    }
                }
            }

            if (!empty($_FILES['path_zonasi']['name'])) {
                if(is_dir('images/pelabuhan_zonasi/'.date("d_m_Y"))){
                    $namePathZonasi = 'images/pelabuhan_zonasi/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pelabuhan_zonasi')){
                        mkdir('images/pelabuhan_zonasi/',0777);
                    }
                    mkdir('images/pelabuhan_zonasi/'.date("d_m_Y"),0777);
                    $namePathZonasi = 'images/pelabuhan_zonasi/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePathZonasi;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('path_zonasi')) {
                    $path_zonasi=$namePathZonasi.$this->upload->data('file_name');
                    $param['path_zonasi'] = $path_zonasi;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img Zonasi'.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/pelabuhan?add=true', 'refresh'); 
                    }
                }
            }

            if (!empty($_FILES['path_sirkular']['name'])) {
                if(is_dir('images/pelabuhan_sirkular/'.date("d_m_Y"))){
                    $namePathSirkular = 'images/pelabuhan_sirkular/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pelabuhan_sirkular')){
                        mkdir('images/pelabuhan_sirkular/',0777);
                    }
                    mkdir('images/pelabuhan_sirkular/'.date("d_m_Y"),0777);
                    $namePathSirkular = 'images/pelabuhan_sirkular/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePathSirkular;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('path_sirkular')) {
                    $path_sirkular=$namePathSirkular.$this->upload->data('file_name');
                    $param['path_sirkular'] = $path_sirkular;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img sirkular'.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/pelabuhan?add=true', 'refresh'); 
                    }
                }
            }

                $param['deskripsi_zonasi']  = $this->input->post('deskripsi_zonasi');
                $param['deskripsi_sirkular']  = $this->input->post('deskripsi_sirkular');
                $param['cabang_id']  = cleartext($this->input->post('cabang'));
                $param['name']     = cleartext($this->input->post('name'));
                $param['deskripsi']  = cleartext($this->input->post('deskripsi'));
                $param['created_at']   = date('Y-m-d H:i:s');
                $param['created_user']   = cleartext($this->session->userdata('admin_data')->username);
            // print_r($param);
            // die();
            $create=$this->m_model->insertgetid($param, 'pelabuhans');
            redirect('panel/pelabuhan', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                    $pathfile=NULL;
                    if (!empty($_FILES['cover']['name'])) {
                        if(is_dir('images/pelabuhan_cover/'.date("d_m_Y"))){
                            $namePathCover = 'images/pelabuhan_cover/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/pelabuhan_cover')){
                                mkdir('images/pelabuhan_cover/',0777);
                            }
                            mkdir('images/pelabuhan_cover/'.date("d_m_Y"),0777);
                            $namePathCover = 'images/pelabuhan_cover/'.date("d_m_Y").'/';
                        }
                        $config['upload_path']   = FCPATH.$namePathCover;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        $config['max_width']  = '5000';
                        $config['max_height']  = '5000';
                        $config['file_name'] = uniqid();
                        
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        // echo print_r('asd',$this->upload->display_errors());
                        // die();
                        if ($this->upload->do_upload('cover')) {
                            $pathfile=$namePathCover.$this->upload->data('file_name');
                            $param['foto'] = $pathfile;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img cover'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                        // echo '<pre>';
                        // print_r($this->load->library('upload',$config));
                        // echo '</pre>';
                    }
                    $pathSimet = NULL;
                    if (!empty($_FILES['simetris']['name'])) {
                        if(is_dir('images/pelabuhan_isometri/'.date("d_m_Y"))){
                            $namePathSimet = 'images/pelabuhan_isometri/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/pelabuhan_isometri')){
                                mkdir('images/pelabuhan_isometri/',0777);
                            }
                            mkdir('images/pelabuhan_isometri/'.date("d_m_Y"),0777);
                            $namePathSimet = 'images/pelabuhan_isometri/'.date("d_m_Y").'/';
                        }
                        $config['upload_path']   = FCPATH.$namePathSimet;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        // $config['min_width']  = '1000';
                        // $config['max_width']  = '2000';
                        // $config['min_height']  = '450';
                        // $config['max_height']  = '1500';
                        $config['file_name'] = uniqid();
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('simetris')) {
                            $pathSimet=$namePathSimet.$this->upload->data('file_name');
                            $param['simetris'] = $pathSimet;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img isometri'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id'), 'refresh'); 
                            }
                        }
                    }


                    // print_r($param);
                    // die();

                    $pathFotoDrag = NULL;
                    if (!empty($_FILES['foto_drag']['name'])) {
                        if(is_dir('images/pelabuhan_drag/'.date("d_m_Y"))){
                            $namePathDarag = 'images/pelabuhan_drag/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/pelabuhan_drag')){
                                mkdir('images/pelabuhan_drag/',0777);
                            }
                            mkdir('images/pelabuhan_drag/'.date("d_m_Y"),0777);
                            $namePathDarag = 'images/pelabuhan_drag/'.date("d_m_Y").'/';
                        }
                        $config['upload_path']   = FCPATH.$namePathDarag;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        $config['max_width']  = '5000';
                        $config['max_height']  = '5000';
                        // $config['min_width']  = '600';
                        // $config['max_width']  = '600';
                        // $config['min_height']  = '700';
                        // $config['max_height']  = '700';
                        $config['file_name'] = uniqid();
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('foto_drag')) {
                            $pathFotoDrag=$namePathDarag.$this->upload->data('file_name');
                            $param['foto_drag'] = $pathFotoDrag;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img drag'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                    }
                    
                    if (!empty($_FILES['path_zonasi']['name'])) {
                        if(is_dir('images/pelabuhan_zonasi/'.date("d_m_Y"))){
                            $namePathZonasi = 'images/pelabuhan_zonasi/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/pelabuhan_zonasi')){
                                mkdir('images/pelabuhan_zonasi/',0777);
                            }
                            mkdir('images/pelabuhan_zonasi/'.date("d_m_Y"),0777);
                            $namePathZonasi = 'images/pelabuhan_zonasi/'.date("d_m_Y").'/';
                        }
                        $config['upload_path']   = FCPATH.$namePathZonasi;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        // $config['min_width']  = '800';
                        // $config['max_width']  = '900';
                        // $config['min_height']  = '1200';
                        // $config['max_height']  = '1350';
                        $config['file_name'] = uniqid();
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('path_zonasi')) {
                            $path_zonasi=$namePathZonasi.$this->upload->data('file_name');
                            $param['path_zonasi'] = $path_zonasi;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img zonasi'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                    }

                    if (!empty($_FILES['path_sirkular']['name'])) {
                        if(is_dir('images/pelabuhan_sirkular/'.date("d_m_Y"))){
                            $namePathSirkular = 'images/pelabuhan_sirkular/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/pelabuhan_sirkular')){
                                mkdir('images/pelabuhan_sirkular/',0777);
                            }
                            mkdir('images/pelabuhan_sirkular/'.date("d_m_Y"),0777);
                            $namePathSirkular = 'images/pelabuhan_sirkular/'.date("d_m_Y").'/';
                        }

                        $config['upload_path']   = FCPATH.$namePathSirkular;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        // $config['min_width']  = '800';
                        // $config['max_width']  = '900';
                        // $config['min_height']  = '1200';
                        // $config['max_height']  = '1350';
                        $config['file_name'] = uniqid();
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('path_sirkular')) {
                            $path_sirkular=$namePathSirkular.$this->upload->data('file_name');
                            $param['path_sirkular'] = $path_sirkular;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img sirkular'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                    }

                    $param['deskripsi_zonasi']  = cleartext($this->input->post('deskripsi_zonasi'));
                    $param['deskripsi_sirkular']  = cleartext($this->input->post('deskripsi_sirkular'));
                    $param['cabang_id']  = cleartext($this->input->post('cabang'));
                    $param['name']     = cleartext($this->input->post('name'));
                    $param['deskripsi']  = cleartext($this->input->post('deskripsi'));
                    $param['status']   = 2;
                    // print_r($param);
                    // die();
                $this->m_model->updateas('id', $this->input->post('id'), $param, 'pelabuhans');
            }else{
                $pathSimet = NULL;
                if (!empty($_FILES['simetris']['name'])) {
                    if(is_dir('images/pelabuhan_isometri/'.date("d_m_Y"))){
                        $namePathSimet = 'images/pelabuhan_isometri/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/pelabuhan_isometri')){
                            mkdir('images/pelabuhan_isometri/',0777);
                        }
                        mkdir('images/pelabuhan_isometri/'.date("d_m_Y"),0777);
                        $namePathSimet = 'images/pelabuhan_isometri/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePathSimet;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    // $config['min_width']  = '1000';
                    // $config['max_width']  = '2000';
                    // $config['min_height']  = '450';
                    // $config['max_height']  = '1500';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('simetris')) {
                        $pathSimet=$namePathSimet.$this->upload->data('file_name');
                        $log['simetris'] = $pathSimet;
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img isometri'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                }

                $pathFotoDrag = NULL;
                if (!empty($_FILES['foto_drag']['name'])) {
                    if(is_dir('images/pelabuhan_drag/'.date("d_m_Y"))){
                        $namePathDrag = 'images/pelabuhan_drag/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/pelabuhan_drag')){
                            mkdir('images/pelabuhan_drag/',0777);
                        }
                        mkdir('images/pelabuhan_drag/'.date("d_m_Y"),0777);
                        $namePathDrag = 'images/pelabuhan_drag/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePathDrag;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['max_width']  = '5000';
                    $config['max_height']  = '5000';
                    // $config['min_width']  = '600';
                    // $config['max_width']  = '600';
                    // $config['min_height']  = '700';
                    // $config['max_height']  = '700';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('foto_drag')) {
                        $pathFotoDrag=$namePathDrag.$this->upload->data('file_name');
                        $log['foto_drag'] = $pathFotoDrag;
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img drag'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                }

                if (!empty($_FILES['path_zonasi']['name'])) {
                    if(is_dir('images/pelabuhan_zonasi/'.date("d_m_Y"))){
                        $namePathZonasi = 'images/pelabuhan_zonasi/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/pelabuhan_zonasi')){
                            mkdir('images/pelabuhan_zonasi/',0777);
                        }
                        mkdir('images/pelabuhan_zonasi/'.date("d_m_Y"),0777);
                        $namePathZonasi = 'images/pelabuhan_zonasi/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePathZonasi;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    // $config['min_width']  = '800';
                    // $config['max_width']  = '900';
                    // $config['min_height']  = '1200';
                    // $config['max_height']  = '1350';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('path_zonasi')) {
                        $path_zonasi=$namePathZonasi.$this->upload->data('file_name');
                        $log['path_zonasi'] = $path_zonasi;
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img zonasi'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                }

                if (!empty($_FILES['path_sirkular']['name'])) {
                    if(is_dir('images/pelabuhan_sirkular/'.date("d_m_Y"))){
                        $namePathSirkular = 'images/pelabuhan_sirkular/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/pelabuhan_sirkular')){
                            mkdir('images/pelabuhan_sirkular/',0777);
                        }
                        mkdir('images/pelabuhan_sirkular/'.date("d_m_Y"),0777);
                        $namePathSirkular = 'images/pelabuhan_sirkular/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePathSirkular;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    // $config['min_width']  = '800';
                    // $config['max_width']  = '900';
                    // $config['min_height']  = '1200';
                    // $config['max_height']  = '1350';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('path_sirkular')) {
                        $path_sirkular=$namePathSirkular.$this->upload->data('file_name');
                        $log['path_sirkular'] = $path_sirkular;
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img sirkular'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                }

                $log['deskripsi_zonasi']  = cleartext($this->input->post('deskripsi_zonasi'));
                $log['deskripsi_sirkular']  = cleartext($this->input->post('deskripsi_sirkular'));

                $pathfile=NULL;
                $getImg = false;
                $pathfile='';
                if (!empty($_FILES['cover']['name'])) {
                    if(is_dir('images/pelabuhan_cover/'.date("d_m_Y"))){
                        $namePathCover = 'images/pelabuhan_cover/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/pelabuhan_cover')){
                            mkdir('images/pelabuhan_cover/',0777);
                        }
                        mkdir('images/pelabuhan_cover/'.date("d_m_Y"),0777);
                        $namePathCover = 'images/pelabuhan_cover/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePathCover;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['max_width']  = '5000';
                    $config['max_height']  = '5000';
                    // $config['min_width']  = '1000';
                    // $config['max_width']  = '2000';
                    // $config['min_height']  = '450';
                    // $config['max_height']  = '1500';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('cover')) {
                        $getImg=true;
                        $pathfile=$namePathCover.$this->upload->data('file_name');
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img cover'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/pelabuhan?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                }
                
                if ($getImg == true) {
                        $log['cabang_id']  = cleartext($this->input->post('cabang'));
                        $log['item']     = cleartext($this->input->post('name'));
                        $log['deskripsi']  = cleartext($this->input->post('deskripsi'));
                        $log['path_file']   = $pathfile;
                        $log['created_at']   = date('Y-m-d H:i:s');
                        $log['created_by']   = cleartext($this->session->userdata('admin_data')->id);
                        $log['status']   = 2;
                        $log['fulluri']   = base_url('panel/pelabuhan?details='.$this->input->post('id'));
                        $log['flag']   = 'Edit';
                        $log['trans_id']   = $this->input->post('id');

                    $param = array(
                        'status' => 2
                    );
                } else{
                    $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'pelabuhans');
                    if(is_null($pathSimet)){
                        $pathSimet = $selectOne->simetris;
                    }

                    if(is_null($pathFotoDrag)){
                        $pathFotoDrag = $selectOne->foto_drag;
                    }
                    $log = array(
                        'cabang_id'  => $this->input->post('cabang'),
                        'item'     => $this->input->post('name'),
                        'deskripsi'  => $this->input->post('deskripsi'),
                        'path_file'   => $selectOne->foto,
                        'created_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                        'status'   => 2,
                        'fulluri'   => base_url('panel/pelabuhan?details='.$this->input->post('id')),
                        'flag'   => 'Edit',
                        'trans_id'   => $this->input->post('id'),
                        'simetris'   => $pathSimet,
                        'foto_drag'   => $pathFotoDrag,
                    );
                    $param = array(
                        'status' => 2
                    );
                }

                $this->m_model->updateas('id', $this->input->post('id'), $param, 'pelabuhans');
                $this->m_model->insertgetid($log, 'pelabuhans_log');
                // EMAIL
                $dataMailCab = $this->m_model->selectOne('id',$this->input->post('cabang'),'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Edit Data - Pelabuhan',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }

            }
            redirect('panel/pelabuhan?detail='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                $this->m_model->destroy($this->input->get('remove'), 'pelabuhans');
            }else{
                $selectOne = $this->m_model->selectOne('id', $this->input->get('remove'),'pelabuhans');
                    $log = array(
                        'cabang_id'  => $selectOne->cabang_id,
                        'item'     => $selectOne->name,
                        'deskripsi'  => $selectOne->deskripsi,
                        'path_file'   => $selectOne->foto,
                        'created_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                        'status'   => 2,
                        'fulluri'   => base_url('panel/pelabuhan?details='.$this->input->get('remove')),
                        'flag'   => 'Remove',
                        'trans_id'   => $this->input->get('remove'),
                        'path_zonasi'   => $selectOne->path_zonasi,
                        'path_sirkular'   => $selectOne->path_sirkular,
                        'deskripsi_sirkular'   => $selectOne->deskripsi_sirkular,
                        'deskripsi_zonasi'   => $selectOne->deskripsi_zonasi,
                    );
                    $param = array(
                        'status' => 2
                    );
                $this->m_model->updateas('id', $this->input->get('remove'), $param, 'pelabuhans');
                $this->m_model->insertgetid($log, 'pelabuhans_log');

                $dataMailCab = $this->m_model->selectOne('id',$selectOne->cabang_id,'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Hapus Data - Pelabuhan',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }
            }

            // $param = array(
            //     'deleted_at'  => date('Y-m-d H:i:s'),
            //     'deleted_by'   => cleartext($this->session->userdata('admin_data')->username),
            // );
            // $this->m_model->updateas('id', cleartext($this->input->get('remove')), $param, 'pelabuhans');
            redirect('panel/pelabuhan', 'refresh');
        }

        if($this->input->get('details')){
            // redirect('panel/pelabuhan', 'refresh');
        }
    }

    public function armada() {
        if ($this->session->userdata('admin')) {
            if($this->input->get('nama')){
                if(isset($this->session->userdata('admin_data')->id_cabang) && ($this->session->userdata('admin_data')->id_cabang != 0)){
                    $query = "SELECT * FROM armada where cabang_id='".$this->session->userdata('admin_data')->id_cabang."' and name LIKE '%".$this->input->get('nama')."%'";  
                }else{
                    $query = "SELECT * FROM armada where name LIKE '%".$this->input->get('nama')."%'";  
                }
            }else{
                if(isset($this->session->userdata('admin_data')->id_cabang) && ($this->session->userdata('admin_data')->id_cabang != 0)){
                    $query = "SELECT * FROM armada where cabang_id='".$this->session->userdata('admin_data')->id_cabang."'";  
                }else{
                    $query = "SELECT * FROM armada";  
                }
            }
                 
            $this->load->library('pagination'); 
            $config['base_url'] = base_url('panel/armada');    
            $config['total_rows'] = $this->db->query($query)->num_rows();    
            $config['per_page'] = 12;    
            $config['uri_segment'] = 3;    

            $config['first_link']       = 'First';
            $config['last_link']        = 'Last';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link page-link-armada">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link page-link-armada">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link page-link-armada">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link page-link-armada">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link page-link-armada">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link page-link-armada">';
            $config['last_tagl_close']  = '</span></li>';
        
            $this->pagination->initialize($config); // Set konfigurasi paginationnya        
            $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
            $query .= " LIMIT ".$page.", ".$config['per_page'];
            $data['page'] = $page;
            $data['limit'] = $config['per_page'];
            $data['total_rows'] = $config['total_rows'];
            $data['pagination'] = $this->pagination->create_links();
            $data['siswa'] = $this->db->query($query)->result();

            $this->load->view('backend/armada',['title' => 'Armada', 'pageUrl' => base_url('panel/armada'), 'data' => $data]);
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $pathfile = '';
            if (!empty($_FILES['cover']['name'])) {
                if(is_dir('images/armada_cover/'.date("d_m_Y"))){
                    $namePath = 'images/armada_cover/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/armada_cover')){
                        mkdir('images/armada_cover/',0777);
                    }
                    mkdir('images/armada_cover/'.date("d_m_Y"),0777);
                    $namePath = 'images/armada_cover/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['max_width']  = '5000';
                $config['max_height']  = '5000';
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('cover')) {
                    $pathfile=$namePath.$this->upload->data('file_name');
                    $param['foto']   = $pathfile;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img cover'.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/armada?add=true', 'refresh'); 
                    }
                }
            }
            $pathfileSimetris = '';
            if (!empty($_FILES['simetris']['name'])) {
                if(is_dir('images/armada_isometri/'.date("d_m_Y"))){
                    $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/armada_isometri')){
                        mkdir('images/armada_isometri/',0777);
                    }
                    mkdir('images/armada_isometri/'.date("d_m_Y"),0777);
                    $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['max_width']  = '';
                $config['max_height']  = '';
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('simetris')) {
                    $pathfileSimetris=$namePath.$this->upload->data('file_name');
                    $param['simetris'] = $pathfileSimetris;
                }else{
                    if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Img isometri'.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/armada?add=true', 'refresh'); 
                    }
                }
            }

                $param['cabang_id']  = cleartext($this->input->post('cabang_id'));
                $param['name']     = cleartext($this->input->post('name'));
                $param['deskripsi']  = cleartext($this->input->post('deskripsi_armada'));
                $param['created_at']   = date('Y-m-d H:i:s');
                $param['created_user']   = cleartext($this->session->userdata('admin_data')->username);

            $create_armada=$this->m_model->insertgetid($param, 'armada');
            if(count($_FILES['safety_plan']['name']) > 0){
                foreach ($_FILES['safety_plan']['name'] as $k => $value) {
                    if(is_dir('images/armada_safety/'.date("d_m_Y"))){
                        $namePath = 'images/armada_safety/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/armada_safety')){
                            mkdir('images/armada_safety/',0777);
                        }
                        mkdir('images/armada_safety/'.date("d_m_Y"),0777);
                        $namePath = 'images/armada_safety/'.date("d_m_Y").'/';
                    }
                    $_FILES['file']['name']     = $_FILES['safety_plan']['name'][$k];
                    $_FILES['file']['type']     = $_FILES['safety_plan']['type'][$k];
                    $_FILES['file']['tmp_name'] = $_FILES['safety_plan']['tmp_name'][$k];
                    $_FILES['file']['error']     = $_FILES['safety_plan']['error'][$k];
                    $_FILES['file']['size']     = $_FILES['safety_plan']['size'][$k];
                    unset($config);
                    $config['upload_path']   = FCPATH.$namePath;
                    $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                    $config['overwrite']     = FALSE;
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['max_width']  = '';
                    $config['max_height']  = '';
                    $config['file_name'] = $value;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('file')) {
                        $pathfile=$namePath.$this->upload->data('file_name');
                        $arr[$k]['fileurl'] = $pathfile;
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                            $this->session->set_flashdata('error', 'Img safety plan'.$this->upload->display_errors());
                            $this->load->helper('url');
                            redirect('panel/armada?add=true', 'refresh'); 
                        }
                    }
                    $arr[$k]['trans_id'] = $this->db->insert_id();
                    $arr[$k]['filename'] = $this->upload->data('file_name');
                    $arr[$k]['created_at']   = date('Y-m-d H:i:s');
                    $arr[$k]['created_by']   = cleartext($this->session->userdata('admin_data')->username);
                }
                $this->db->insert_batch('armada_file',$arr);
            }
            
            $i=0;
            // print_r($_FILES['file']);
            // die();
            foreach ($_FILES as $key => $value) {
                if( $key != 'cover'){
                    if( $key != 'simetris'){
                        if( $key != 'safety_plan'){
                            if( $key != 'file'){

                            if(is_dir('images/armada_drag/'.date("d_m_Y"))){
                                $namePath = 'images/armada_drag/'.date("d_m_Y").'/';
                            }else{
                                if(!is_dir('images/armada_drag')){
                                    mkdir('images/armada_drag/',0777);
                                }
                                mkdir('images/armada_drag/'.date("d_m_Y"),0777);
                                $namePath = 'images/armada_drag/'.date("d_m_Y").'/';
                            }
                            $pathfile="";
                            # code...
                            $config[$i]['upload_path']   = FCPATH.$namePath;
                            $config[$i]['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config[$i]['max_size'] = 50000000;
                            $config[$i]['min_size'] = 500;
                            $config[$i]['max_width']  = '5000';
                            $config[$i]['max_height']  = '5000';
                            $config[$i]['file_name'] = uniqid();
                            $this->load->library('upload',$config[$i]);
                            $this->upload->initialize($config[$i]);
                            if ($this->upload->do_upload($key)) {
                                $pathfile=$namePath.$this->upload->data('file_name');
                                $paramDeck = array(
                                    'armada_id'  =>$create_armada,
                                    'name'     => cleartext($this->input->post('deck')[$i]),
                                    'deskripsi'     => cleartext($this->input->post('deskripsi')[$i]),
                                    'path_file'   => $pathfile,
                                    'created_at'   => date('Y-m-d H:i:s'),
                                    'created_user'   => cleartext($this->session->userdata('admin_data')->username),
                                );
                                $i++;
                                $create=$this->m_model->create($paramDeck, 'armada_elements');
                            }
                        }
                        }
                    }
                }
            }

            redirect('panel/armada', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $arr = [];
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                if(count($_FILES['safety_plan']['name']) > 0){
                    foreach ($_FILES['safety_plan']['name'] as $k => $value) {
                        if(is_dir('images/armada_safety/'.date("d_m_Y"))){
                            $namePath = 'images/armada_safety/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/armada_safety')){
                                mkdir('images/armada_safety/',0777);
                            }
                            mkdir('images/armada_safety/'.date("d_m_Y"),0777);
                            $namePath = 'images/armada_safety/'.date("d_m_Y").'/';
                        }
                        $_FILES['file']['name']     = $_FILES['safety_plan']['name'][$k];
                        $_FILES['file']['type']     = $_FILES['safety_plan']['type'][$k];
                        $_FILES['file']['tmp_name'] = $_FILES['safety_plan']['tmp_name'][$k];
                        $_FILES['file']['error']    = $_FILES['safety_plan']['error'][$k];
                        $_FILES['file']['size']     = $_FILES['safety_plan']['size'][$k];
                        unset($config);
                        $config['upload_path']   = FCPATH.$namePath;
                        $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                        $config['max_width']  = '';
                        $config['max_height']  = '';             
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        $config['file_name'] = $value;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('file')) {
                            $pathfile=$namePath.$this->upload->data('file_name');
                            $arr[$k]['fileurl'] = $pathfile;
                            $arr[$k]['trans_id'] = $this->input->post('id');
                            $arr[$k]['filename'] = $this->upload->data('file_name');
                            $arr[$k]['created_at']   = date('Y-m-d H:i:s');
                            $arr[$k]['created_by']   = cleartext($this->session->userdata('admin_data')->username);
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img safety plan'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                    }
                    if(count($arr) > 0){
                        $this->db->insert_batch('armada_file',$arr);
                    }
                }

                $pathfile="";

                    $pathfileSimetris = "";
                    if (!empty($_FILES['simetris']['name'])) {
                        if(is_dir('images/armada_isometri/'.date("d_m_Y"))){
                            $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/armada_isometri')){
                                mkdir('images/armada_isometri/',0777);
                            }
                            mkdir('images/armada_isometri/'.date("d_m_Y"),0777);
                            $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                        }
                        $config['upload_path']   = FCPATH.$namePath;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        $config['max_width']  = '';
                        $config['max_height']  = '';
                        $config['file_name'] = uniqid();
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('simetris')) {
                            $pathfileSimetris=$namePath.$this->upload->data('file_name');
                            $param['simetris']   = $pathfileSimetris;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img isometri'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                    }
                
                $checkFile = 0;
                if (!empty($_FILES['cover']['name'])) {
                    if(is_dir('images/armada_cover/'.date("d_m_Y"))){
                        $namePath = 'images/armada_cover/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/armada_cover')){
                            mkdir('images/armada_cover/',0777);
                        }
                        mkdir('images/armada_cover/'.date("d_m_Y"),0777);
                        $namePath = 'images/armada_cover/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePath;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['max_width']  = '5000';
                    $config['max_height']  = '5000';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('cover')) {
                        $pathfile=$namePath.$this->upload->data('file_name');
                        $param['cabang_id']  = cleartext($this->input->post('cabang_id'));
                        $param['name']     = cleartext($this->input->post('name'));
                        $param['deskripsi']  = cleartext($this->input->post('deskripsi_armada'));
                        $param['foto']   = $pathfile;
                        $param['updated_at']   = date('Y-m-d H:i:s');
                        $checkFile = 1;
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img cover'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                }

                if ($checkFile == 0) {
                    $param['cabang_id']  = cleartext($this->input->post('cabang_id'));
                    $param['name']     = cleartext($this->input->post('name'));
                    $param['deskripsi']  = cleartext($this->input->post('deskripsi_armada'));
                    $param['updated_at']   = date('Y-m-d H:i:s');
                    // $param['created_by']   = cleartext($this->session->userdata('admin_data')->id);
                }

                $this->m_model->updateas('id', cleartext($this->input->post('id')), $param, 'armada');
                $i=0;
                foreach ($_FILES as $key => $value) {
                    if( $key != 'cover'){
                    if( $key != 'simetris'){
                    if( $key != 'safety_plan'){
                    if( $key != 'file'){
                    $checkFileDeck = 0;
                        if(!empty($_FILES[$key]['name'])){
                            
                            if(is_dir('images/armada_drag/'.date("d_m_Y"))){
                                $namePath = 'images/armada_drag/'.date("d_m_Y").'/';
                            }else{
                                if(!is_dir('images/armada_drag')){
                                    mkdir('images/armada_drag/',0777);
                                }
                                mkdir('images/armada_drag/'.date("d_m_Y"),0777);
                                $namePath = 'images/armada_drag/'.date("d_m_Y").'/';
                            }
                            $pathfile="";
                            $_FILES['filee']['name']     = $value['name'];
                            $_FILES['filee']['type']     = $value['type'];
                            $_FILES['filee']['tmp_name'] = $value['tmp_name'];
                            $_FILES['filee']['error']     = $value['error'];
                            $_FILES['filee']['size']     = $value['size'];

                            $config[$i]['upload_path']   = FCPATH.$namePath;
                            $config[$i]['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config[$i]['max_size'] = 50000000;
                            $config[$i]['min_size'] = 500;
                            $config[$i]['max_width']  = '5000';
                            $config[$i]['max_height']  = '5000';
                            // $config[$i]['file_name'] = $key;
                            $this->load->library('upload',$config[$i]);
                            $this->upload->initialize($config[$i]);
                            if ($this->upload->do_upload('filee')) {
                                $pathfile=$namePath.$this->upload->data('file_name');
                                $paramDeck = array(
                                    'name'     => cleartext($this->input->post('deck')[$i]),
                                    'deskripsi'     => cleartext($this->input->post('deskripsi')[$i]),
                                    'path_file'   => $pathfile,
                                    'created_at'   => date('Y-m-d H:i:s'),
                                    'created_user'   => cleartext($this->session->userdata('admin_data')->username),
                                );
                                $checkFileDeck = 1;
                            }else{
                                if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                    $this->session->set_flashdata('error', 'Img drag & drop'.$this->upload->display_errors());
                                    $this->load->helper('url');
                                    redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                                }
                            }
                        }
                        if ($checkFileDeck == 0) {
                            $paramDeck = array(
                                'name'     => cleartext($this->input->post('deck')[$i]),
                                'deskripsi'     => cleartext($this->input->post('deskripsi')[$i]),
                                'created_at'   => date('Y-m-d H:i:s'),
                                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
                            );
                        }
                        // echo '<pre>';
                        //     print_r($paramDeck);
                        //     echo '<pre>';
                        //     die();
                        if(!isset($_POST['deck_id'][$i])){
                            $paramDeck['armada_id']=cleartext($this->input->post('id'));
                            $create=$this->m_model->create($paramDeck, 'armada_elements');
                        }
                        else{
                            $update=$this->m_model->updateas('id', $this->input->post('deck_id')[$i], $paramDeck, 'armada_elements');
                        }
                        $i++;
                    }
                    }
                    }
                    }
                }
            }else{
                if(count($_FILES['safety_plan']['name']) > 0){
                    foreach ($_FILES['safety_plan']['name'] as $k => $value) {
                        if(is_dir('images/armada_safety/'.date("d_m_Y"))){
                            $namePath = 'images/armada_safety/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/armada_safety')){
                                mkdir('images/armada_safety/',0777);
                            }
                            mkdir('images/armada_safety/'.date("d_m_Y"),0777);
                            $namePath = 'images/armada_safety/'.date("d_m_Y").'/';
                        }
                        $_FILES['file']['name']     = $_FILES['safety_plan']['name'][$k];
                        $_FILES['file']['type']     = $_FILES['safety_plan']['type'][$k];
                        $_FILES['file']['tmp_name'] = $_FILES['safety_plan']['tmp_name'][$k];
                        $_FILES['file']['error']     = $_FILES['safety_plan']['error'][$k];
                        $_FILES['file']['size']     = $_FILES['safety_plan']['size'][$k];
                        unset($config);
                        $config['upload_path']   = FCPATH.$namePath;
                        $config['allowed_types'] = 'jpg|png|jpeg|giv|ico';
                        $config['overwrite']     = FALSE;
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        $config['max_width']  = '';
                        $config['max_height']  = '';
                        $config['file_name'] = $value;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('file')) {
                            $pathfile = $namePath.$this->upload->data('file_name');
                            $arr[$k]['fileurl'] = $pathfile;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img safety plan'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                        $arr[$k]['trans_id'] = $this->input->post('id');
                        $arr[$k]['filename'] = $this->upload->data('file_name');
                        $arr[$k]['created_at']   = date('Y-m-d H:i:s');
                        $arr[$k]['created_by']   = cleartext($this->session->userdata('admin_data')->username);

                    }
                    $this->db->insert_batch('armada_file',$arr);
                }

            
                // AMPAS CABANG
                $pathfile="";
                if (!empty($_FILES['cover']['name'])) {
                    if(is_dir('images/armada_cover/'.date("d_m_Y"))){
                        $namePath = 'images/armada_cover/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/armada_cover')){
                            mkdir('images/armada_cover/',0777);
                        }
                        mkdir('images/armada_cover/'.date("d_m_Y"),0777);
                        $namePath = 'images/armada_cover/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePath;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['max_width']  = '5000';
                    $config['max_height']  = '5000';
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('cover')) {
                        $pathfile=$namePath.$this->upload->data('file_name');
                    }else{
                        if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img cover'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }

                        $pathfileSimetris = "";
                        if (!empty($_FILES['simetris']['name'])) {
                             if(is_dir('images/armada_isometri/'.date("d_m_Y"))){
                                $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                            }else{
                                if(!is_dir('images/armada_isometri')){
                                    mkdir('images/armada_isometri/',0777);
                                }
                                mkdir('images/armada_isometri/'.date("d_m_Y"),0777);
                                $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                            }
                            $config['upload_path']   = FCPATH.$namePath;
                            $config['allowed_types'] = 'jpg|png|jpeg';
                            $config['max_size'] = 50000000;
                            $config['min_size'] = 500;
                            $config['max_width']  = '';
                            $config['max_height']  = '';
                            $config['file_name'] = uniqid();
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            if ($this->upload->do_upload('simetris')) {
                                $pathfileSimetris=$namePath.$this->upload->data('file_name');
                                $log['simetris']   = $pathfileSimetris;
                            }else{
                                if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                    $this->session->set_flashdata('error', 'Img isometri'.$this->upload->display_errors());
                                    $this->load->helper('url');
                                    redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                                }
                            }
                        }

                        $log['cabang_id']  = cleartext($this->input->post('cabang_id'));
                        $log['item']     = cleartext($this->input->post('name'));
                        $log['deskripsi']  = cleartext($this->input->post('deskripsi_armada'));
                        $log['path_file']   = $pathfile;
                        $log['created_by']   = cleartext($this->session->userdata('admin_data')->id);
                        $log['status']   = 2;
                        $log['fulluri']   = base_url('panel/armada?details='.$this->input->post('id'));
                        $log['flag']   = 'Edit';
                        $log['trans_id']   = $this->input->post('id');

                    $param = array(
                        'status' => 2
                    );
                } else{
                    $pathfileSimetris = "";
                    if (!empty($_FILES['simetris']['name'])) {
                         if(is_dir('images/armada_isometri/'.date("d_m_Y"))){
                            $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                        }else{
                            if(!is_dir('images/armada_isometri')){
                                mkdir('images/armada_isometri/',0777);
                            }
                            mkdir('images/armada_isometri/'.date("d_m_Y"),0777);
                            $namePath = 'images/armada_isometri/'.date("d_m_Y").'/';
                        }
                        $config['upload_path']   = FCPATH.$namePath;
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['max_size'] = 50000000;
                        $config['min_size'] = 500;
                        $config['max_width']  = '';
                        $config['max_height']  = '';
                        $config['file_name'] = uniqid();
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('simetris')) {
                            $pathfileSimetris=$namePath.$this->upload->data('file_name');
                            $log['simetris']   = $pathfileSimetris;
                        }else{
                            if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                $this->session->set_flashdata('error', 'Img isometri'.$this->upload->display_errors());
                                $this->load->helper('url');
                                redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                            }
                        }
                    }
                    
                    $selectOne = $this->m_model->selectOne('id',$this->input->post('id'),'armada');
                    $log['cabang_id']  = cleartext($this->input->post('cabang_id'));
                    $log['item']     = cleartext($this->input->post('name'));
                    $log['deskripsi']  = cleartext($this->input->post('deskripsi_armada'));
                    // $log['created_at']   = date('Y-m-d H:i:s');
                    $log['created_by']   = cleartext($this->session->userdata('admin_data')->id);
                    $log['status']   = 2;
                    $log['fulluri']   = base_url('panel/armada?details='.$this->input->post('id'));
                    $log['flag']   = 'Edit';
                    $log['trans_id']   = $this->input->post('id');
                    $log['path_file']   = $selectOne->foto;

                    $param = array(
                        'status' => 2
                    );
                }


                $this->m_model->insertgetid($log, 'armada_log');
                $this->m_model->updateas('id', cleartext($this->input->post('id')), $param, 'armada');
                $i=0;
                foreach ($_FILES as $key => $value) {
                    if($key != 'cover'){
                    if($key != 'simetris'){
                    if($key != 'safety_plan'){
                    if($key != 'file'){
                    $checkFileDeck = 0;
                        if(!empty($_FILES[$key]['name'])){
                            if(is_dir('images/armada_drag/'.date("d_m_Y"))){
                                $namePath = 'images/armada_drag/'.date("d_m_Y").'/';
                            }else{
                                if(!is_dir('images/armada_drag')){
                                    mkdir('images/armada_drag/',0777);
                                }
                                mkdir('images/armada_drag/'.date("d_m_Y"),0777);
                                $namePath = 'images/armada_drag/'.date("d_m_Y").'/';
                            }

                            $pathfile="";
                            # code...
                            $config[$i]['upload_path']   = FCPATH.$namePath;
                                
                            $config[$i]['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config[$i]['max_size'] = 50000000;
                            $config[$i]['min_size'] = 500;
                            $config[$i]['max_width']  = '5000';
                            $config[$i]['max_height']  = '5000';
                            $config[$i]['file_name'] = uniqid();
                            $this->load->library('upload',$config[$i]);
                            $this->upload->initialize($config[$i]);
                            if ($this->upload->do_upload($key)) {
                                $pathfile=$namePath.$this->upload->data('file_name');
                                $paramDeck = array(
                                    'name'     => cleartext($this->input->post('deck')[$i]),
                                    'deskripsi'     => cleartext($this->input->post('deskripsi')[$i]),
                                    'path_file'   => $pathfile,
                                    'created_at'   => date('Y-m-d H:i:s'),
                                    'created_user'   => cleartext($this->session->userdata('admin_data')->username),
                                );
                                $checkFileDeck = 1;
                            }else{
                                if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                                    $this->session->set_flashdata('error', 'Img drag & drop'.$this->upload->display_errors());
                                    $this->load->helper('url');
                                    redirect('panel/armada?edit='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh'); 
                                }
                            }
                        }
                        if ($checkFileDeck == 0) {
                            $paramDeck = array(
                                'name'     => cleartext($this->input->post('deck')[$i]),
                                'deskripsi'     => cleartext($this->input->post('deskripsi')[$i]),
                                'created_at'   => date('Y-m-d H:i:s'),
                                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
                            );
                        }

                        if(!isset($_POST['deck_id'][$i])){
                            $paramDeck['armada_id']=cleartext($this->input->post('id'));
                            $create=$this->m_model->create($paramDeck, 'armada_elements');
                        }else{
                            $update=$this->m_model->updateas('id', $this->input->post('deck_id')[$i], $paramDeck, 'armada_elements');
                        }
                        $i++;
                    }
                    }
                    }
                    }
                }
            }
            redirect('panel/armada?detail='.$this->input->post('id').'&page='.$this->input->get('page'), 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                $this->m_model->destroy($this->input->get('remove'), 'armada');
            }else{
                $selectOne = $this->m_model->selectOne('id',$this->input->post('id'),'armada');
                    $log = array(
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'item'     => cleartext($this->input->post('name')),
                        'deskripsi'  => cleartext($this->input->post('deskripsi_armada')),
                        // 'created_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                        'status'   => 2,
                        'fulluri'   => base_url('panel/armada?details='.$this->input->post('id')),
                        'flag'   => 'Edit',
                        'trans_id'   => $this->input->post('id'),
                        'path_file'   => $selectOne->path_file,
                        'simetris'   => $selectOne->simetris,
                    );

                    $param = array(
                        'status' => 2
                    );
                $this->m_model->insertgetid($log, 'armada_log');
                $this->m_model->updateas('id', cleartext($this->input->post('id')), $param, 'armada');
            }

            redirect('panel/armada', 'refresh');
        }

        if ($this->input->get('removeElement')) {
            $this->m_model->destroy($this->input->get('removeElement'), 'armada_elements');
            redirect($this->agent->referrer(), 'refresh');
        }

        if($this->input->get('details')){
            // redirect('panel/pelabuhan', 'refresh');
        }
    }

    public function photo() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/foto');
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $pathfile="";
            if (!empty($_FILES['photo']['name'])) {
                
                if(is_dir('images/photo/'.date("d_m_Y"))){
                    $namePath = 'images/photo/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/photo')){
                        mkdir('images/photo/',0777);
                    }
                    mkdir('images/photo/'.date("d_m_Y"),0777);
                    $namePath = 'images/photo/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;

                $config['file_name'] = uniqid();
                // $config['overwrite'] = true;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $this->upload->do_upload('photo');
                $pathfile=$namePath.$this->upload->data('file_name');
            }

            $param = array(
                'item'  => cleartext($this->input->post('item')),
                'cabang_id'  => cleartext($this->input->post('cabang_id')),
                'deskripsi'  => cleartext($this->input->post('deskripsi')),
                'path_file'   => $pathfile,
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $create=$this->m_model->insertgetid($param, 'photo');
            redirect('panel/photo', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                if (!empty($_FILES['photo']['name'])) {
                    if(is_dir('images/photo/'.date("d_m_Y"))){
                        $namePath = 'images/photo/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/photo')){
                            mkdir('images/photo/',0777);
                        }
                        mkdir('images/photo/'.date("d_m_Y"),0777);
                        $namePath = 'images/photo/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePath;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['file_name'] = uniqid();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('photo');

                    $pathfile=$namePath.$this->upload->data('file_name');
                    $param = array(
                        'item'  => cleartext($this->input->post('item')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => cleartext($this->input->post('deskripsi')),
                        'path_file'   => $pathfile,
                        'updated_at'   => date('Y-m-d H:i:s'),
                        'updated_by'   => cleartext($this->session->userdata('admin_data')->id),
                        'status'   => 3,
                    );
                }else{
                    $param = array(
                        'item'  => cleartext($this->input->post('item')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => cleartext($this->input->post('deskripsi')),
                        // 'path_file'   => $pathfile,
                        'updated_at'   => date('Y-m-d H:i:s'),
                        'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                        'status'   => 3,
                    );
                }
                $this->m_model->updateas('id', $this->input->post('id'), $param, 'photo');
            }else{
                if (!empty($_FILES['photo']['name'])) {
                    if(is_dir('images/photo/'.date("d_m_Y"))){
                        $namePath = 'images/photo/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/photo')){
                            mkdir('images/photo/',0777);
                        }
                        mkdir('images/photo/'.date("d_m_Y"),0777);
                        $namePath = 'images/photo/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePath;
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $config['max_size'] = 50000000;
                    $config['min_size'] = 500;
                    $config['file_name'] = uniqid();
                    // $config['overwrite'] = true;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('photo');
                    $pathfile=$namePath.$this->upload->data('file_name');

                    $log = array(
                        'item'  => cleartext($this->input->post('item')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => cleartext($this->input->post('deskripsi')),
                        'path_file'   => $pathfile,
                        // 'updated_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                        'fulluri'   => base_url('panel/photo?details='.$this->input->post('id')),
                        'trans_id'   => $this->input->post('id'),
                        'status'   => 2,
                        'flag'   => 'Edit',
                    );
                    $param = array(
                        'status' => 2
                    );
                }else{
                    $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'photo');
                    $log = array(
                        'path_file'  => $selectOne->path_file,
                        'item'  => cleartext($this->input->post('item')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => cleartext($this->input->post('deskripsi')),
                        // 'updated_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                        'fulluri'   => base_url('panel/photo?details='.$this->input->post('id')),
                        'trans_id'   => $this->input->post('id'),
                        'status'   => 2,
                        'flag'   => 'Edit',
                    );
                    $param = array(
                        'status' => 2
                    );
                }
                
                $this->m_model->updateas('id', $this->input->post('id'), $param, 'photo');
                $this->m_model->insertgetid($log, 'photo_log');

                $dataMailCab = $this->m_model->selectOne('id',$this->input->post('cabang_id'),'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Edit Data - Photo',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }
            }

            if($this->input->get('details')){
                // redirect('panel/pelabuhan', 'refresh');
            }

            redirect('panel/photo', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                $this->m_model->destroy($this->input->get('remove'),'photo');

            }else{
                $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'photo');
                $log = array(
                    'path_file'  => $selectOne->path_file,
                    'item'  => $selectOne->item,
                    'cabang_id'  => $selectOne->cabang_id,
                    'deskripsi'  => $selectOne->deskripsi,
                    // 'updated_at'   => date('Y-m-d H:i:s'),
                    'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                    'fulluri'   => base_url('panel/photo?details='.$this->input->post('id')),
                    'trans_id'   => $this->input->get('remove'),
                    'status'   => 2,
                    'flag'   => 'Remove',
                );
                $param = array(
                    'status' => 2
                );
                $this->m_model->updateas('id', $this->input->get('remove'), $param, 'photo');
                $this->m_model->insertgetid($log, 'photo_log'); 

                $dataMailCab = $this->m_model->selectOne('id',$selectOne->cabang_id,'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Hapus Data - Photo',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }
            }
            


            redirect('panel/photo', 'refresh');
        }
    }

    public function cabang() {
        if (!$this->session->userdata('admin')) {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $createdata = array(
                'name'     => cleartext($this->input->post('name')),
                'status'  => cleartext($this->input->post('status')),
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $createdata=$this->m_model->insertgetid($createdata, 'cabangs');
            redirect('panel/cabang', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $createdata = array(
                'name'     => cleartext($this->input->post('name')),
                'status'  => cleartext($this->input->post('status')),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->updateas('id', $this->input->post('id'), $createdata, 'cabangs');
            redirect('panel/cabang', 'refresh');
        }

        if ($this->input->get('id') && $this->input->get('status')) {
            switch (strtolower($this->input->get('status'))) {
                case 'active':
                    # code...
                    $status=1;
                    break;
                case 'unactive':
                    # code...
                    $status=0;
                    break;
                
                default:
                    # code...
                    redirect('panel/cabang', 'refresh');
                    break;
            }

            $createdata = array(
                'status'  => $status,
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->updateas('id', cleartext($this->input->get('id')), $createdata, 'cabangs');
            redirect('panel/cabang', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $createdata = array(
                'deleted_at'  => date('Y-m-d H:i:s'),
                'deleted_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->destroy($this->input->get('remove'), 'cabangs');
            redirect('panel/cabang', 'refresh');
        }

        if ($this->session->userdata('admin')) {
            $this->load->view('backend/cabang');
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function jenis_aspeks() {
        if (!$this->session->userdata('admin')) {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $param = array(
                'nama_aspek' => cleartext($this->input->post('nama_aspek')),
                'deskripsi'     => cleartext($this->input->post('deskripsi')),
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $createdata=$this->m_model->insertgetid($param, 'jenis_aspeks');
            redirect('panel/jenis_aspeks', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $param = array(
                'nama_aspek' => cleartext($this->input->post('nama_aspek')),
                'deskripsi'     => cleartext($this->input->post('deskripsi')),
                'updated_at'   => date('Y-m-d H:i:s'),
                'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->updateas('id', $this->input->post('id'), $param, 'jenis_aspeks');
            redirect('panel/jenis_aspeks', 'refresh');
        }

        //---
        if ($this->input->get('remove')) {
            $createdata = array(
                'deleted_at'  => date('Y-m-d H:i:s'),
                'deleted_by'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->updateas('id', cleartext($this->input->get('remove')), $createdata, 'jenis_aspeks');
            redirect('panel/jenis_aspeks', 'refresh');
        }

        if ($this->session->userdata('admin')) {
            $this->load->view('backend/jenis_aspeks');
        } else {
            redirect('panel/login', 'refresh');
        }
    }

    public function video() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/video');
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            // print_r('expression');
            // die();
            $pathfile="";
            if (!empty($_FILES['photo']['name'])) {

                if(is_dir('images/video/'.date("d_m_Y"))){
                    $namePath = 'images/video/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/video')){
                        mkdir('images/video/',0777);
                    }
                    mkdir('images/video/'.date("d_m_Y"),0777);
                    $namePath = 'images/video/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                // $config['allowed_types'] = 'mp4|mkv|avi|3gp|mpg|mpeg|flv|mdi';
                $config['max_size'] = 50000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                // $config['overwrite'] = true;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('photo')) {
                    $pathfile=$namePath.$this->upload->data('file_name');
                }else{
                    // if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'Video '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/video?add=true', 'refresh'); 
                    // }
                }
            }

            $param = array(
                'filename'  => $_FILES['photo']['name'],
                'item'  => cleartext($this->input->post('item')),
                'cabang_id'  => cleartext($this->input->post('cabang_id')),
                'deskripsi'  => cleartext($this->input->post('deskripsi')),
                'path_file'   => $pathfile,
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $create=$this->m_model->insertgetid($param, 'video');
            redirect('panel/video', 'refresh');

        }
        //---
        if ($this->input->post('save')) {
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                if (!empty($_FILES['photo']['name'])) {
                    if(is_dir('images/video/'.date("d_m_Y"))){
                        $namePath = 'images/video/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/video')){
                            mkdir('images/video/',0777);
                        }
                        mkdir('images/video/'.date("d_m_Y"),0777);
                        $namePath = 'images/video/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePath;
                    // $config['allowed_types'] = 'mp4|mkv|avi|3gp|mpg|mpeg|flv|mdi';
                    $config['max_size'] = 50000;
                    $config['min_size'] = 500;
                    $config['file_name'] = uniqid();
                    // $config['overwrite'] = true;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('photo')) {
                        $pathfile=$namePath.$this->upload->data('file_name');
                    }else{
                        // if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                            $this->session->set_flashdata('error', 'Video '.$this->upload->display_errors());
                            $this->load->helper('url');
                            redirect('panel/video?edit='.$this->input->post('id'), 'refresh'); 
                        // }
                    }
                    $param = array(
                        'item'  => cleartext($this->input->post('item')),
                        'status'  => 3,
                        // 'fulluri'  => base_url('panel/video/detail/'.$this->input->post('id')),
                        // 'trans_id'  => cleartext($this->input->post('id')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => $this->input->post('deskripsi'),
                        'path_file'   => $pathfile,
                        'filename'  => $_FILES['photo']['name'],
                        // 'flag'  => 'Edit',

                        // 'updated_at'   => date('Y-m-d H:i:s'),
                        // 'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                    );
                }else{
                     $param = array(
                        'item'  => cleartext($this->input->post('item')),
                        'status'  => 3,
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => $this->input->post('deskripsi'),
                    );
                }
                $this->m_model->updateas('id', $this->input->post('id'), $param, 'video');
            }else{
                $pathfile="";
                if (!empty($_FILES['photo']['name'])) {
                    if(is_dir('images/video/'.date("d_m_Y"))){
                        $namePath = 'images/video/'.date("d_m_Y").'/';
                    }else{
                        if(!is_dir('images/video')){
                            mkdir('images/video/',0777);
                        }
                        mkdir('images/video/'.date("d_m_Y"),0777);
                        $namePath = 'images/video/'.date("d_m_Y").'/';
                    }
                    $config['upload_path']   = FCPATH.$namePath;
                    // $config['allowed_types'] = 'mp4|mkv|avi|3gp|mpg|mpeg|flv|mdi';
                    $config['max_size'] = 50000;
                    $config['min_size'] = 500;
                    $config['file_name'] = uniqid();
                    // $config['overwrite'] = true;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('photo')) {
                        $pathfile=$namePath.$this->upload->data('file_name');
                    }else{
                        // if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                            $this->session->set_flashdata('error', 'Video '.$this->upload->display_errors());
                            $this->load->helper('url');
                            redirect('panel/video?edit='.$this->input->post('id'), 'refresh'); 
                        // }
                    }
                    $log = array(
                        'item'  => cleartext($this->input->post('item')),
                        'status'  => 2,
                        'fulluri'  => base_url('panel/video?detail='.$this->input->post('id')),
                        'trans_id'  => cleartext($this->input->post('id')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => $this->input->post('deskripsi'),
                        'path_file'   => $pathfile,
                        'filename'  => $_FILES['photo']['name'],
                        'flag'  => 'Edit',

                        // 'updated_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                    );
                }else{
                    $selectOne = $this->m_model->selectOne('id', $this->input->post('id'),'video');
                    $log = array(
                        'item'  => cleartext($this->input->post('item')),
                        'status'  => 2,
                        'fulluri'  => base_url('panel/video?detail='.$this->input->post('id')),
                        'trans_id'  => cleartext($this->input->post('id')),
                        'cabang_id'  => cleartext($this->input->post('cabang_id')),
                        'deskripsi'  => $this->input->post('deskripsi'),
                        'path_file'   => $selectOne->path_file,
                        'filename'  => $selectOne->filename,
                        'flag'  => 'Edit',
                        // 'updated_at'   => date('Y-m-d H:i:s'),
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                    );
                }
                
                $param = array(
                    'status' => 2
                );
                $this->m_model->updateas('id', $this->input->post('id'), $param, 'video');
                $this->m_model->insertgetid($log, 'video_log');

                $dataMailCab = $this->m_model->selectOne('id',$this->input->post('cabang_id'),'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Edit Data - Video',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }
            }

            redirect('panel/video', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $cekUsersNya = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id,'users');
            if(($cekUsersNya->roles == 1) || ($cekUsersNya->roles == 2)){
                $this->m_model->destroy($this->input->get('remove'), 'video');
            }else{
                $selectOne = $this->m_model->selectOne('id', $this->input->get('remove'),'video');
                    $log = array(
                        'item'  => $selectOne->item,
                        'status'  => 2,
                        'fulluri'  => base_url('panel/video?details='.$this->input->get('remove')),
                        'trans_id'  => $selectOne->trans_id,
                        'cabang_id'  => $selectOne->cabang_id,
                        'deskripsi'  => $selectOne->deskripsi,
                        'path_file'   => $selectOne->path_file,
                        'filename'  => $selectOne->filename,
                        'flag'  => 'Remove',
                        'created_by'   => cleartext($this->session->userdata('admin_data')->id),
                        // 'updated_at'   => date('Y-m-d H:i:s'),
                        // 'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                    );
                
                $param = array(
                    'status' => 2
                );
                $this->m_model->updateas('id', $this->input->get('remove'), $param, 'video');
                $this->m_model->insertgetid($log, 'video_log');

                $dataMailCab = $this->m_model->selectOne('id',$selectOne->cabang_id,'cabangs');
                $dataMailSp = $this->m_model->selectcustom('select * from users where roles in (1,2)');
                if(count($dataMailSp) > 0){
                    foreach ($dataMailSp as $value) {
                        $dataEmailNya = array(
                            "pesan" => "Ada Approval Terbaru Untuk Anda",
                            "PIC Peminta" => $this->session->userdata('admin_data')->username,
                            "email" => $value->email,
                            "Cabang" => $dataMailCab->name,
                            "Tipe Permintaan" => 'Edit Data - Video',
                            "Status" => "Waiting For Approval"
                        );
                        sendEMAIL($dataEmailNya);
                    }
                }
            }

            redirect('panel/video', 'refresh');
        }

        if($this->input->get('details')){
                // redirect('panel/pelabuhan', 'refresh');
        }
    }

    public function deck() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/armada');
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            if (!empty($_FILES['cover']['name'])) {
                $config['upload_path']   = FCPATH.'images/armada/cover/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $this->upload->do_upload('cover');
                $pathfile='images/armada/cover/'.$this->upload->data('file_name');
            }

            $param = array(
                'cabang_id'  => cleartext($this->input->post('cabang_id')),
                'name'     => cleartext($this->input->post('name')),
                'deskripsi'  => cleartext($this->input->post('deskripsi')),
                'foto'   => $pathfile,
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $create=$this->m_model->insertgetid($param, 'armada');
            redirect('panel/armada', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $business_creator = array(
                'store_id_supplier'     => $this->input->post('supplier'),
                'user_id_agent'  => $this->input->post('user_agent'),
                'fee'   => $this->input->post('fee'),
                'product_store_id'   => $this->input->post('product_store'),
                'keterangan'  => $this->input->post('keterangan'),
                'start'  => convert_time_ymd($this->input->post('start')),
                'end'  => convert_time_ymd($this->input->post('end')),
                'status' => $this->input->post('status')
            );
            $this->m_model->updateas('id', $this->input->post('id'), $business_creator, 'armada');
            redirect('panel/armada', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $this->db->delete('armada', array('id' => $this->input->get('remove')));
            redirect('panel/armada', 'refresh');
        }
    }

    public function aspekArmada() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/aspekArmada');
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $param = array(
                'nama_aspek' => cleartext($this->input->post('nama_aspek')),
                'status' => cleartext($this->input->post('status')),
                'deskripsi'     => cleartext($this->input->post('deskripsi')),
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $createdata=$this->m_model->insertgetid($param, 'jenis_aspeks');
            redirect('panel/aspekArmada', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $param = array(
                'nama_aspek' => cleartext($this->input->post('nama_aspek')),
                'status' => cleartext($this->input->post('status')),
                'deskripsi'     => cleartext($this->input->post('deskripsi')),
                'updated_at'   => date('Y-m-d H:i:s'),
                'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->updateas('id', $this->input->post('id'), $param, 'jenis_aspeks');
            redirect('panel/aspekArmada', 'refresh');
        }

        //---
        if ($this->input->get('remove')) {
            
            $this->m_model->destroy($this->input->get('remove'), 'jenis_aspeks');
            redirect('panel/aspekArmada', 'refresh');
        }
        //---
        if ($this->input->post('addsubaspek')) {
            // print_r($this->input->post());
            // die();
            $data = array(
                'jenis_aspek_id' => cleartext($this->input->post('jenis_aspeks')),
                'name'            => cleartext($this->input->post('sub_aspek')),
                'created_at' => date('Y-m-d H:i:s'),
                'created_user' => cleartext($this->session->userdata('admin_data')->username),
            );
            // print_r($data);
            // die();
            $this->m_model->create($data,'sub_aspeks');
            $arr = [];
            $saves = $this->db->insert_id();
            // print_r($this->db->insert_id());
            // die();
            foreach ($this->input->post('icon') as $k => $value) {
                $arr[$k]['trans_sub_id'] = $saves;
                $arr[$k]['trans_icon_id'] = $k;
                $arr[$k]['status'] = $value;
                $arr[$k]['units'] = $this->input->post('satuan')[$k];
                $arr[$k]['created_at'] = $this->input->post('created_at')[$k];
            }
            $this->db->insert_batch('sub_aspeks_icon',$arr);
            redirect('panel/aspekArmada', 'refresh');
        }
        if ($this->input->post('savesub')) {
            // print_r($this->input->post());
            // die();
           $data = array(
                'jenis_aspek_id' => $this->input->post('jenis_aspeks'),
                'name'            => $this->input->post('sub_aspek'),
                'update_at' => date('Y-m-d H:i:s'),
            );
            $update = $this->m_model->updateas('id', $this->input->post('id'), $data,
                'sub_aspeks');
            $arr = [];
            foreach ($this->input->post('icon') as $k => $value) {
                $arr[$k]['trans_sub_id'] = $this->input->post('id');
                $arr[$k]['trans_icon_id'] = $k;
                $arr[$k]['status'] = $value;
                $arr[$k]['units'] = $this->input->post('satuan')[$k];
                $arr[$k]['created_at'] = $this->input->post('created_at')[$k];
            }
            $this->m_model->deleteas('trans_sub_id',$this->input->post('id'),'sub_aspeks_icon');
            $this->db->insert_batch('sub_aspeks_icon',$arr);
            redirect('panel/aspekArmada', 'refresh');
        }
        if ($this->input->get('removesub')) {
            $this->m_model->destroy($this->input->get('removesub'), 'sub_aspeks');
            redirect('panel/aspekArmada', 'refresh');
        }
        //---
        if ($this->input->post('addchild')) {
            $seo = str_replace('--', '-', str_replace('--', '-', strtolower(str_replace(' ', '-',
                preg_replace('/[^A-Za-z0-9\-]/', ' ', $this->input->post('name'))))));
            $data = array(
                'category_sub' => $this->input->post('category_sub'),
                'name'         => $this->input->post('name'),
                'name_id' => $this->input->post('name_id'),
                'seo' => $seo
            );
            if ($this->m_model->create($data, 'category_child') == 1) {
                redirect('panel/aspekArmada', 'refresh');
            } else {
                redirect('panel/aspekArmada', 'refresh');
            }
        }
        if ($this->input->post('savechild')) {
            $seo = str_replace('--', '-', str_replace('--', '-', strtolower(str_replace(' ', '-',
                preg_replace('/[^A-Za-z0-9\-]/', ' ', $this->input->post('name'))))));
            $data   = array(
                'category_sub' => $this->input->post('category_sub'),
                'name'         => $this->input->post('name'),
                'name_id' => $this->input->post('name_id'),
                'seo' => $seo
            );
            $update = $this->m_model->updateas('id', $this->input->post('id'), $data,
                'category_child');
            if ($update == 1) {
                redirect('panel/aspekArmada', 'refresh');
            } else {
                redirect('panel/aspekArmada', 'refresh');
            }
        }
        if ($this->input->get('removechild')) {
            $this->db->delete('category_child', array('id' => $this->input->get('removechild')));
            redirect('panel/aspekArmada', 'refresh');
        }
    }

    public function aspekPelabuhan() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/aspekPelabuhan');
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $param = array(
                'nama_aspek' => cleartext($this->input->post('nama_aspek')),
                'status' => cleartext($this->input->post('status')),
                'deskripsi'     => cleartext($this->input->post('deskripsi')),
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $createdata=$this->m_model->insertgetid($param, 'jenis_aspeks');
            redirect('panel/aspekPelabuhan', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $param = array(
                'nama_aspek' => cleartext($this->input->post('nama_aspek')),
                'status' => cleartext($this->input->post('status')),
                'deskripsi'     => cleartext($this->input->post('deskripsi')),
                'updated_at'   => date('Y-m-d H:i:s'),
                'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $this->m_model->updateas('id', $this->input->post('id'), $param, 'jenis_aspeks');
            redirect('panel/aspekPelabuhan', 'refresh');
        }

        //---
        if ($this->input->get('remove')) {
            
            $this->m_model->destroy($this->input->get('remove'), 'jenis_aspeks');
            redirect('panel/aspekPelabuhan', 'refresh');
        }
        //---
        if ($this->input->post('addsubaspek')) {
            // print_r($this->input->post());
            // die();
            $data = array(
                'jenis_aspek_id' => cleartext($this->input->post('jenis_aspeks')),
                'name'            => cleartext($this->input->post('sub_aspek')),
                'created_at' => date('Y-m-d H:i:s'),
                'created_user' => cleartext($this->session->userdata('admin_data')->username),
            );
            // print_r($data);
            // die();
            $this->m_model->create($data,'sub_aspeks');
            $arr = [];
            $saves = $this->db->insert_id();
            // print_r($this->db->insert_id());
            // die();
            foreach ($this->input->post('icon') as $k => $value) {
                $arr[$k]['trans_sub_id'] = $saves;
                $arr[$k]['trans_icon_id'] = $k;
                $arr[$k]['status'] = $value;
                $arr[$k]['units'] = $this->input->post('satuan')[$k];
                $arr[$k]['created_at'] = $this->input->post('created_at')[$k];
            }
            $this->db->insert_batch('sub_aspeks_icon',$arr);
            redirect('panel/aspekPelabuhan', 'refresh');
        }
        if ($this->input->post('savesub')) {
            // print_r($this->input->post());
            // die();
           $data = array(
                'jenis_aspek_id' => $this->input->post('jenis_aspeks'),
                'name'            => $this->input->post('sub_aspek'),
                'update_at' => date('Y-m-d H:i:s'),
            );
            $update = $this->m_model->updateas('id', $this->input->post('id'), $data,
                'sub_aspeks');
            $arr = [];
            if($this->input->post('icon')){   
                foreach ($this->input->post('icon') as $k => $value) {
                    $arr[$k]['trans_sub_id'] = $this->input->post('id');
                    $arr[$k]['trans_icon_id'] = $k;
                    $arr[$k]['status'] = $value;
                    $arr[$k]['units'] = $this->input->post('satuan')[$k];
                    $arr[$k]['created_at'] = $this->input->post('created_at')[$k];
                }
            }
            $this->m_model->deleteas('trans_sub_id',$this->input->post('id'),'sub_aspeks_icon');
            $this->db->insert_batch('sub_aspeks_icon',$arr);
            redirect('panel/aspekPelabuhan', 'refresh');
        }
        if ($this->input->get('removesub')) {
            $this->m_model->destroy($this->input->get('removesub'), 'sub_aspeks');
            redirect('panel/aspekPelabuhan', 'refresh');
        }
        //---
        if ($this->input->post('addchild')) {
            $seo = str_replace('--', '-', str_replace('--', '-', strtolower(str_replace(' ', '-',
                preg_replace('/[^A-Za-z0-9\-]/', ' ', $this->input->post('name'))))));
            $data = array(
                'category_sub' => $this->input->post('category_sub'),
                'name'         => $this->input->post('name'),
                'name_id' => $this->input->post('name_id'),
                'seo' => $seo
            );
            if ($this->m_model->create($data, 'category_child') == 1) {
                redirect('panel/aspekPelabuhan', 'refresh');
            } else {
                redirect('panel/aspekPelabuhan', 'refresh');
            }
        }
        if ($this->input->post('savechild')) {
            $seo = str_replace('--', '-', str_replace('--', '-', strtolower(str_replace(' ', '-',
                preg_replace('/[^A-Za-z0-9\-]/', ' ', $this->input->post('name'))))));
            $data   = array(
                'category_sub' => $this->input->post('category_sub'),
                'name'         => $this->input->post('name'),
                'name_id' => $this->input->post('name_id'),
                'seo' => $seo
            );
            $update = $this->m_model->updateas('id', $this->input->post('id'), $data,
                'category_child');
            if ($update == 1) {
                redirect('panel/aspekPelabuhan', 'refresh');
            } else {
                redirect('panel/aspekPelabuhan', 'refresh');
            }
        }
        if ($this->input->get('removechild')) {
            $this->db->delete('category_child', array('id' => $this->input->get('removechild')));
            redirect('panel/aspekPelabuhan', 'refresh');
        }
    }

    public function icon() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/icon');
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            // print_r($_FILES['icon']);
            // die();
            $pathfile="";
            if (!empty($_FILES['icon']['name'])) {
                if(is_dir('images/icon/'.date("d_m_Y"))){
                    $namePath = 'images/icon/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/icon')){
                        mkdir('images/icon/',0777);
                    }
                    mkdir('images/icon/'.date("d_m_Y"),0777);
                    $namePath = 'images/icon/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $this->upload->do_upload('icon');
                $pathfile=$namePath.$this->upload->data('file_name');
            }

            $param = array(
                'name'  => cleartext($this->input->post('name')),
                
                'deskripsi'  => cleartext($this->input->post('deskripsi')),
                'path_file'   => $pathfile,
                'created_at'   => date('Y-m-d H:i:s'),
                'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );

            $this->m_model->create($param,'icon');
            $arr = [];
            $saves = $this->db->insert_id();
            // print_r($this->db->insert_id());
            // die();
            // foreach ($this->input->post('ic_desc') as $k => $value) {
            //     $arr[$k]['trans_id'] = $saves;
            //     $arr[$k]['value'] = $value;
            // }
            // $this->db->insert_batch('icon_sub',$arr);

            redirect('panel/icon', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            
            if (!empty($_FILES['icon']['name'])) {
                if(is_dir('images/icon/'.date("d_m_Y"))){
                    $namePath = 'images/icon/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/icon')){
                        mkdir('images/icon/',0777);
                    }
                    mkdir('images/icon/'.date("d_m_Y"),0777);
                    $namePath = 'images/icon/'.date("d_m_Y").'/';
                }

                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 50000000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $this->upload->do_upload('icon');
                $pathfile=$namePath.$this->upload->data('file_name');

                $param = array(
                    'name'  => cleartext($this->input->post('name')),
                    
                    'deskripsi'  => cleartext($this->input->post('deskripsi')),
                    'path_file'   => $pathfile,
                    'updated_at'   => date('Y-m-d H:i:s'),
                    'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                );
            }
            else{
                $param = array(
                    'name'  => cleartext($this->input->post('name')),
                    
                    'deskripsi'  => cleartext($this->input->post('deskripsi')),
                    'updated_at'   => date('Y-m-d H:i:s'),
                    'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                );
            }

            $this->m_model->updateas('id', $this->input->post('id'), $param, 'icon');
            $arr = [];
            // $this->m_model->deleteas('trans_id',$this->input->post('id'),'icon_sub');
            // if(count($this->input->post('ic_desc'))){
            //     foreach ($this->input->post('ic_desc') as $k => $value) {
            //         $arr[$k]['trans_id'] = $this->input->post('id');
            //         $arr[$k]['value'] = $value;
            //     }
            //     $this->db->insert_batch('icon_sub',$arr);

            // }

            redirect('panel/icon', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $this->m_model->deleteas('trans_id',$this->input->get('remove'), 'icon_sub');

            $this->m_model->destroy($this->input->get('remove'), 'icon');
            redirect('panel/icon', 'refresh');
        }
    }

    public function roles() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/roles',[
                'title' => 'Roles',
                'bcrumb' => 'Master Data > Roles'
            ]);
        } else {
            redirect('panel/login', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $param = array(
                'roles'  => cleartext($this->input->post('roles')),
            );
            $create=$this->m_model->insertgetid($param, 'roles');
            $this->session->set_flashdata('sukses', 'Data Berhasil Di Buat');
            redirect('panel/roles', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $param = array(
                'roles'  => cleartext($this->input->post('roles')),
            );
            $this->m_model->updateas('id', $this->input->post('id'), $param, 'roles');
            $this->session->set_flashdata('sukses', 'Data Berhasil Di Ubah');
            redirect('panel/roles', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            $this->m_model->destroy($this->input->get('remove'), 'roles');
            $this->session->set_flashdata('sukses', 'Data Berhasil Di Hapus');
            redirect('panel/roles', 'refresh');
        }
    }

    // public function signup() {
    //     if ($this->session->userdata('admin')) {
    //         redirect('panel', 'refresh');
    //     } else {
    //         $this->load->view('backend/signup');
    //     }

    //     if ($this->input->post('email') && $this->input->post('password')) {
    //         $data = array(
    //             'username'    => $this->input->post('username'),
    //             'email'    => $this->input->post('email'),
    //             'password' => $this->input->post('password'),
    //         );
    //         if ($this->m_model->loginadmin($data) == 1) {
    //             redirect('panel', 'refresh');
    //         }
    //         else{
    //             $this->session->set_flashdata('gagal', 'Kesalahan Untuk Registrasi');
    //             $this->session->set_userdata($sess_data);
    //         }
    //     }
    // }

    public function approve($type, $id) {
        $data = array(
            'form_type'    => $type,
            'form_id'    => $id,
            'user_id'    => $this->session->userdata('admin_data')->id,
            'status'    => 'On Process',
        );

        $cab = $this->m_model->selectOne('id',$this->session->userdata('admin_data')->id_cabang,'cabangs');
        $dataJ = '{
            "PIC Peminta" : "'.$this->session->userdata('admin_data')->username.'",
            "email" : "'.$this->session->userdata('admin_data')->email.'",
            "Cabang" : "'.$cab->name.'",
            "Tipe Permintaan" : "'.$type.'",
            "Status" : "On Process",
            "pesan" : "Ada Permintaan Approval Terbaru Untuk Anda Akun Baru Untuk Anda"
        }';
        sendsMaiils($dataJ);
        $admSuper = $this->m_model->selectOne('roles','1','users');
        $admCab = $this->m_model->selectOne('roles','2','users');
        $collAdm = [$admSuper->email,$admCab->email];
        if(count($collAdm) > 0){
            foreach ($collAdm as $value) {
                $dataJ = '{
                    "PIC Peminta" : "'.$this->session->userdata('admin_data')->username.'",
                    "email" : "'.$value.'",
                    "Cabang" : "'.$cab->name.'",
                    "Tipe Permintaan" : "'.$type.'",
                    "Status" : "On Process",
                    "pesan" : "Ada Permintaan Approval Terbaru Untuk Anda Akun Baru Untuk Anda"
                }';
                sendsMaiils($dataJ);
            }
        }
        $this->m_model->deleteas2('form_id',$id,'form_type',$type,'trans_approval');
        $create=$this->m_model->insertgetid($data, 'trans_approval');
        if ($create == 1) {
            redirect('panel/'.$type, 'refresh');
        }else{
            // $this->session->set_flashdata('gagal', 'Kesalahan Approve');
            redirect('panel/'.$type, 'refresh');
        }
    }

     public function file() {
        if ($this->session->userdata('admin')) {
            $this->load->view('backend/pdf',['title'=>'Standarisasi']);
        } else {
            redirect('panel/file', 'refresh');
        }
        //---
        if ($this->input->post('add')) {
            $pathfile="";
            if (!empty($_FILES['photo']['name'])) {
                if(is_dir('images/pdf/'.date("d_m_Y"))){
                    $namePath = 'images/pdf/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pdf')){
                        mkdir('images/pdf/',0777);
                    }
                    mkdir('images/pdf/'.date("d_m_Y"),0777);
                    $namePath = 'images/pdf/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = 50000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('photo')) {
                    $pathfile=$namePath.$this->upload->data('file_name');
                }else{
                    // if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'File '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/file?add=true', 'refresh'); 
                    // }
                }
                
            }

            $param = array(
                'filename'  => cleartext($this->input->post('filename')),
                'deskripsi'  => cleartext($this->input->post('deskripsi')),
                'kategori'  => cleartext($this->input->post('kategori')),
                'fileurl'   => $pathfile,
                'created_at'   => date('Y-m-d H:i:s'),
                // 'created_user'   => cleartext($this->session->userdata('admin_data')->username),
            );
            $create=$this->m_model->insertgetid($param, 'file');
            redirect('panel/file', 'refresh');
        }
        //---
        if ($this->input->post('save')) {
            $pathfile="";
            if (!empty($_FILES['photo']['name'])) {
                if(is_dir('images/pdf/'.date("d_m_Y"))){
                    $namePath = 'images/pdf/'.date("d_m_Y").'/';
                }else{
                    if(!is_dir('images/pdf')){
                        mkdir('images/pdf/',0777);
                    }
                    
                    mkdir('images/pdf/'.date("d_m_Y"),0777);
                    $namePath = 'images/pdf/'.date("d_m_Y").'/';
                }
                $config['upload_path']   = FCPATH.$namePath;
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = 50000;
                $config['min_size'] = 500;
                $config['file_name'] = uniqid();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('photo')) {
                    $pathfile=$namePath.$this->upload->data('file_name');
                }else{
                    // if(strlen(str_replace(' ','',$this->upload->display_errors())) != 36){
                        $this->session->set_flashdata('error', 'File '.$this->upload->display_errors());
                        $this->load->helper('url');
                        redirect('panel/file?edit='.$this->input->post('id'), 'refresh'); 
                    // }
                }

                $param = array(
                    'filename'  => cleartext($this->input->post('filename')),
                    'deskripsi'  => cleartext($this->input->post('deskripsi')),
                    'kategori'  => cleartext($this->input->post('kategori')),
                    'fileurl'   => $pathfile,
                    'updated_at'   => date('Y-m-d H:i:s'),
                    'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                );
            }
            else{
                $param = array(
                    'filename'  => cleartext($this->input->post('filename')),
                    'deskripsi'  => cleartext($this->input->post('deskripsi')),
                    'kategori'  => cleartext($this->input->post('kategori')),
                    'updated_at'   => date('Y-m-d H:i:s'),
                    'updated_by'   => cleartext($this->session->userdata('admin_data')->username),
                );
            }
            $this->m_model->updateas('id', $this->input->post('id'), $param, 'file');
            redirect('panel/file', 'refresh');
        }
        //---
        if ($this->input->get('remove')) {
            
            $this->m_model->destroy($this->input->get('remove'),'file');
            redirect('panel/file', 'refresh');
        }
    }

    

}
