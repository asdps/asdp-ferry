<?php

Class M_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'file'));
        $this->load->library('session');
    }

    function exist($data, $dbase) {
        $this->db->select('id');
        foreach ($data as $key => $val) {
            $this->db->where($key, $val);
        }

        $query = $this->db->get($dbase);
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function create($data, $dbase) {
        if ($this->db->insert($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function insertgetid($data, $dbase) {
        $this->db->insert($dbase, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function select($dbase, $colom_order=null, $order_type=null) {
        $this->db->select('*');
        if($colom_order!=null){
            if($order_type ==null){
                $order_type='DESC';
            }
            $this->db->order_by($colom_order, $order_type);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectaslike($as, $data, $dbase, $colom_order=null, $order_type=null) {
        $this->db->select('*');
        $this->db->like($as, clearText($data));
        if($colom_order!=null && $order_type!=null){
            $this->db->order_by($colom_order, $order_type);
        }
        $query = $this->db->get($dbase);
        return $query->result();
    }

    function selectasnotlikemax($as, $like, $max, $dbase) {
        $this->db->select("*");
        $this->db->where($as.'!='.clearText($like));
        $this->db->limit($max);
        $query = $this->db->get($dbase);
        return $query->result();
    }

    function selectmax($max, $dbase) {
        $this->db->select("*");
        $this->db->limit($max);
        $query = $this->db->get($dbase);
        return $query->result();
    }

    function selectasmax($max, $where, $dbase, $key_order='id', $type_order=null) {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->limit($max);
        if($key_order!=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by($key_order, $type_order);
        }
        $query = $this->db->get($dbase);
        return $query->result();
    }

    function selectas($as, $data, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        if($key_order!=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by($key_order, $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectwhere($as, $data, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        if($key_order!=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by($key_order, $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectasgroup($as, $data, $group, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        $this->db->group_by($group);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectas2($as, $data, $as2, $data2, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        $this->db->where($as2, clearText($data2));
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectas3($as, $data, $as2, $data2, $as3, $data3, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        $this->db->where($as2, clearText($data2));
        $this->db->where($as3, clearText($data3));
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectas4($as, $data, $as2, $data2,$as3, $data3,$as4, $data4, $dbase, $custom_query=null) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        $this->db->where($as2, clearText($data2));
        $this->db->where($as3, clearText($data3));
        $this->db->where($as4, clearText($data4));
        // $this->db->where($custom_query);
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectas5($as, $data, $as2, $data2,$as3, $data3,$as4, $data4,$as5, $data5, $dbase, $custom_query=null) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        $this->db->where($as2, clearText($data2));
        $this->db->where($as3, clearText($data3));
        $this->db->where($as4, clearText($data4));
        $this->db->where($as5, clearText($data5));
        // $this->db->where($custom_query);
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectcustom($query) {
        $query = $this->db->query($query);

        return $query->result();
    }

    function querycustom($query) {
        $query = $this->db->query($query);
        if($query){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    function updateas($as, $asid, $data, $dbase) {
        $this->db->where($as, $asid);
        if ($this->db->update($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function updateas2($as, $asid, $as2, $asid2, $data, $dbase) {
        $this->db->where($as, $asid);
        $this->db->where($as2, $asid2);
        if ($this->db->update($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function updateas3($as, $asid, $as2, $asid2, $as3, $asid3, $data, $dbase) {
        $this->db->where($as, $asid);
        $this->db->where($as2, $asid2);
        $this->db->where($as3, $asid3);
        if ($this->db->update($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function updateas4($as, $asid, $as2, $asid2, $as3, $asid3, $as4, $asid4, $data, $dbase) {
        $this->db->where($as, $asid);
        $this->db->where($as2, $asid2);
        $this->db->where($as3, $asid3);
        $this->db->where($as4, $asid4);
        if ($this->db->update($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function updateas5($as, $asid, $as2, $asid2, $as3, $asid3, $as4, $asid4, $as5, $asid5, $data, $dbase) {
        $this->db->where($as, $asid);
        $this->db->where($as2, $asid2);
        $this->db->where($as3, $asid3);
        $this->db->where($as4, $asid4);
        $this->db->where($as5, $asid5);
        if ($this->db->update($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function selecteng($dbase) {
        $this->db->select('*');
        $this->db->where("lang", "eng");
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectaslang($as, $data, $dbase) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        if (get_cookie('lang_is') === 'en') {
            $this->db->where('lang', 'en');
        } else {
            $this->db->where('lang', 'id');
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectaslang2($as, $data, $as2, $data2, $by, $dbase) {
        $this->db->select('*');
        $this->db->where($as, clearText($data));
        $this->db->where($as2, clearText($data2));
        if (get_cookie('lang_is') === 'en') {
            $this->db->where('lang', 'en');
        } else {
            $this->db->where('lang', 'id');
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function updateall($data, $dbase) {
        if ($this->db->update($dbase, $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function deleteas($as, $data, $dbase) {
        $this->db->where($as, $data);
        if ($this->db->delete($dbase)) {
            return 1;
        } else {
            return 0;
        }
    }
    function deleteas2($as, $data, $as2, $data2, $dbase) {
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        if ($this->db->delete($dbase)) {
            return 1;
        } else {
            return 0;
        }
    }

    function destroy($id,$db){
        $this->db->where('id', $id);
        $this->db->delete($db);
    }

    function loginData($data) {
        $condition = "email ="."'".clearText($data['email'])."' AND "."password ="."'".md5($data['password'])
            ."' AND "."user_role ="."'".$data['user_role']."'";
        $this->db->select('*');
        $this->db->where($condition);
        $this->db->where('active', 1);
        $this->db->limit(1);
        $query = $this->db->get('user');
        return $query->result();
    }

    function loginSession($data) {
        $condition = "email ="."'".clearText($data['email'])."' AND "."password ="."'".md5($data['password'])
            ."' AND "."user_role ="."'".$data['user_role']."'";
        $this->db->select('*');
        $this->db->where($condition);
        $this->db->where('active', 1);
        $this->db->limit(1);
        $query = $this->db->get('user');
        $row = $query->row();
        if ($query->num_rows() == 1) {
            switch ($row->user_role2) {
                case 2:
                    # code...
                    $user_role2='supplier';
                    break;
                case 3:
                    # code...
                    $user_role2='marketer';
                    break;
                
                default:
                    # code...
                    $user_role2='';
                    break;
            }
            $sess_data = array(
                'user_data' => $query->row(),
                'user_role2' => $user_role2,
            );
            $this->session->set_userdata($sess_data);

            return 1;
        } else {
            return 0;
        }
    }

    function updateSession($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get('user');
        $row = $query->row();
        if ($query->num_rows() == 1) {
            switch ($row->user_role2) {
                case 2:
                    # code...
                    $user_role2='supplier';
                    break;
                case 3:
                    # code...
                    $user_role2='marketer';
                    break;
                
                default:
                    # code...
                    $user_role2='';
                    break;
            }
            $sess_data = array(
                'user_data' => $query->row(),
                'user_role2' => $user_role2,
            );
            $this->session->set_userdata($sess_data);

            return 1;
        } else {
            return 0;
        }
    }

    function loginadmin($data) {
        $condition = "username ="."'".clearText($data['email'])."'";
        // print_r($condition);
        // die();
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        $row   = $query->row();
        // print_r($query);
        // die();
        if ($query->num_rows() == 1) {
            // if(password_verify($data['password'], $row->password)){
                $sess_data = array(
                    'admin'      => 1,
                    'admin_data' => $query->row(),
                );
                // print_r($sess_data);
                
                $this->session->set_userdata($sess_data);
                // print_r($this->session->userdata('admin_data'));
                // die();
                return 1;
            // }else{
            //     return 0;
                
            // }
            
        } else {
            return 0;
        }
    }

// ================================== NEW FUNCTION FOR A ONE TO MANY CALL //
    function all($db,$by='id',$order='DESC'){
        $query = $this->db->order_by($by, $order)->get($db);
        return $query->result();
    }

    function getOne($id,$db){
        $this->db->select('*');
        $this->db->where('id', $id);
        $cek = $this->db->get($db)->row_array();

        return $cek;
    }

    function createNew($data, $dbase) {
        return $this->db->insert($dbase, $data);
    }

    function selectWhere2($as, $data, $as2, $data2, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectWhere3($as, $data, $as2, $data2,$as3, $data3, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        $this->db->where($as3, $data3);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->result();
    }

    function selectOne($as, $data, $db,$key_order='id', $type_order='desc') {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->order_by(strtolower($key_order), $type_order);
        $query = $this->db->get($db);

        return $query->row();
    }

    function selectOneWhere2($as, $data, $as2, $data2, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->row_array();
    }

    function selectOneWhere3($as, $data, $as2, $data2,$as3, $data3, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        $this->db->where($as3, $data3);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->row();
    }

    function selectOneWhere4($as, $data, $as2, $data2,$as3, $data3, $as4, $data4, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        $this->db->where($as3, $data3);
        $this->db->where($as4, $data4);
        $this->db->where($as4, $data4);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->row();
    }

    function selectOneWhere5($as, $data, $as2, $data2,$as3, $data3, $as4, $data4, $as5, $data5, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        $this->db->where($as3, $data3);
        $this->db->where($as4, $data4);
        $this->db->where($as5, $data5);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->row();
    }

    function selectOneWhere6($as, $data, $as2, $data2,$as3, $data3, $as4, $data4, $as5, $data5,$as6, $data6, $dbase, $key_order='id', $type_order=null) {
        $this->db->select('*');
        $this->db->where($as, $data);
        $this->db->where($as2, $data2);
        $this->db->where($as3, $data3);
        $this->db->where($as4, $data4);
        $this->db->where($as5, $data5);
        $this->db->where($as6, $data6);
        if($key_order !=null){
            if($type_order ==null){
                $type_order='DESC';
            }
            $this->db->order_by(strtolower($key_order), $type_order);
        }
        $query = $this->db->get($dbase);

        return $query->row();
    }
}

?>