<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;


function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function sendsMaiils($dataJ = []){
    // print_r($dataJ);
    // die();
    $url = "https://ayokulakan.com/api/xample-email";
    // $url = 'http://localhost/proj_olshop/public/api/xample-email';
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    // curl_setopt($ch, CURLOPT_URL, $url);
    // // curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJ);
    // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_FAILONERROR, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJ);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $data = curl_exec($ch);
    curl_close($ch);

}

function encode_img_base64( $img_path = false, $img_type = 'png' ){
    if( $img_path ){
        //convert image into Binary data
        $img_data = fopen ( $img_path, 'rb' );
        $img_size = filesize ( $img_path );
        $binary_image = fread ( $image_data, $img_size );
        fclose ( $img_data );

        //Build the src string to place inside your img tag
        $img_src = "data:image/".$img_type.";base64,".str_replace ("\n", "", base64_encode ( $binary_image ) );
        return $img_src;
    }
    return false;
}

function sendEMAIL($data = '',$urlS = ''){     
    $html = '
    <!doctype html>
    <html>
      <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Fasilitas ASDP Indonesia Ferry</title>
        <style>body,table td{font-family:sans-serif;font-size:14px}.body,body{background-color:#f6f6f6}.btn,.btn a,.content,.wrapper{box-sizing:border-box}.btn a,h1{text-transform:capitalize}.align-center,.btn table td,.footer,h1{text-align:center}.clear,.footer{clear:both}.first,.mt0{margin-top:0}.last,.mb0{margin-bottom:0}img{border:none;-ms-interpolation-mode:bicubic;max-width:100%}body{-webkit-font-smoothing:antialiased;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}.container,.content{display:block;max-width:580px;padding:10px}table{border-collapse:separate;mso-table-lspace:0;mso-table-rspace:0;width:100%}table td{vertical-align:top}.body{width:100%}.btn a,.btn table td{background-color:#fff}.container{Margin:0 auto!important;width:580px}.btn,.footer,.main{width:100%}.content{Margin:0 auto}.main{background:#fff;border-radius:3px}.wrapper{padding:20px}.content-block{padding-bottom:10px;padding-top:10px}.footer{Margin-top:10px}h1,h2,h3,h4,ol,p,ul{font-family:sans-serif;margin:0}.footer a,.footer p,.footer span,.footer td{color:#999;font-size:12px;text-align:center}h1,h2,h3,h4{color:#000;font-weight:400;line-height:1.4;Margin-bottom:30px}.btn a,a{color:#3498db}h1{font-size:35px;font-weight:300}.btn a,ol,p,ul{font-size:14px}ol,p,ul{font-weight:400;Margin-bottom:15px}ol li,p li,ul li{list-style-position:inside;margin-left:5px}a{text-decoration:underline}.btn a,.powered-by a{text-decoration:none}.btn>tbody>tr>td{padding-bottom:15px}.btn table{width:auto}.btn table td{border-radius:5px}.btn a{border:1px solid #3498db;border-radius:5px;cursor:pointer;display:inline-block;font-weight:700;margin:0;padding:12px 25px}.btn-primary a,.btn-primary table td{background-color:#3498db}.btn-primary a{border-color:#3498db;color:#fff}.align-right{text-align:right}.align-left{text-align:left}.preheader{color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0}hr{border:0;border-bottom:1px solid #f6f6f6;Margin:20px 0}@media only screen and (max-width:620px){table[class=body] h1{font-size:28px!important;margin-bottom:10px!important}table[class=body] a,table[class=body] ol,table[class=body] p,table[class=body] span,table[class=body] td,table[class=body] ul{font-size:16px!important}table[class=body] .article,table[class=body] .wrapper{padding:10px!important}table[class=body] .content{padding:0!important}table[class=body] .container{padding:0!important;width:100%!important}table[class=body] .main{border-left-width:0!important;border-radius:0!important;border-right-width:0!important}table[class=body] .btn a,table[class=body] .btn table{width:100%!important}table[class=body] .img-responsive{height:auto!important;max-width:100%!important;width:auto!important}}@media all{.btn-primary a:hover,.btn-primary table td:hover{background-color:#34495e!important}.ExternalClass{width:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}.apple-link a{color:inherit!important;font-family:inherit!important;font-size:inherit!important;font-weight:inherit!important;line-height:inherit!important;text-decoration:none!important}.btn-primary a:hover{border-color:#34495e!important}}
        </style>
      </head>
      <body>
        <table border="0" cellpadding="0" cellspacing="0" class="body">
          <tr>
            <td>&nbsp;</td>
            <td class="container">
              <div class="content">
                <span class="preheader">ASDP Indonesia Ferry</span>
                <table class="main">
                  <tr>
                    <td class="wrapper">
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td>
                            <center><a href=""><img src="http://databasefasilitas.indonesiaferry.id/images/logo-r.png" style="width: 350px;"></a></center>
                           <hr style="border-width: 5px;color:#545454;line-height:1.5;">
                            <br>
                            <p id="to">Anda Mempunyai Pesan baru!</p>
                            <p>';
                              if($data){
                                foreach ($data as $k => $value) {
                                  $html .= $k.' : '.$value.'<br>';
                                }
                              }
                            
                            $html .= '</p>
                              <br><br>
                              <hr>
                              <p style="text-align: right;">
                                Thanks and Regards,<br>
                                Divisi Perencanaan Pelayanan dan Fasilitas
                              </p>
                            </p>
                            <br>
                          </td>
                        </tr>
                      </table>

                    <br>
                    <div style="background: #FAFAFA; width: 100%; height: 30px; padding-top: 15px;">
                    <p style="color:#999;line-height:1.5; text-align: center;">
                    2019@fasilitas</a>
                    </p>
                    </div>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>

      </body>
    </html>'; 
    $mail = new PHPMailer;        
    $mail->isSMTP();
    $mail->Host = 'mail.indonesiaferry.co.id';
    $mail->Username = 'dbfasilitas@indonesiaferry.co.id'; 
    $mail->Password = '@5dp-123';
    $mail->Port = 25;
    // $mail->Host = 'smtp.gmail.com';
    // $mail->Username = 'legrondhibebzky@gmail.com'; 
    // $mail->Password = 'legron26801';
    // $mail->Port = 587;
    $mail->SMTPAuth = true;
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
    $mail->SMTPSecure = 'TLS';
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->setFrom('dbfasilitas@indonesiaferry.co.id', 'Fasilitas ASDP Indonesia Ferry');        
    $mail->addAddress($data['email'], $data['email']);        
    $mail->isHTML(true);
    $mail->Subject = 'Pesan Baru Dari Fasilitas ASDP Indonesia Ferry';        
    $mail->Body = $html;        
    $mail->send();
}
