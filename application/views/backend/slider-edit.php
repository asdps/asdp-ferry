<?php include 'header.php'; ?>
<?php 
    $dataSes=$this->session->flashdata('sukses');
    if($dataSes!=""){ ?>
    <center><div class="alert alert-success"><strong>Sukses! </strong> <?=$dataSes;?></div></center>
    <?php } ?>
    <?php 
    $dataSes2=$this->session->flashdata('error');
    if($dataSes2!=""){ ?>
    <center><div class="alert alert-danger"><strong> Gagal! </strong> <?=$dataSes2;?></div></center>
<?php } ?>
<div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>Add Slider</h2>
                    </div>
                    <div class="body">
                        <form class="form-horizontal" action="<?= site_url('backend/slider/update/'.$record->id); ?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $record->id ?>">
                            <div class="row clearfix">
                                <div class="form-group col-lg-6">
                                    <div class="form-line">
                                        <label for="name">Judul</label>
                                        <input type="text" name="name" class="form-control" value="<?=  $record->name ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                        <label>Foto Slider</label>
                                        <input name="cover" type="file" class="form-control" style="cursor: pointer;" accept="image/*">
                                        <?php
                                            $img=check_img($record->url);
                                        ?>
                                        <center>
                                            <img src="<?=$img['path'];?>" class="img-fluid" style="max-height: 100px;max-width: 150px">
                                        </center>
                                </div>
                            </div>
                            <div class="row clearfix" style="margin-top: 20px;">
                                <div class="col-lg-6">
                                    <a href="<?= site_url('backend/slider'); ?>" class="btn btn-block btn-danger">Back</a>
                                </div>
                                <div class="col-lg-6">
                                    <input name="add" type="submit" value="Add" class="btn btn-block btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

<?php include 'footer.php'; ?>