<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="icon" href="<?= site_url('images/ico.jpg') ?>" sizes="192x192">
  <title>ASDP</title>
  <style>
    body {
      font-family: Arial, Helvetica Neue, Helvetica, sans-serif; 
    }

    @page {
      margin: 20px 0px;
    }


    body {
      margin-top: 5cm;
      margin-bottom: -20px;
      /*margin-left: 0cm;
      margin-right: 0cm;*/
      font-style: normal;

    }

    .ui.table.bordered {
      border: solid black 2px;
      border-collapse: collapse;
      width: 100%;
    }

    .ui.table.bordered td {
      border: solid black 1px;
      border-collapse: collapse;
      /*padding:10px;*/
    }

    .ui.table.bordered td img {
      padding: 10px;
    }

    .ui.table.bordered td.center.aligned {
      text-align : center;
    }


    header {
      position: fixed;
      top: -55px;
      height: 50px;
      text-align: center;
      line-height: 35px;
    }

    main {
      position: sticky;
      font-size : 12px;
      /*margin: 20px 20px 20px 20px;*/
      margin-bottom : 40px;
      margin-left: 1px;
      margin-right: 1px;
      border: solid white 1.5px !important; 
      /*border-collapse: collapse;*/
      width: 100%;
    }

    footer {
      position: fixed;
      bottom: -60px;
      left: 0px;
      right: 0px;
      height: 50px;


      text-align: center;
      line-height: 35px;
      clear: both;
    }
    .footer .page-number:after {
      content: counter(page);
    }

    .col-6 {
      -webkit-box-flex: 0;
      -ms-flex: 20% 20% 50%;
      flex: 20% 20% 50%;
      max-width: 50%;
    }

    .row {
      /*border: #c0c5c7 1px dashed;*/
      margin: auto;
      margin-right: 0px;
      margin-left: 0px;
      margin-top: auto;
    }
    .col-sm-4 {
      width: 63.33333333%;
    }
    .col-sm-3 {
      width: 27%;
    }
    .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
      float: left;
    }
    .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
      position: relative;
      min-height: 1px;
      padding-right: 15px;
      padding-left: 15px;
    }

    /* Create three equal columns that floats next to each other */
    .column {
      float: left;
      
      height: 300px; /* Should be removed. Only for demonstration */
    }

    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
  </style>



</head>
<body>    
  <header>

    <table class="ui table" style="width: 100%;position: relative;">
      <tr>
        <td class="" style="width: 400px">
          <center style="position:relative;top:45px;"><img src="<?= dirname($_SERVER['SCRIPT_FILENAME']).'/images/logo-r.png' ?>" style="width: 350px;"></center>
          <div style="font-size: 8px;position: relative;left:80px;top: 30px;color: #6f7176;"><b>PT. ASDP Indonesia Ferry (Persero)</b><span style="margin-left: 50px">https://www.indonesiaferry.co.id</span></div>
          <div style="font-size: 8px;position: relative;left:80px;top: 10px;color: #6f7176;">Jl. Jend. Ahmad Yani Kav. 52 A <span style="margin-left: 70px">pelanggan@indonesiaferry.co.id</span></div>
          <div style="font-size: 8px;position: relative;left:80px;top: -10px;color: #6f7176;">Cempaka Putih Timur,Kota Jakarta Pusat <span style="margin-left: 40px">(021) 191</span></div>

        </td>
        <td class="center aligned">
          <div class="row">
            <div class="column" style="background-color:#ffd0a1;width: 20px;height: 100px;-webkit-transform:skew(60deg);-moz-transform:skew(60deg);-o-transform:skew(-20deg);transform:skew(-20deg);margin-right: 5px;position: relative;left:30px;top: 30px;"></div>
            
            <div class="column" style="background-color:#4a599f;width: 20px;height: 60px; -webkit-transform:skew(60deg);-moz-transform:skew(60deg);-o-transform:skew(-20deg);transform:skew(-20deg);margin-right: 5px;position: relative;left: 65px;top: 52px;">
            </div>
            <div class="column" style="background-color:#dbdeec;width: 430px;height: 60px; -webkit-transform:skew(-21deg);-moz-transform:skew(-21deg);-o-transform:skew(-20deg);transform:skew(-20deg);position:relative;top: 52px;">
              <h4 style="position: relative;top: -10px;left:20px;color: #6f7176;font-size: 16px;">FORM LAPORAN <?= $data['judul']; ?></label>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="2" rowspan="" headers="" style="background-color: #ffd0a1;"></td>
        </tr>
        <tr>
          <td class="" colspan="2" style="background-color: #dbdeec;color: #6f7176;font-size:13px;">
            <center><?= isset($data['record']->name) ? $data['record']->name : '-'; ?></center>
          </td>
        </tr>
        <tr>
          <td colspan="2" rowspan="" headers="" style="background-color: #ffd0a1;"></td>
        </tr>
      </table>
    </header>

    <?php
    if(isset($data['record']->foto)){
      if(strlen($data['record']->foto) > 13){
        $img= checkImgByBase($data['record']->foto);
      }else{
        $img['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
      }
    }else{
      $img['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
    }

    if(isset($data['record']->simetris)){
      if($data['record']->simetris){
        if(strlen($data['record']->simetris) > 13){
          $imgSimet=checkImgByBase($data['record']->simetris); 
        }else{
          $imgSimet['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
        }
      }else{
        $imgSimet['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
      }
    }else{
      $imgSimet['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
    }
    ?>
    <center><img src="<?= $img['path']; ?>" style="max-height:720px;height: auto;max-width: 650px;width: auto;"></center>

    <table class="ui table" style="font-size: 14px;width: 100%;page-break-after: always">
      <tbody>
        <tr>
          <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
            <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
              <span style="">&nbsp;&nbsp;Deskripsi</span>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="" rowspan="" headers="" style="background-color: #ebecec;font-size: 11px">
            <p style="padding-left: 5px">
              <?php
                if(isset($data['record']->deskripsi)){
                  if(!is_null($data['record']->deskripsi) || strlen($data['record']->deskripsi > 5)){
                    echo str_replace('\"', '"', $data['record']->deskripsi);
                  }
                }
              ?>
            </p>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="ui table" style="font-size: 14px;width: 100%;page-break-after: always;">
      <tbody>
        <tr>
          <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
            <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
              <span style="">&nbsp;&nbsp;Isometri</span>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="" rowspan="" headers="">
            <center><img src="<?= $imgSimet['path']; ?>" style="max-height:720px;height: auto;max-width: 650px;"></center>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="ui table" style="font-size: 14px;width: 100%;page-break-after: always;">
      <tbody>
        <tr>
          <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
            <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
              <span style="">&nbsp;&nbsp;Zonasi</span>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="" rowspan="" headers="">
            <center>
              <?php
              if(isset($data['record']->path_zonasi)){
                if(strlen($data['record']->path_zonasi) > 13){
                  $imgZonasi=checkImgByBase($data['record']->path_zonasi);
                }else{
                  $imgZonasi['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                }
              }else{
                $imgZonasi['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
              }
              ?>
              <img src="<?= $imgZonasi['path']; ?>" style="max-height:720px;height: auto;max-width: 650px;">
            </center>
            <p style="padding-left: 5px;background-color: #ebecec;font-size: 11px">
              <?= isset($data['record']->deskripsi_zonasi) ? $data['record']->deskripsi_zonasi: '-' ?>
            </p>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="ui table" style="font-size: 14px;width: 100%;page-break-after: always;">
      <tbody>
        <tr>
          <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
            <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
              <span style="">&nbsp;&nbsp;Sirkulasi</span>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="" rowspan="" headers="">
            <center>
              <?php
              if(isset($data['record']->path_sirkular)){
                if(strlen($data['record']->path_sirkular) > 13){
                  $imgSirkular=checkImgByBase($data['record']->path_sirkular);
                }else{
                  $imgSirkular['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                }
              }else{
                $imgSirkular['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
              }
              ?>
              <img src="<?= $imgSirkular['path']; ?>" style="max-height:720px;height: auto;max-width: 650px;">
            </center>
            <p style="padding-left: 5px;background-color: #ebecec;font-size: 11px">
            <?= isset($data['record']->deskripsi_sirkular) ? $data['record']->deskripsi_sirkular: '-' ?>
           </p>
         </td>
       </tr>
     </tbody>
   </table>

   <main>
     <?php
     $jenisAspek = $this->m_model->selectas('status','Pelabuhan','jenis_aspeks');
     if(count($jenisAspek)){
      foreach ($jenisAspek as $k1 => $value1) {
        ?>
        <table class="ui table" style="font-size: 13px;width: 100%;page-break-after: always;">
          <tbody>
            <tr>
              <td colspan="" rowspan="" headers="" style="color: white;vertical-align: center;">
                <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
                  <span style="padding-left: 20px">&nbsp;&nbsp;<?= $value1->nama_aspek; ?> </span>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="" rowspan="" headers="">
                <?php
                $cekCanvas = $this->m_model->selectOneWhere2('id_pelabuhan',$data['record']->id,'id_jenis_aspek',$value1->id,'pelabuhans_canvas');
                if($cekCanvas){
                  if(isset($cekCanvas['url_canvas'])){
                    if(strlen($cekCanvas['url_canvas']) > 23){
                      $imgCanvs=checkImgByBase($cekCanvas['url_canvas']);
                    }else{
                      $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                    }
                  }else{
                    if(isset($data['record']->foto_drag)){
                      if(strlen($data['record']->foto_drag) > 23){
                        $imgCanvs=checkImgByBase($data['record']->foto_drag);
                      }else{
                        $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                      }
                    }else{
                      $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                    }
                  }
                }else{
                  if(isset($data['record']->foto_drag)){
                    if(strlen($data['record']->foto_drag) > 23){
                      $imgCanvs=checkImgByBase($data['record']->foto_drag);
                    }else{
                      $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                    }
                  }else{
                    $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                  }
                }
                ?>
                <center><img src="<?= $imgCanvs['path']; ?>" style="max-height:720px;height: auto;max-width: 650px;width: auto;"></center> 
                <?php

                ?>
              </td>
            </tr>
          </tbody>
        </table>
        <?php
        $subAspek = $this->m_model->selectas('jenis_aspek_id',$value1->id,'sub_aspeks','created_at','ASC');
        if(count($subAspek) > 0){
          foreach ($subAspek as $k2 => $value2) {
              $checkPbHslTrue = $this->m_model->selectcustom("select * from trans_pelabuhans_hasil where id_pelabuhan=".$data['record']->id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value2->id." group by icon_id");
              if(count($checkPbHslTrue) > 0){
                ?>
                <table class="" style="font-size: 13px;width: 100%;border-collapse: collapse;page-break-after: always">
                  <tbody style="">
                    <tr>
                      <td colspan="" rowspan="" headers="" style="width: 10px">
                        <label></label>
                      </td>
                      <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;color: black;">
                        <div style="border-bottom: 25px solid #ffd0a1;border-right: 50px solid transparent;height: 0;width: 500px;">
                          <span style="">&nbsp;&nbsp;<?= $value2->name; ?></span>
                        </div>
                        &nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                  </tbody>
                  <?php
                foreach ($checkPbHslTrue as $k3 => $value3) {
                  $getIcon = $this->m_model->selectOne('id',$value3->icon_id,'icon');
                  $checkPbHslALL = $this->m_model->selectcustom("select * from trans_pelabuhans_hasil where id_pelabuhan=".$data['record']->id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value2->id." and icon_id=".$value3->icon_id."");
                  $checkPbHslALLLimit = $this->m_model->selectcustom("select * from trans_pelabuhans_hasil where id_pelabuhan=".$data['record']->id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value2->id." and icon_id=".$value3->icon_id." limit 3");
                  if(count($checkPbHslALL) > 3){
                    $cekOneHslPl = $this->m_model->selectOneWhere4('id_pelabuhan',$data['record']->id,'id_jenis_aspek',$value1->id,'id_sub_jenis_aspek',$value2->id,'icon_id',$value3->icon_id,'trans_pelabuhans_hasil');
                    array_push($checkPbHslALLLimit,$cekOneHslPl);
                  }
                  ?>
                  <tbody>
                    <?php
                    if($k3 > 2){
                      ?>
                      <tr>
                        <td colspan="" rowspan="" headers="" style="width: 10px">
                        </td>
                        <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;color: black;height: 20px">
                          <div style="border-right: 50px solid transparent;height: 0;width: 500px;height: 10px">
                            <span style="">&nbsp;&nbsp;</span>
                          </div>
                        </td>
                      </tr> 
                      <?php
                    }
                    ?>
                    <tr>
                      <td colspan="" rowspan="" headers="" style="width: 10px">
                        <label></label>
                      </td>
                      <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;page-break-after: avoid;">
                        <ul style="list-style: square">
                          <li>
                            <b>
                              <?= $getIcon->name; ?>
                            </b>  
                          </li>
                        </ul>
                      </td>
                    </tr>
                  </tbody>
                  <tbody>
                    <tr>
                      <td colspan="" rowspan="" headers="">

                      </td>
                      <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;">
                        <div style="background-color: white;">
                          <div class="row" style="position: relative;left: 15px">
                            <?php
                            if(count($checkPbHslALLLimit) > 0){
                                    // $split = 4;
                                    // $HslArmdAll1 = array_chunk($checkPbHslALL, $split);
                              $checkPbHslALL1 = $checkPbHslALLLimit;
                              foreach ($checkPbHslALL1 as $k4 => $value4) {
                                ?>
                                <div class="col-sm-2" style="background-color: #ebecec;margin-bottom: 10px;top:10px;margin-right: 10px;height: 190px;width:150px;font-size: 11px;max-width: 150px"
                                >
                                Nomor : <?= $value4->nomor; ?><br>
                                Kondisi : <?= (strlen($value4->kondisi) > 60) ? substr($value4->kondisi,0,60).' ...' : $value4->kondisi; ?><br>
                                Posisi : <?= (strlen($value4->posisi) > 60) ? substr($value4->posisi,0,60).' ...' : $value4->posisi; ?><br>
                                Tahun Pengadaan : <?= $value4->tahun; ?><br>
                                Lampiran Foto :<br>
                                <?php
                                $cekFiless = $this->m_model->selectOne('trans_id',$value4->id,'trans_pelabuhans_hasil_foto');
                                if($cekFiless){
                                  if(isset($cekFiless->fileurl)){
                                    if(strlen($cekFiless->fileurl) > 13){
                                      $imgFf=checkImgByBase($cekFiless->fileurl);
                                      if(is_file($imgFf['path'])){
                                      ?>
                                        <center style="padding-top:5px"><img src="<?= $imgFf['path']; ?>" width="150px" height="90px" style=""></center>
                                      <?php
                                      }else{
                                        $cekFiless = $this->m_model->selectas('trans_id',$value4->id,'trans_pelabuhans_hasil_foto');
                                        $imgFf['path'] = '';
                                        if(count($cekFiless) > 0){
                                          foreach ($cekFiless as $kFiles => $vFiles) {
                                            if(is_file(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$vFiles->fileurl)){
                                              $imgFf=checkImgByBase($vFiles->fileurl);
                                              break;
                                            }
                                          }
                                          ?>
                                            <center style="padding-top:5px"><img src="<?= $imgFf['path']; ?>" width="150px" height="90px" style=""></center>
                                          <?php
                                        }
                                      }
                                    }
                                  }
                                }
                                ?>
                              </div>
                              <?php
                            }
                          }
                          ?>
                        </div>
                      </div>

                    </td>
                  </tr>

                </tbody>
                <?php
              }
            }
            ?>
          </table>
          <?php
        }
      }
    }
  }
  ?>

</div>

<?php
$jenisAspek = $this->m_model->selectas('status','Pelabuhan','jenis_aspeks');
if(count($jenisAspek) > 0){
  $ampasNo = 0;
  $colspanNo = 0;
  ?> 
  <table class="ui table bordered" style="font-size: 13px;width: 100%;">
    <?php
    foreach ($jenisAspek as $k => $jenisAspek) {
      $subAspek = $this->m_model->selectas('jenis_aspek_id',$jenisAspek->id,'sub_aspeks','created_at','ASC');
      ?>
      <tbody>
        <tr>
          <td colspan="" rowspan="" headers="" style="width: 80px;background-color: #ffe5d3;">
            <center><b><label><?= $jenisAspek->nama_aspek; ?></label></b></center>
          </td>
        </tr>
        <?php
        if(count($subAspek) > 0){
          foreach ($subAspek as $k1 => $value1) {
            $checkPbHslTrue = $this->m_model->selectcustom("select * from trans_pelabuhans_hasil where id_pelabuhan=".$data['record']->id." and id_jenis_aspek=".$jenisAspek->id." and id_sub_jenis_aspek=".$value1->id." group by icon_id");
            ?>
            <tr>
              <td colspan="" rowspan="" headers="">
                <b><label style="position: relative;padding-left: 10px"><?= $k1+1; ?>. <?= $value1->name; ?> ( <?= $jenisAspek->nama_aspek; ?> ) </label></b>
                <ul>
                  <?php
                  if(count($checkPbHslTrue) > 0){
                    foreach ($checkPbHslTrue as $k2 => $value2) {
                      $getIcon = $this->m_model->selectOne('id',$value2->icon_id,'icon');
                      $checkPbHslALL = $this->m_model->selectcustom("select * from trans_pelabuhans_hasil where id_pelabuhan=".$data['record']->id." and id_jenis_aspek=".$jenisAspek->id." and id_sub_jenis_aspek=".$value1->id." and icon_id=".$value2->icon_id."");

                      $nominal = 0;
                      if(count($checkPbHslALL) > 0){
                        foreach ($checkPbHslALL as $kS => $valueS) {
                          $nominal += (int)$valueS->total_icon;
                        }
                      }
                      ?>
                      <li>
                        <?= $getIcon->name; ?>
                        <ul style="list-style-type: lower-alpha;">
                          <li>Jumlah :<?= count($checkPbHslALL) ?></li>
                          <li>Unit   : <?= $nominal; ?></li>
                        </ul>    
                      </li>
                      <?php
                    }
                  }
                  ?>
                </ul>    
              </td>
            </tr>
            <?php
          }
        }
        ?>
      </tbody>
      <?php
    }
    ?>
  </table>
  <?php
}
?>
</main>
</body>
</html>
