<script src="<?=base_url();?>assets/frontend/js/jquery.js"></script> 
<script src="<?=base_url();?>assets/frontend/js/konva.min.js"></script>
<script src="<?=base_url();?>assets/frontend/js/html2canvas.min.js"></script>
<?php include 'header.php'; ?>
<style type="text/css">
  #ceks {
    width: 100%;
    height: 100%;
  }
</style>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="card">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="btn-group">
              <a href="<?=base_url();?>panel/pelabuhan?detail=<?= $pelabuhanss->id ?>&page=<?= $page; ?>"  class="btn btn-primary btn-sm" style="color: #fff">Kembali</a>
              <?php
              if($this->session->userdata('admin_data')->roles == 4){
                ?>
                  &nbsp;<a href="<?=base_url();?>backend/pelabuhan/laporan/<?= $record->id ?>/<?= $pelabuhanss->id ?>"  target="_blank" class="btn btn-danger btn-sm" style="color: #fff">Print Out</a>
                <?php
              }else{
                ?>
                &nbsp;<a href="<?=base_url();?>backend/pelabuhan/edit/edit/<?= $record->id ?>/<?= $pelabuhanss->id ?>?page=<?= $page; ?>"  class="btn btn-success btn-sm" style="color: #fff">Edit</a>
                &nbsp;<a href="<?=base_url();?>backend/pelabuhan/laporan/<?= $record->id ?>/<?= $pelabuhanss->id ?>"  target="_blank" class="btn btn-danger btn-sm" style="color: #fff">Print Out</a>
                <?php
              }
              ?>
            </div>
          </div>
          <div class="col-md-12 alertLah">

          </div>
        </div>
        <div class="wrapper content">
          <div class="container-fluid">
            <h3><?= $record->nama_aspek;  ?> <?= $pelabuhanss->name; ?></h3>
            <div class="row">
                <?php
                $img=check_img($pelabuhanss->foto_drag);
                $cekIMG = isset($pelabuhanss->foto_drag) ? $pelabuhanss->foto_drag : 'images/images.png';
                if(file_exists(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$cekIMG)){
                  $cekIMG = $pelabuhanss->foto_drag;
                }else{
                  $cekIMG = 'images/images.png';
                }
                list($width, $height, $type, $attr) = getimagesize(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$cekIMG);
                $ratio = $width/$height;
                $widthR = 600;
                $heightR = 700/$ratio;
                ?>
                <div class="col-lg-8 mb-3 pr-0" id="ceks">
                <div style="background-image: url('<?=$img['path'];?>'); background-size: <?= $widthR; ?>px <?= $heightR; ?>px;background-repeat: no-repeat;" id="containers" data-id="containers" ></div>

              </div>
              <div class="col-lg-4 mb-3 p-0" style="background: #fffafa">
                <div class="bg-warning text-center py-1">
                  <h3 class="text-white font-weight-bold"><span style="font-weight: 500"><?= $record->nama_aspek;  ?></span></h3>
                </div>
                <div class="px-3 my-3" style="max-width: 310px;overflow-y: hidden;overflow-x: scroll;">
                  <ul style="position: relative;left: -50px;font-size: 13px">
                    <?php
                    if(count($records) > 0){
                      $no = 0;
                      foreach ($records as $k => $value) {
                        if($value->name != ''){
                          $no++;
                          ?>              
                          <li style="list-style: none;font-size: 13px">
                            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block" style="padding-top: 2px;height: 50px;width: 300px;">
                              <div class="row">
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-lg-2">
                                      <span class="rounded-circle text-white bg-warning mr-1" style="padding: 3px 8px;"><?= $no; ?></span>
                                    </div>
                                    <div class="col-lg-9">
                                      <p class="text-warning">
                                        <?= $value->name; ?>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-2" style="text-align: right;">
                                  <span class="text-warning" style="position: relative;right: 7px">Titik</span>
                                </div>
                                <div class="col-md-2">
                                  <span class="text-warning " style="position: relative;right: 7px">Unit Total</span>
                                  <br>
                                </div>
                              </div>

                            </a>
                            <ul class="ml-menu" style="display: none;font-size: 13px;position: relative;left: -5px;">
                              <?php
                              if(count($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon'))){
                                foreach ($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon') as $keySubIco => $valueSubIco) {

                                  $cekReal = $this->m_model->getOne($valueSubIco->trans_icon_id,'icon');
                                  $selectOneTotal = $this->m_model->selectas4(
                                    'id_pelabuhan', $pelabuhanss->id, 
                                    'id_jenis_aspek', $record->id, 
                                    'id_sub_jenis_aspek', $value->id, 
                                    'icon_id', $cekReal['id'], 
                                    'trans_pelabuhans_hasil'
                                  );
                                  $imgs=check_img($cekReal['path_file']);
                                  ?>
                                  <?php 
                                  $coun = 0;
                                  $color = 'background-color: #f00;color: white;border-radius:20px;border:3px solid #f00 !important';
                                  
                                  $hasilCount = count($this->m_model->selectas4('id_pelabuhan',$pelabuhanss->id,'id_jenis_aspek',$record->id,'id_sub_jenis_aspek',$value->id,'icon_id',$cekReal['id'],'trans_pelabuhans_hasil'));

                                  if($hasilCount > 0){
                                    $coun = $hasilCount;
                                    $color = '-';
                                  }
                                  ?>
                                  <li style="font-size: 13px;padding-bottom:1px;width: 270px;" >
                                    <div class="row">
                                      <div class="col-md-8">
                                        <img src="<?=$imgs['path'];?>" class="img-responsive drag dragClick" data-key="<?= $keySubIco + 1; ?>" data-id="<?= $cekReal['id']; ?>" data-aspek="<?= $value->name; ?>" data-name="<?= $cekReal['name']; ?>" style="cursor: pointer; max-width: 50px; max-height:50px;width: 30px;padding-bottom: 1px;<?= $color; ?>" data-fancybox="images<?= $keySubIco + 1; ?>" href="<?=$imgs['path'];?>" draggable="true">&nbsp;
                                        <span style="font-size: 13px;">
                                          <?= $cekReal['name']; ?>
                                        </span>
                                      </div>
                                      <div class="col-md-2">
                                        <span style="position: relative;right: 20px"><?= $coun; ?></span>
                                      </div>
                                      <div class="col-md-2">
                                          <?php
                                          $nominal = 0;
                                          if(count($selectOneTotal) > 0){
                                            foreach ($selectOneTotal as $k => $valueS) {
                                              $nominal += (int)$valueS->total_icon;
                                            }
                                          }
                                          ?>
                                          <span style="position: relative;right: 20px"><?= $nominal; ?></span>
                                          <p><?= $valueSubIco->units; ?></p>  
                                      </div>

                                    </div>
                                  </li>
                                  <hr style="width: 270px">
                                  <?php
                                }
                              }
                              ?>
                            </ul>
                          </li>
                          <?php          
                        }
                      }
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
      
      var stage = new Konva.Stage({
        container: 'containers',
        width: <?= $widthR; ?>,
        height: <?= $heightR; ?>
      });
      function fitStageIntoParentContainer() {
        var container = document.querySelector('#containers');
        // now we need to fit stage into parent
        var containerWidth = container.offsetWidth;
        // to do this we need to scale the stage
        var scale = containerWidth / container.offsetWidth;

        stage.width(container.offsetWidth * scale);
        stage.height(container.offsetHeight * scale);
        stage.scale({ x: scale, y: scale });
        stage.draw();
      }

      fitStageIntoParentContainer();
      window.addEventListener('resize', fitStageIntoParentContainer);
      
      var layer = new Konva.Layer();
      stage.add(layer);
      
      
      // ADD FOTO DARI KUMPULAN ARRAY
      var widths = 20;
      $.ajax({
        url: '<?= site_url('backend/pelabuhan/getData'); ?>',
        type: 'post',
        data: {id_jenis_aspek: '<?= $record->id; ?>',id_pelabuhan:'<?= $pelabuhanss->id; ?>'},
        dataType: 'json',
        success:function(response){
          if(response.length > 0){
            $.each(response,function(k,v){
              var group = new Konva.Group({
                x: parseFloat(v.pointer_x),
                y: parseFloat(v.pointer_y),
              });

              layer.add(group);
                    // 3. Create an Image node and add it to group
                    Konva.Image.fromURL(v.url, function(yoda) {
                      // if(v.width == null || v.width == ''){
                      //   widths = 50
                      // }else{
                      //   widths = v.width;
                      // }

                      // if(v.height == null || v.height == ''){
                      //   heights = v.width
                      // }else{
                      //   heights = v.height;
                      // }

                      if(v.width == null){
                        widths = 20
                        heights = 20
                      }else{
                        widths = v.width;
                        heights = v.width;
                      }
                      yoda.setAttrs({
                        width: widths,
                        height: heights,
                        id_pelabuhan: v.id_pelabuhan,
                        id_jenis_aspek: v.id_jenis_aspek,
                        primary_key: v.primary_key,
                        pointer_x: v.pointer_x,
                        pointer_y: v.pointer_y,
                        total_icon: v.total_icon,
                        icon_id: v.icon_id,
                        cek_target:'true'
                      });
                      // 4. Add it to group.
                      group.add(yoda);
                      layer.batchDraw(); // It
                    });

                  });  
          }
        },
        error: function() {
              // $('.alertLah').html(`
              //   <div class="alert alert-danger">
              //     Refresh Halaman Kembali
              //   </div>
              // `);
            }
          });

      
      // END ADD FOTO DARI KUMPULAN ARRAY
      
      // what is url of dragging element?
      var itemURL = '';
      var dataIds = '';
      var dataAspek = '';
      var dataName = '';
      $(document).on('dragstart','#drag-items', function(e) {
        itemURL = e.target.src;
        dataIds = e.target.dataset.id;
        $('input[name="icon_id"]').val(dataIds);
        $('input[name="url"]').val(e.target.src);
        $('input[name="aspek"]').val(e.target.dataset.aspek);
        $('input[name="nama"]').val(e.target.dataset.name);
      });

      var con = stage.container();
      con.addEventListener('dragover', function(e) {
        e.preventDefault(); 
      });

      arrayHasil = [];
      con.addEventListener('drop', function(e) {
        e.preventDefault();
        stage.setPointersPositions(e);
        console.log('e',e)
        Konva.Image.fromURL(itemURL, function(image) {
          layer.add(image);

          image.position(stage.getPointerPosition());
          image.width(20);
          image.height(20);
          image.draggable(false);
          arrayHasil.push(stage.getPointerPosition());
          console.log('arrayHasil',arrayHasil)
          layer.draw();
          $("#add-panel").modal("show");
          $('.modal-backdrop').removeClass();
          $('input[name="pointer_x"]').val(stage.getPointerPosition().x);
          $('input[name="pointer_y"]').val(stage.getPointerPosition().y);
          $('input[name="primary_key"]').val(arrayHasil.length);
          $('.deletesData').hide();
        });
      });

      stage.on('click', function(e) {
        $('.modal-dialog').addClass('ampase');
        console.log('cekClikc')
        if(e.target.attrs.cek_target === 'true'){
         $.ajax({
          url: '<?= site_url('backend/pelabuhan/getDataOne/'); ?>',
          type: 'post',
          data: {id_jenis_aspek: e.target.attrs.id_jenis_aspek, id_pelabuhan:e.target.attrs.id_pelabuhan, primary_key:e.target.attrs.primary_key,pointer_x:e.target.attrs.pointer_x,pointer_y:e.target.attrs.pointer_y},
          dataType: 'json',
          success:function(response){
            console.log('response',response)
            if(response){
              $("#add-panel").modal("show");
              $('.modal-backdrop').removeClass();

              $('input[name="id"]').val(response.record.id);
              $('input[name="id_pelabuhan"]').val(response.record.id_pelabuhan);
              $('input[name="id_jenis_aspek"]').val(response.record.id_jenis_aspek);
              $('input[name="id_sub_jenis_aspek"]').val(response.record.id_sub_jenis_aspek);
              $('input[name="icon_id"]').val(response.record.icon_id);
              $('input[name="url"]').val(response.record.url);
              $('input[name="pointer_x"]').val(response.record.pointer_x);
              $('input[name="pointer_y"]').val(response.record.pointer_y);
              $('input[name="primary_key"]').val(response.record.primary_key);
              $('input[name="kategori"]').val(response.record.kategori);

              $('.Nama').text(response.record.nama);
              $('.Aspek').text(response.record.aspek);
              $('.Nomor').text(response.record.nomor);
              $('.Kondisi').text(response.record.kondisi);
              $('.Posisi').text(response.record.posisi);
              $('.Pengadaan').text(response.record.tahun);
              $('.total_icon').text(response.record.total_icon);

              if(response.record_foto){
                $('.showImg').html('');
                var cekNo = 0;
                $.each(response.record_foto,function(k,v){
                    if(v.fileurl.length > 12){
                      cekNo++;
                      var kkNo = (cekNo == 1) ? 'active' : '';
                      $('.showImg').append(`
                        <div class="carousel-item `+kkNo+`">
                          <img src="<?php echo base_url(); ?>`+v.fileurl+`" class="img-fluid" style="width:100%;height:420px;" alt="">
                        </div>
                      `);
                    }
                })
              }
            }
          },
          error: function() {
            $('.alertLah').html(`
              <div class="alert alert-danger">
              Refresh Halaman Kembali
              </div>
              `);
          }
        });
       }
     });

      stage.on('click tap', function(e) {
        $('.modal-dialog').removeClass('ampase');
        console.log('cek Tap')
        if(e.target.attrs.cek_target === 'true'){
         $.ajax({
          url: '<?= site_url('backend/pelabuhan/getDataOne/'); ?>',
          type: 'post',
          data: {id_jenis_aspek: e.target.attrs.id_jenis_aspek, id_pelabuhan:e.target.attrs.id_pelabuhan, primary_key:e.target.attrs.primary_key,pointer_x:e.target.attrs.pointer_x,pointer_y:e.target.attrs.pointer_y},
          dataType: 'json',
          success:function(response){
            console.log('response',response)
            if(response){
              $("#add-panel").modal("show");
              $('.modal-backdrop').removeClass();

              $('input[name="id"]').val(response.record.id);
              $('input[name="id_pelabuhan"]').val(response.record.id_pelabuhan);
              $('input[name="id_jenis_aspek"]').val(response.record.id_jenis_aspek);
              $('input[name="id_sub_jenis_aspek"]').val(response.record.id_sub_jenis_aspek);
              $('input[name="icon_id"]').val(response.record.icon_id);
              $('input[name="url"]').val(response.record.url);
              $('input[name="pointer_x"]').val(response.record.pointer_x);
              $('input[name="pointer_y"]').val(response.record.pointer_y);
              $('input[name="primary_key"]').val(response.record.primary_key);
              $('input[name="kategori"]').val(response.record.kategori);

              $('.Nama').text(response.record.nama);
              $('.Aspek').text(response.record.aspek);
              $('.Nomor').text(response.record.nomor);
              $('.Kondisi').text(response.record.kondisi);
              $('.Posisi').text(response.record.posisi);
              $('.Pengadaan').text(response.record.tahun);
              $('.total_icon').text(response.record.total_icon);

              if(response.record_foto){
                $('.showImg').html('');
                var cekNo = 0;
                      $.each(response.record_foto,function(k,v){
                          if(v.fileurl.length > 12){
                            cekNo++;
                            var kkNo = (cekNo == 1) ? 'active' : '';
                            $('.showImg').append(`
                              <div class="carousel-item `+kkNo+`">
                                <img src="<?php echo base_url(); ?>`+v.fileurl+`" class="img-fluid" style="width:100%;height:420px;" alt="">
                              </div>
                            `);
                          }
                      })
              }
            }
          },
          error: function() {
            $('.alertLah').html(`
              <div class="alert alert-danger">
              Refresh Halaman Kembali
              </div>
              `);
          }
        });
       }
      });

      $(document).on('click','.dragClick',function(){
          var id = $(this).data('id')
          $.each(stage.find('Image'),function(k,v){
            if(v.attrs.icon_id == id){
              if(v.attrs.fill){
                v.attrs.radius= '';
                v.attrs.fill= '';
                v.attrs.stroke= '';
                v.attrs.strokeWidth= 0;
              }else{
                v.attrs.radius= v.attrs.width;
                v.attrs.fill= 'red';
                v.attrs.stroke= 'red';
                v.attrs.strokeWidth= 4;
              }
            }
          });
          layer.draw();
      });

   </script>
   <style type="text/css">
    .ampase{
      margin:140px auto;
    }
  </style>
   <div class="modal fade " id="add-panel" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-md ampase" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Keterangan Data Jenis Aspek</h4>
        </div>
        <div class="modal-body">
          <form id="formModals" action="<?= base_url('backend/pelabuhan/store'); ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
              <input type="hidden" name="id">
              <input type="hidden" name="id_pelabuhan" value="<?= $pelabuhanss->id; ?>">
              <input type="hidden" name="id_jenis_aspek" value="<?= $record->id; ?>">
              <input type="hidden" name="icon_id">
              <input type="hidden" name="url">
              <input type="hidden" name="pointer_x">
              <input type="hidden" name="pointer_y">
              <input type="hidden" name="primary_key">
              <input type="hidden" name="kategori" value="Pelabuhan">
              <div class="col-lg-12">
                <ul>
                  <li>Nama : <span class="Nama"></span></li>
                  <li>Aspek : <span class="Aspek"></span></li>
                  <li>Nomor : <span class="Nomor"></span></li>
                  <li>Kondisi : <span class="Kondisi"></span></li>
                  <li>Posisi : <span class="Posisi"></span></li>
                  <li>Tahun Pengadaan : <span class="Pengadaan"></span></li>
                  <li>Deskripsi Total : <span class="total_icon"></span></li>
                </ul>
              </div>
            </div>
                <div class="card">
                  <div id="demo2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner showImg" style="width:100%;">


                    </div>
                    <a class="carousel-control-prev" href="#demo2" data-slide="prev" >
                      <span class="carousel-control-prev-icon" style="background-color: #1f91f3"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo2" data-slide="next">
                      <span class="carousel-control-next-icon" style="background-color: #1f91f3"></span>
                    </a>
                  </div>
                </div>
                <div class="col-md-12 floted-right pull-right" style="text-align: right;"><br>
                 <button type="button" class="btn btn-default" id="cancel-button" data-dismiss="modal">Tutup</button>
               </div>
              </form>
             </div> 
         </div>
         <div class="modal-footer">

         </div>
       </div>
     </div>

     <!-- EDIT MODAL -->


     <script type="text/javascript">
        // $(document).on('click','.saveBtn',function(){
        //   var data = $('#formModals').serializeArray();
        //   console.log('data',data)
        //   $.ajax({
        //     url: '<?= site_url('backend/pelabuhan/store'); ?>',
        //     type: 'post',
        //     data: data,
        //     dataType: 'json',
        //     success:function(response){
        //         if(response.status == true){
        //           $('.alertLah').html(`
        //             <div class="alert alert-success">
        //               `+ response.message +`
        //             </div>
        //           `);
        //         }else{
        //           $('.alertLah').html(`
        //             <div class="alert alert-danger">
        //               Gagal Menyimpan Data
        //             </div>
        //           `);
        //         }
        //         $("#add-panel").modal("hide");

        //     },
        //     error: function() {
        //       $('.alertLah').html(`
        //         <div class="alert alert-danger">
        //           Refresh Halaman Kembali
        //         </div>
        //       `);
        //         $("#add-panel").modal("hide");

        //     }
        //   });
        // });

        $(document).on('click','.deleteDatak',function(){
          var data = $('#formModals').serializeArray();
          console.log('data',data)
          $.ajax({
            url: '<?= site_url('backend/pelabuhan/delete'); ?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success:function(response){
              if(response.status == true){
                window.location.reload();
              }else{
                $('.alertLah').html(`
                  <div class="alert alert-danger">
                  Gagal Menghapus Data
                  </div>
                  `);
              }
              $("#add-panel").modal("hide");

            },
            error: function() {
              $('.alertLah').html(`
                <div class="alert alert-danger">
                Refresh Halaman Kembali
                </div>
                `);
              $("#add-panel").modal("hide");

            }
          });
        });

        $(document).on('click','#appendAdd',function(){
          console.log('asd');
          console.log($(this).data('id1'),$(this).data('cekreal'));
          var id = '.appendChildSub'+$(this).data('id1')+'-'+$(this).data('cekreal');
          console.log('id',id);
          $('.appendChildSub'+$(this).data('id1')+'-'+$(this).data('cekreal')).append(`
            <tr>
            <td colspan="" rowspan="" headers="">
            <input type="text" name="sub_text[]" id="subText" data-pelabuhanid="<?= $pelabuhanss->id; ?>" data-idaspek="<?= $record->id; ?>" data-icon="<?= $cekReal['id']; ?>" class="form-control" style="width: 150px;height:25px;border:1px solid black !important;font-size: 11px">
            </td>
            <td colspan="" rowspan="" headers="">
            <input type="text" name="sub_value[]" class="form-control" style="width: 100px;height:25px;border:1px solid black !important;font-size: 11px" id="subValue" data-pelabuhanid="<?= $pelabuhanss->id; ?>" data-idaspek="<?= $record->id; ?>" data-icon="<?= $cekReal['id']; ?>">
            </td>
            <td colspan="" rowspan="" headers="">
            <a href="javascript:void(0)" class="btn btn-sm btn-danger appendDelete" id="appendDelete" style="position:relative;top:-3px;height:25px;width: 15px">
            <i class="zmdi zmdi-close" style="position: relative;right: 5px;top: -2px;"></i>
            </a>
            </td>

            </tr>
            `);
        });

        $(document).on('click','#appendDelete',function(){
          $(this).closest('tr').remove();
        });

        $(document).on('keypress keydown keyup','#subText',function(){
          var data = {title:$(this).val(),id_pelabuhan:$(this).data('pelabuhanid'),idaspek:$(this).data('idaspek'),id_icon:$(this).data('icon')};

          $.ajax({
            url: '<?= site_url('backend/pelabuhan/subtitle'); ?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success:function(response){
              console.log('success')
            },
            error: function() {
              console.log('error')
            }
          });
          
        });

        $(document).on('keypress keydown keyup','#subValue',function(){
          var data = {value:$(this).val(),id_pelabuhan:$(this).data('pelabuhanid'),idaspek:$(this).data('idaspek'),id_icon:$(this).data('icon')};

          $.ajax({
            url: '<?= site_url('backend/pelabuhan/subvalue'); ?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success:function(response){
              console.log('success')
            },
            error: function() {
              console.log('error')
            }
          });
        });
      </script>
      <?php include 'footer.php'; ?>