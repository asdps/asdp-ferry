
<script src="<?=base_url();?>assets/frontend/js/jquery.js"></script> 
<script src="<?=base_url();?>assets/frontend/js/konva.min.js"></script>
<script src="<?=base_url();?>assets/frontend/js/html2canvas.min.js"></script>

<style type="text/css">
	.color-red{
		background-color: red;
		color: white;

	}
  #ceks {
      width: 100%;
      height: 100%;
    }
</style>


<script type="text/javascript">
	$(document).on('change','select[name="deck_id"]',function(){
		var deckId = $(this).val();
		$('.showFind').attr('href','<?php echo site_url(); ?>backend/armada/showDetail/<?= $record->id; ?>/'+deckId+'/<?= $armadass->id; ?>?page=<?= $this->input->get('page') ?>');
	});


    
</script>
<?php include 'header.php'; ?>

<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="card">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<div class="btn-group">
							<a href="<?php echo site_url(); ?>backend/armada/showDetail/<?= $record->id; ?>/<?= $armadaElments->id; ?>/<?= $armadass->id; ?>?page=<?= $this->input->get('page') ?>"  class="btn btn-primary btn-sm" style="color: #fff">Kembali</a>
              <a href="javascript:void(0)"  class="btn btn-danger btn-sm stopDrag" style="color: #fff">Stop Drag & Drop</a>
						</div>
					</div>
					<div class="col-md-12 alertLah">
                      
                    </div>
				</div>


				<div class="wrapper content">
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-4">      
								<div class="form-group">
									<div class="input-group" >     

										<select name="deck_id" class="form-control show-tick" required>
											<option value="-">Select One </option>

											<?php
											if(count($this->m_model->selectwhere('armada_id',$armadass->id,'armada_elements')) > 0){
												foreach ($this->m_model->selectwhere('armada_id',$armadass->id,'armada_elements') as $k => $value) {
													$cek = '';
													if($armadaElments->id == $value->id){
														$cek = 'selected';
													}
													?>
													<option value="<?=$value->id;?>" <?= $cek; ?>><?=$value->name;?></option>
													<?php
												}
											}else{
												echo '<option value="">No Data Found</option>';
											}
											?>
										</select>
										<div class="btn-group">
											<a href="" class="btn btn-primary btn-sm showFind" style="color: #fff">Find</a>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-8 " style="text-align: right">
								<h3><?= $armadass->name; ?></h3>
								<h6><?= $armadaElments->name; ?></h6>
							</div>
							<div class="row">
								<div class="col-lg-12 mb-3 pr-0">
									<div class="card">
										<div id="demo2" class="carousel slide" data-ride="carousel">
											<div class="carousel-inner" style="width:100%;height: auto;">
												<div class="carousel-item active">
													<?php 
													$imgs=check_img($armadaElments->path_file);
                          $cekIMG = isset($armadaElments->path_file) ? $armadaElments->path_file : 'images/images.png';
                          if(file_exists(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$cekIMG)){
                            $cekIMG = $armadaElments->path_file;
                          }else{
                            $cekIMG = 'images/images.png';
                          }
                          list($width, $height, $type, $attr) = getimagesize(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$cekIMG);
                          $ratio = $width/$height;
                          $widthR = 950;
                          $heightR = 800/$ratio;
                          ?>
                          <input type="hidden" name="imgWidth" value="<?= $widthR; ?>">
                          <input type="hidden" name="imgHeight" value="<?= $heightR; ?>">
                          <div style="background-image: url('<?=$imgs['path'];?>'); background-size: <?= $widthR; ?>px <?= $heightR; ?>px;background-repeat: no-repeat;" id="containersss" data-id="containersss"></div>
													
												</div>
											</div>

											
										</div>
									</div>
								</div>   
								<div class="col-lg-12 mb-3 p-0" style="background: #fffafa">
									<div class="bg-warning text-center py-1">
										<h3 class="text-white font-weight-bold"><span style="font-weight: 1;" ><?= $record->nama_aspek; ?></span></h3>
									</div>
									<div class="row" style="padding-top: 3px;position:relative;left: -20px;">
										<?php
										if(count($records) > 0){
                      $no = 0;
											foreach ($records as $k => $value) {
                        if($value->name != ''){
                          $no++;
												?>         
												<div class="col-md-6">                          
                          <ul style="position: relative;">
                            <li style="list-style: none;font-size: 13px">
                              <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block" style="padding-top: 3px;height: 50px;">
                                <div class="row">
                                  <div class="col-md-8">
                                    <span class="rounded-circle text-white bg-warning mr-1" style="padding: 3px 8px;"><?= $no; ?></span>
                                    <span class="text-warning"><?= $value->name; ?></span>
                                  </div>
                                  <div class="col-md-2" style="left: 10px">
                                    <span class="text-warning">Titik</span>
                                  </div>
                                  <div class="col-md-2" style="left: 10px">
                                    <span class="text-warning">Unit Total</span>
                                  </div>
                                </div>
                              </a>
															<ul class="ml-menu" style="display: none;">
																<?php
																if(count($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon'))){
																	foreach ($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon','created_at') as $keySubIco => $valueSubIco) {

																		$cekReal = $this->m_model->getOne($valueSubIco->trans_icon_id,'icon');
                                    $selectAs5 = $this->m_model->selectas5(
                                      "id_armada",$armadass->id, 
                                      "id_armada_elments",$armadaElments->id,
                                      "id_jenis_aspek",$record->id,
                                      "id_sub_jenis_aspek",$value->id,
                                      "icon_id",$cekReal['id'],
                                      "trans_armada_hasil"
                                    );

																		$imgs=check_img($cekReal['path_file']);
																		?>
                                    <?php 
                                      $coun = 0;
                                      $color = 'background-color: #f00;color: white;border-radius:20px;border:3px solid #f00 !important';
                                      if(count($this->m_model->selectas4('id_armada',$armadass->id,'id_jenis_aspek',$record->id,'icon_id',$cekReal['id'],'id_armada_elments',$armadaElments->id,'trans_armada_hasil')) > 0){
                                        $coun = count($this->m_model->selectas4('id_armada',$armadass->id,'id_jenis_aspek',$record->id,'icon_id',$cekReal['id'],'id_armada_elments',$armadaElments->id,'trans_armada_hasil'));

                                        $color = '-';
                                      }
                                    ?>
																		<li style="padding-bottom: 1px;" id="drag-items">
																			<div class="row">
                                          <div class="col-md-8">
                                            <img src="<?=$imgs['path'];?>" class="img-responsive drag drag-items" style="cursor: pointer; max-width: 50px; max-height:50px;width: 30px;padding-bottom: 1px;<?= $color; ?>" data-fancybox="images<?= $keySubIco + 1; ?>" href="<?=$imgs['path'];?>" data-key="<?= $keySubIco + 1; ?>" data-sub="<?= $value->id; ?>" data-id="<?= $cekReal['id']; ?>" data-aspek="<?= $value->name; ?>" data-name="<?= $cekReal['name']; ?>" data-elment="<?= $armadaElments->id; ?>">&nbsp;
                                            <span style="font-size: 12px;">
                                              <?= $cekReal['name']; ?>     
                                            </span>
                                          </div>
                                          <div class="col-md-2">
                                            <span class="rounded-circle text-white bg-warning mr-1" style="padding: 1px 8px;"><?= $coun; ?></span>    
                                          </div>
                                          <div class="col-md-2">

                                           <span class="rounded-circle text-white bg-warning mr-1" style="padding: 1px 8px;">
                                              <?php
                                                $nominal = 0;
                                                if(count($selectAs5) > 0){
                                                  foreach ($selectAs5 as $k => $valueS) {
                                                    $nominal += (int)$valueS->total_icon;
                                                  }
                                                }
                                                echo $nominal;
                                              ?>
                                            </span> 
                                          </div>
                                        </div>
																		</li>
                                    <hr style="width: 350px">
																		<?php
																	}
																}
																?>
															</ul>
														</li>

													</ul>
                        </div>
												<?php          
											  }
                      }
										}
										?>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
      // function update(activeAnchor) {
      //     var group = activeAnchor.getParent();

      //     var topLeft = group.get('.topLeft')[0];
      //     var topRight = group.get('.topRight')[0];
      //     var bottomRight = group.get('.bottomRight')[0];
      //     var bottomLeft = group.get('.bottomLeft')[0];
      //     var image = group.get('Image')[0];

      //     var anchorX = activeAnchor.getX();
      //     var anchorY = activeAnchor.getY();

      //     // update anchor positions
      //     switch (activeAnchor.getName()) {
      //       case 'topLeft':
      //         topRight.y(anchorY);
      //         bottomLeft.x(anchorX);
      //         break;
      //       case 'topRight':
      //         topLeft.y(anchorY);
      //         bottomRight.x(anchorX);
      //         break;
      //       case 'bottomRight':
      //         bottomLeft.y(anchorY);
      //         topRight.x(anchorX);
      //         break;
      //       case 'bottomLeft':
      //         bottomRight.y(anchorY);
      //         topLeft.x(anchorX);
      //         break;
      //     }

      //     image.position(topLeft.position());

      //     var width = topRight.getX() - topLeft.getX();
      //     var height = bottomLeft.getY() - topLeft.getY();
      //     if (width && height) {
      //       if(width >= 40){
      //         width = 40;
      //       }else if((width <= 40) && (width >= 30)){
      //         width = 30;
      //       }else if((width <= 30) && (width >= 20)){
      //         width = 20;
      //       }else if((width <= 20) && (width >= 10)){
      //         width = 10;
      //       }else if(width <= 10){
      //         width = 10;
      //       }
      //       image.width(width);
      //       image.height(width);
      //     }
      // }
      // function addAnchor(group, x, y, name) {
      //   var stage = group.getStage();
      //   var layer = group.getLayer();

      //   if(name==='bottomRight'){
      //     var anchor = new Konva.Circle({
      //       x: x,
      //       y: y,
      //       stroke: '#f0ad4e',
      //       fill: '#f0ad4e',
      //       strokeWidth: 1,
      //       radius: 5,
      //       name: name,
      //       draggable: true,
      //       dragOnTop: false
      //     });
      //   }else{
      //     var anchor = new Konva.Circle({
      //       x: x,
      //       y: y,
      //       // stroke: '#f0ad4e',
      //       // fill: '#f0ad4e',
      //       // strokeWidth: 1,
      //       // radius: 5,
      //       name: name,
      //       // draggable: true,
      //       // dragOnTop: false
      //     });
      //   }

      //   anchor.on('dragmove', function() {
      //     update(this);
      //     layer.draw();
      //   });
      //   anchor.on('mousedown touchstart', function() {
      //     group.draggable(false);
      //     this.moveToTop();
      //   });
      //   anchor.on('dragend', function() {
      //     group.draggable(true);
      //     layer.draw();
      //   });
      //   // add hover styling
      //   anchor.on('mouseover', function() {
      //     var layer = this.getLayer();
      //     document.body.style.cursor = 'pointer';
      //     this.strokeWidth(4);
      //     layer.draw();
      //   });
      //   anchor.on('mouseout', function() {
      //     var layer = this.getLayer();
      //     document.body.style.cursor = 'default';
      //     this.strokeWidth(2);
      //     layer.draw();
      //   });

      //   group.add(anchor);
      // }
  	  var width = 935;
      var height = 450;
      var stage = new Konva.Stage({
        container: 'containersss',
        width: <?= $widthR; ?>,
        height: <?= $heightR; ?>
      });
      function fitStageIntoParentContainer() {
        var container = document.querySelector('#containersss');
        // now we need to fit stage into parent
        var containerWidth = container.offsetWidth;
        // to do this we need to scale the stage
        var scale = containerWidth / container.offsetWidth;

        stage.width(container.offsetWidth * scale);
        stage.height(container.offsetHeight * scale);
        stage.scale({ x: scale, y: scale });
        stage.draw();
      }

      fitStageIntoParentContainer();
      // adapt the stage on any window resize
      window.addEventListener('resize', fitStageIntoParentContainer);
      var layer = new Konva.Layer();
      stage.add(layer);

      
      // ADD FOTO DARI KUMPULAN ARRAY
      var widths = 20;
    	$.ajax({
            url: '<?= site_url('backend/armada/getData'); ?>',
            type: 'post',
            data: {id_jenis_aspek: '<?= $record->id; ?>',id_armada:'<?= $armadass->id; ?>',id_armada_elments:'<?= $armadaElments->id; ?>'},
            dataType: 'json',
            success:function(response){
                if(response.length > 0){
                  $.each(response,function(k,v){
                    
                    // 3. Create an Image node and add it to group
                    Konva.Image.fromURL(v.url, function(yoda) {
                      layer.add(yoda);

                      var group = new Konva.Group({
                        x: parseFloat(v.pointer_x),
                        y: parseFloat(v.pointer_y),
                      });

                      layer.add(group);
                      group.add(yoda);
                      if(v.width == null || v.width == ''){
                        widths = 20;
                        heights = v.width;
                      }else{
                        widths = v.width;
                        heights = v.width
                      }

                      yoda.setAttrs({
                        width: widths,
                        height: heights,
                        id_armada: v.id_armada,
                        id_armada_elments: v.id_armada_elments,
                        id_jenis_aspek: v.id_jenis_aspek,
                        id_sub_jenis_aspek:v.id_sub_jenis_aspek,
                        primary_key: v.primary_key,
                        pointer_x: v.pointer_x,
                        pointer_y: v.pointer_y,
                        total_icon: v.total_icon,
                        cek_target:'true'
                      });
                      // 4. Add it to group.
                      yoda.draggable(true);
                      layer.draw(); // It
                    });

                  });  
                }
            },
            error: function() {
              // $('.alertLah').html(`
              //   <div class="alert alert-danger">
              //     Terjadi Kesalahan!
              //   </div>
              // `);
            }
          });
      // END ADD FOTO DARI KUMPULAN ARRAY
      
      // what is url of dragging element?
      var itemURL = '';
      var dataIds = '';
      var dataAspek = '';
      var dataName = '';
      arrayHasil = [];
      var imagesArr = [];

        $(document).on('dragstart touchstart','.drag-items', function(e) {
          
          itemURL = e.target.src;
          dataIds = e.target.dataset.id;
          $('input[name="icon_id"]').val(dataIds);
          $('input[name="url"]').val(e.target.src);
          $('input[name="aspek"]').val(e.target.dataset.aspek);
          $('input[name="id_sub_jenis_aspek"]').val(e.target.dataset.sub);
          $('input[name="nama"]').val(e.target.dataset.name);
        });

      stage.on('click tap', function(e) {
        console.log('e.target',e.target)
        imagesArr = e.target;
        if(e.target.attrs.cek_target === 'true'){
            
             $.ajax({
              url: '<?= site_url('backend/armada/getDataOne/'); ?>',
              type: 'post',
              data: {id_jenis_aspek: e.target.attrs.id_jenis_aspek, id_armada:e.target.attrs.id_armada, id_armada_elments:e.target.attrs.id_armada_elments, primary_key:e.target.attrs.primary_key,pointer_x:e.target.attrs.pointer_x,pointer_y:e.target.attrs.pointer_y},
              dataType: 'json',
              success:function(response){
                
                if(response){

                  if(e.target._lastPos == null){
                    var points = {x:response.record.pointer_x,y:response.record.pointer_y};
                  }else{
                    var points = {x:e.target._lastPos.x,y:e.target._lastPos.y};
                  }

                  imagesArr = e.target;
                  $("#add-panel").modal("show");
                    $('.modal-backdrop').removeClass();
                   
                    $('input[name="id"]').val(response.record.id);
                    $('input[name="id_armada"]').val(response.record.id_armada);
                    $('input[name="id_armada_elments"]').val(response.record.id_armada_elments);
                    $('input[name="id_jenis_aspek"]').val(response.record.id_jenis_aspek);
                    $('input[name="id_sub_jenis_aspek"]').val(response.record.id_sub_jenis_aspek);
                    $('input[name="icon_id"]').val(response.record.icon_id);
                    $('input[name="url"]').val(response.record.url);
                    $('input[name="pointer_x"]').val(points.x);
                    $('input[name="pointer_y"]').val(points.y);
                    $('input[name="primary_key"]').val(response.record.primary_key);
                    $('input[name="kategori"]').val(response.record.kategori);
                    $('input[name="nama"]').val(response.record.nama);
                    $('input[name="aspek"]').val(response.record.aspek);
                    $('input[name="nomor"]').val(response.record.nomor);
                    $('input[name="kondisi"]').val(response.record.kondisi);
                    $('input[name="posisi"]').val(response.record.posisi);
                    $('input[name="tahun"]').val(response.record.tahun);
                    $('input[name="total_icon"]').val(response.record.total_icon);
                    $('input[name="width"]').val(e.target.attrs.width);
                    $('input[name="height"]').val(e.target.attrs.height);

                    var imgApp = `<center><img src="`+response.record.url+`" alt="" style="width:20px;" class="imgResze"></center>`;
                    $('.appendImg').html(imgApp);

                    $('.deletesData').show();
                    if(response.record_file){
                      $('.ReadshowImg').show();
                      $('.showImg').html('');
                      var cekNo = 0;
                      $.each(response.record_file,function(k,v){
                          if(v.fileurl.length > 12){
                            cekNo++;
                            var kkNo = (cekNo == 1) ? 'active' : '';
                            $('.showImg').append(`
                              <div class="carousel-item `+kkNo+`">
                                <img src="<?php echo base_url(); ?>`+v.fileurl+`" class="img-fluid" style="width:100%;height:420px;" alt="">
                              </div>
                            `);
                          }
                      })
                    }
                }
              },
              error: function() {
                $('.alertLah').html(`
                  <div class="alert alert-danger">
                    Terjadi Kesalahan!
                  </div>
                `);
              }
            });
        }else{
          console.log('e',e)
          var itemURL = $(e.target.attrs.image).attr('src');
          if(itemURL){

          
          // e.preventDefault();
          stage.setPointersPositions(e);
          Konva.Image.fromURL(itemURL, function(image) {
            if(e.target._lastPos == null){
              var points = {x:e.target.attrs.pointer_x,y:e.target.attrs.pointer_y};
            }else{
              var points = {x:e.target._lastPos.x,y:e.target._lastPos.y};
            }
            // layer.add(image);
            // image.position(stage.getPointerPosition());
            // image.width(20);
            // image.height(20);
            image.draggable(true);
            arrayHasil.push(points);
            layer.draw();

            $("#add-panel").modal("show");
            $('.modal-backdrop').removeClass();
            $('input[name="width"]').val(e.target.attrs.width);
            $('input[name="height"]').val(e.target.attrs.height);
            $('input[name="pointer_x"]').val(points.x);
            $('input[name="pointer_y"]').val(points.y);

            $('input[name="primary_key"]').val(arrayHasil.length);
            

            $.ajax({
              url: '<?= site_url('backend/armada/getDataNumWithUrl'); ?>',
              type: 'post',
              data: {
                id_jenis_aspek: '<?= $record->id; ?>',
                id_armada:'<?= $armadass->id; ?>',
                id_armada_elments:'<?= $armadaElments->id; ?>',
                icon_id:e.target.attrs.icon_id,
                id_sub_jenis_aspek:e.target.attrs.id_sub_jenis_aspek
              },
              dataType: 'json',
              success:function(response){
                $('input[name="nomor"]').val(response.count);
                $('input[name="icon_id"]').val(e.target.attrs.icon_id);
                $('input[name="url"]').val(itemURL);
                $('input[name="nama"]').val(e.target.attrs.nama);
                $('input[name="aspek"]').val(e.target.attrs.aspek);
                $('input[name="id_sub_jenis_aspek"]').val(e.target.attrs.id_sub_jenis_aspek);
              },
              error: function() {
                
              }
            });
            
            var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:30px;" class="imgResze"></center>`;
            $('.appendImg').html(imgApp);
          });
          }

        }
      });

      var con = stage.container();
      con.addEventListener('dragover', function(e) {
        
        e.preventDefault(); 
      });
      
      con.addEventListener('drop', function(e) {
        console.log('drop');

        e.preventDefault();
        stage.setPointersPositions(e);
        Konva.Image.fromURL(itemURL, function(image) {
          layer.add(image);
          
          image.position(stage.getPointerPosition());
          image.width(20);
          image.height(20);
          image.draggable(true);
          arrayHasil.push(stage.getPointerPosition());
          imagesArr = image;

          imagesArr.setAttrs({
            icon_id:$('input[name="icon_id"]').val(),
            url:$('input[name="url"]').val(),
            aspek:$('input[name="aspek"]').val(),
            id_sub_jenis_aspek:$('input[name="id_sub_jenis_aspek"]').val(),
            nama:$('input[name="nama"]').val(),
            pointer_x:stage.getPointerPosition().x,
            pointer_y:stage.getPointerPosition().y,
          });
          layer.draw();
          $('input[name="pointer_x"]').val(stage.getPointerPosition().x);
          $('input[name="pointer_y"]').val(stage.getPointerPosition().y);
          $('input[name="primary_key"]').val(arrayHasil.length);

          var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:20px;" class="imgResze"></center>`;
          $('.appendImg').html(imgApp);
        });
      });

       layer.addEventListener('dragmove', function(e) {
        
       });

      con.addEventListener('touchend',function(e) {
        console.log('touchend',e,'itemUrl',itemURL);
        if(itemURL){
          e.preventDefault();
          stage.setPointersPositions(e);
          Konva.Image.fromURL(itemURL, function(image) {
            
            image.position(stage.getPointerPosition());
            image.width(20);
            image.height(20);
            image.draggable(true);
            arrayHasil.push(stage.getPointerPosition());
            imagesArr = image;

            imagesArr.setAttrs({
              icon_id:$('input[name="icon_id"]').val(),
              url:$('input[name="url"]').val(),
              aspek:$('input[name="aspek"]').val(),
              id_sub_jenis_aspek:$('input[name="id_sub_jenis_aspek"]').val(),
              nama:$('input[name="nama"]').val(),
              pointer_x:stage.getPointerPosition().x,
              pointer_y:stage.getPointerPosition().y,
            });
            layer.add(image);

            layer.draw();
            $('input[name="pointer_x"]').val(stage.getPointerPosition().x);
            $('input[name="pointer_y"]').val(stage.getPointerPosition().y);
            $('input[name="primary_key"]').val(arrayHasil.length);

            var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:20px;" class="imgResze"></center>`;
            $('.appendImg').html(imgApp);
          });
        }
      });
      
       $(document).on('click','.stopDrag',function(){
          itemURL = null;
       });

   
</script>
<div class="modal fade " id="add-panel" tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-md" role="document" style="margin:140px auto;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align: left">Buat Keterangan Data</h4>
      </div>
      <div class="modal-body">
          <form id="formModals" class="form-horizontal" action="<?= site_url('backend/armada/store'); ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
              <input type="hidden" name="id">
              <input type="hidden" name="id_armada" value="<?= $armadass->id; ?>">
              <input type="hidden" name="id_armada_elments" value="<?= $armadaElments->id; ?>">
              <input type="hidden" name="id_jenis_aspek" value="<?= $record->id; ?>">
              <input type="hidden" name="id_sub_jenis_aspek">
              <input type="hidden" name="icon_id">
              <input type="hidden" name="url">
              <input type="hidden" name="pointer_x">
              <input type="hidden" name="pointer_y">
              <input type="hidden" name="primary_key">
              <input type="hidden" name="height">
              <input type="hidden" name="kategori" value="Armada">
              <textarea name="url_canvas" id="url_canvas" style="display: none;"></textarea>
              <div class="col-md-8">
                <div class="form-group form-line">
                  <label>Resize Width Image</label>
                  <input name="width" id="resize" placeholder="Width / Pixels" type="number" class="form-control" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')" value="30" />
                  <label class="label-error" style="color: red;"></label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <div class="btn btn-sm btn-success resizeNow" style="position: relative;top: 12px;">
                    Resize
                  </div>
                </div>
              </div>
              <div class="col-md-12 appendImg">

              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label>Nama</label>*
                  <input name="nama" placeholder="Nama" type="text" class="form-control" />
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label>Aspek</label>*
                  <input name="aspek" placeholder="Aspek" type="text" class="form-control" />
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label>Nomor</label>*
                  <input name="nomor" placeholder="Nomor" type="text" class="form-control" />
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label>Kondisi</label>*
                  <input name="kondisi" placeholder="Kondisi" type="text" class="form-control" maxlength="20"/>
                </div>
              </div>
               <div class="col-lg-4">
                <div class="form-group">
                  <label>Posisi</label>*
                  <input name="posisi" placeholder="Posisi" type="text" class="form-control" maxlength="20"/>
                </div>
              </div>
               <div class="col-lg-4">
                <div class="form-group">
                  <label>Tahun Pengadaan</label>
                  <input name="tahun" placeholder="Tahun Pengadaan" type="text" class="form-control" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')"/>
                </div>
              </div>
               <div class="col-lg-12">
                <div class="form-group">
                  <label>Deskripsi Total</label>
                  <input name="total_icon" placeholder="Deskripsi Total" type="number" class="form-control" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')" />
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-line">
                    <div class="clearfix"></div>
                    <label>*click below to browse file</label>
                    <p>*Ukuran Foto Berkisar (854 x 480) Pixel</p>
                    <input name="icon[]" type="file" class="form-control" style="cursor: pointer;" accept="image/*" multiple="">
                </div>
              </div><br>
              <div class="card ReadshowImg" style="display: none;">
                    <div id="demo2" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <!-- <ul class="carousel-indicators">
                          <li data-target="#demo2" data-slide-to="0" class=""></li>
                          <li data-target="#demo2" data-slide-to="1" class=""></li>
                          <li data-target="#demo2" data-slide-to="2" class=""></li>
                        </ul> -->
        
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner showImg" style="width:100%;">
                          
                          
                        </div>
        
                        <!-- Controls -->
                        <!-- Left and right controls -->
                       <!--  <a class="carousel-control-prev" href="#demo2" data-slide="prev" >
                          <span class="carousel-control-prev-icon" style="background-color: #1f91f3"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo2" data-slide="next">
                          <span class="carousel-control-next-icon" style="background-color: #1f91f3"></span>
                        </a> -->
                      </div>
                    </div>
              <div class="col-md-12 floted-right pull-right" style="text-align: right;"><br>
              	 <button type="button" class="btn btn-default" id="cancel-button">Cancel</button>
        			<button type="button" class="btn btn-danger deleteDatak deletesData" id="cancel-button" data-dismiss="modal" style="display: none">Delete</button>
        			<button name="store" type="button" class="btn btn-primary saveBtn" id="confirm-button">Save</button>
              <button type="submit" class="btn btn-primary" id="bnke_btn" style="display: none;">Save</button>
              </div>
            </div>
          </form>
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
  <script type="text/javascript">
      $(document).on('click','.resizeNow',function(e){
        // console.log('layer',stage.findOne('Image'))
          var width = $('input[name="width"]').val();
          var id = $('input[name="id"]').val();
          if(parseInt(width) < 10){
            $('.label-error').text('Tidak Boleh Kurang Dari 10');
          }else if(parseInt(width) > 50){
            $('.label-error').text('Tidak Boleh Lebih Dari 50');
          }else{
            if(width == ''){
              width = 20;
            }
            $('.label-error').text('');
            $('.imgResze').css("width",width);
            $('.imgResze').css("height",width);
            if(id){
              
              imagesArr.setAttrs({
                width : width,
                height : width,
                pointer_x : $('input[name="pointer_x"]').val(),
                pointer_y : $('input[name="pointer_y"]').val(),
              });
            }else{
              imagesArr.setAttrs({
                width : width,
                height : width,
                pointer_x : $('input[name="pointer_x"]').val(),
                pointer_y : $('input[name="pointer_y"]').val(),
              });
            }

            layer.draw();
          }
      });

      $(document).on('click','#cancel-button',function(){
        window.location.reload();
      });

        $(document).on('keypress', function (e) {
          // e.preventDefault();
             if(e.which === 13){
                $('.saveBtn').trigger('click');
             }
        });
        
        $(document).on('click','.saveBtn',function(){
            
            var element = $('#containersss');
            var getCanvas= '';
            html2canvas(element, {
             onrendered: function (canvas) {
                    getCanvas = canvas;
                    var imgageData = getCanvas.toDataURL("image/png");
                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                    
                    $('#url_canvas').val(imgageData);
                    // window.open(imgageData);
                    // $("#btn-Convert-Html2Image").attr("download", "your_pic_name.png").attr("href", newData);
                 }
             });
            // 
            $('.saveBtn').attr('disabled','disabled');
            $('.saveBtn').text('Wait for a while');
            var data = $('#formModals').serializeArray();
            
            time = 5;
            interval = setInterval(function(){
              time--;
              if(time == 0){
                clearInterval(interval);
                $( "#bnke_btn" ).trigger('click');
                
              }
            },1000);
        });

        $(document).on('click','.deleteDatak',function(){
          var element = $('#containersss');
          var getCanvas= '';
          html2canvas(element, {
           onrendered: function (canvas) {
                  getCanvas = canvas;
                  var imgageData = getCanvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                   var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                  
                  // $('#url_canvas').val(imgageData);
                  // window.open(imgageData);
                  // $("#btn-Convert-Html2Image").attr("download", "your_pic_name.png").attr("href", newData);
               }
           });

          var data = $('#formModals').serializeArray();

          
          $.ajax({
            url: '<?= site_url('backend/armada/delete'); ?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.status == true){
                  window.location.reload();
                }else{
                  $('.alertLah').html(`
                    <div class="alert alert-danger">
                      Gagal Menghapus Data
                    </div>
                  `);
                }
                $("#add-panel").modal("hide");

            },
            error: function() {
              // $('.alertLah').html(`
              //   <div class="alert alert-danger">
              //     Terjadi Kesalahan!
              //   </div>
              // `);
                $("#add-panel").modal("hide");

            }
          });
        });
      </script>
<?php include 'footer.php'; ?>