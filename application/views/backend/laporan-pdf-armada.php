<html>
<head>
  <meta http-equiv="Content-Type" content="charset=utf-8"/>
  <link rel="icon" href="<?= site_url('images/ico.jpg') ?>" sizes="192x192">
  <title>ASDP</title>
  <style>
    body {
      font-family: Arial, Helvetica Neue, Helvetica, sans-serif; 
    }

    @page {
      margin: 20px 0px;
    }


    body {
      margin-top: 5cm;

      font-style: normal;

    }

    .ui.table.bordered {
      border: solid black 2px;
      border-collapse: collapse;
      width: 100%;
    }

    .ui.table.bordered td {
      border: solid black 1px;
      border-collapse: collapse;

    }

    .ui.table.bordered td img {
      padding: 10px;
    }

    .ui.table.bordered td.center.aligned {
      text-align : center;
    }


    header {
      position: fixed;
      top: -55px;
      height: 50px;
      text-align: center;
      line-height: 35px;
    }

    main {
      position: sticky;
      font-size : 12px;
      /*margin: 20px 20px 20px 20px;*/
      margin-bottom : 10px;
      margin-left: 1px;
      margin-right: 1px;
      border: solid white 1.5px !important; 
      /*border-collapse: collapse;*/
      width: 100%;
    }

    footer {
      position: fixed;
      bottom: -60px;
      left: 0px;
      right: 0px;
      height: 200px;

      background-color: red;
      text-align: center;
      line-height: 35px;
      clear: both;
    }
    .footer .page-number:after {
      content: counter(page);
    }

    .col-6 {
      -webkit-box-flex: 0;
      -ms-flex: 20% 20% 50%;
      flex: 20% 20% 50%;
      max-width: 50%;
    }

    .row {
      margin: auto;
      margin-right: 0px;
      margin-left: 0px;
      margin-top: auto;
    }
    .col-sm-4 {
      width: 63.33333333%;
    }
    .col-sm-3 {
      width: 27%;
    }
    .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
      float: left;
    }
    .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
      position: relative;
      min-height: 1px;
      padding-right: 15px;
      padding-left: 15px;
    }

    /* Create three equal columns that floats next to each other */
    .column {
      float: left;

      height: 300px; /* Should be removed. Only for demonstration */
    }

    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
  </style>



</head>
<body style="margin-bottom: 25px">
  
  <header>

    <table class="ui table" style="width: 100%;position: relative;">
      <tr>
        <td class="" style="width: 400px">
          <center style="position:relative;top:45px;"><img src="<?= dirname($_SERVER['SCRIPT_FILENAME']).'/images/logo-r.png' ?>" style="width: 350px;"></center>
          <div style="font-size: 8px;position: relative;left:80px;top: 30px;color: #6f7176;"><b>PT. ASDP Indonesia Ferry (Persero)</b><span style="margin-left: 50px">https://www.indonesiaferry.co.id</span></div>
          <div style="font-size: 8px;position: relative;left:80px;top: 10px;color: #6f7176;">Jl. Jend. Ahmad Yani Kav. 52 A <span style="margin-left: 70px">pelanggan@indonesiaferry.co.id</span></div>
          <div style="font-size: 8px;position: relative;left:80px;top: -10px;color: #6f7176;">Cempaka Putih Timur,Kota Jakarta Pusat <span style="margin-left: 40px">(021) 191</span></div>

        </td>
        <td class="center aligned">
          <div class="row">
            <div class="column" style="background-color:#ffd0a1;width: 20px;height: 100px;-webkit-transform:skew(60deg);-moz-transform:skew(60deg);-o-transform:skew(-20deg);transform:skew(-20deg);margin-right: 5px;position: relative;left:30px;top: 30px;"></div>

            <div class="column" style="background-color:#4a599f;width: 20px;height: 60px; -webkit-transform:skew(60deg);-moz-transform:skew(60deg);-o-transform:skew(-20deg);transform:skew(-20deg);margin-right: 5px;position: relative;left: 65px;top: 52px;">
            </div>
            <div class="column" style="background-color:#dbdeec;width: 430px;height: 60px; -webkit-transform:skew(-21deg);-moz-transform:skew(-21deg);-o-transform:skew(-20deg);transform:skew(-20deg);position:relative;top: 52px;">
              <h3 style="position: relative;top: -10px;left:20px;color: #6f7176;">FORM LAPORAN <?= $data['judul']; ?></h3>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2" rowspan="" headers="" style="background-color: #ffd0a1;"></td>
      </tr>
      <tr>
        <td class="" colspan="2" style="background-color: #dbdeec;color: #6f7176;">
          <center><?= isset($data['record']->name) ? $data['record']->name : '-'; ?></center>
        </td>
      </tr>
      <tr>
        <td colspan="2" rowspan="" headers="" style="background-color: #ffd0a1;"></td>
      </tr>
    </table>
  </header>
  <?php
  if(isset($data['record']->foto)){
    if($data['record']->foto != ''){
      if(strlen($data['record']->foto) > 14){
        $img=checkImgByBase($data['record']->foto);
      }else{
        $img['path']=dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
      } 
    }else{
      $img['path']=dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
    }
  }else{
    $img['path']=dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
  }

  if(isset($data['record']->simetris)){
    if($data['record']->simetris != ''){
      if(strlen($data['record']->simetris) > 14){
        $imgSimet = checkImgByBase($data['record']->simetris);
      }else{
        $img['path']=dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
      }
    }else{
      $img['path']=dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
    }
  }else{
    $imgSimet['path'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
  }
  ?>
  <center><img src="<?= $img['path']; ?>" style="max-width: 600px;height: auto;width: auto;"></center>
  <table class="ui table" style="font-size: 14px;width: 100%;page-break-before: always;">
    <tbody>
      <tr>
        <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
          <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
            <span style="padding-left: 10px">&nbsp;&nbsp;Deskripsi</span>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="" rowspan="" headers="" style="background-color: #ebecec;font-size: 11px">
          <p style="padding-left: 20px">
            <?php
              if(isset($data['record']->deskripsi)){
                if(!is_null($data['record']->deskripsi) || strlen($data['record']->deskripsi > 5)){
                  echo str_replace('\"', '"', $data['record']->deskripsi);
                }
              }
            ?>
          </p>
        </td>
      </tr>
    </tbody>
  </table>
  <table class="ui table" style="font-size: 14px;width: 100%;page-break-before: always;">
    <tbody>
      <tr>
        <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
          <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
            <span style="padding-left: 10px">&nbsp;&nbsp;Isometri</span>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <center style="page-break-after: always;"><img src="<?= $imgSimet['path']; ?>" style="max-width: 600px;height: auto;width: auto;"></center>
  <table class="ui table" style="font-size: 14px;width: 100%;page-break-before: avoid;">
    <tbody>
      <tr>
        <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
          <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 500px;">
            <span style="padding-left: 10px">&nbsp;&nbsp;Safety Plan</span>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <?php
  $cekSafet = $this->m_model->selectcustom('select * from armada_file where trans_id='.$data['record']->id.' order by id desc limit 4');
// $arrSafet = array_chunk($cekSafet, 4);
  $noSafe = -1;
  if(count($cekSafet) > 0){
    foreach ($cekSafet as $k1 => $value1) {
      if(isset($value1->fileurl)){
        if(strlen($value1->fileurl) > 13){
          $noSafe++;
          $vImgSafe = checkImgByBase($value1->fileurl);
          if($noSafe == 0){
            ?>
              <table class="ui table" style="font-size: 14px;width: 100%;page-break-after: always;">
                <tbody>
                  <tr>
                    <td colspan="" rowspan="" headers="">
                      <center style=""><img src="<?= $vImgSafe['path']; ?>" style="max-width: 500px;height: auto;width: auto;">
                      </center>
                    </td>
                  </tr>
                </tbody>
              </table>
              
            <?php
          }else{
            ?>
            <table class="ui table" style="font-size: 14px;width: 100%;page-break-after: always;">
                <tbody>
                  <tr>
                    <td colspan="" rowspan="" headers="">
                      <center style=""><img src="<?= $vImgSafe['path']; ?>" style="max-width: 500px;height: auto;width: auto;">
                      </center>
                    </td>
                  </tr>
                </tbody>
              </table>
            <?php
          }
        }
      }
    }
  }
  ?>
  <main>
  <table style="page-break-after: always;">
    <?php
    $jenisAspek = $this->m_model->selectas('status','Armada','jenis_aspeks','created_at','asc');
    if(count($jenisAspek)){
      foreach ($jenisAspek as $k1 => $value1) {
        ?>
        <table class="ui table" style="font-size: 13px;width: 100%;">
          <tbody>
            <tr>
              <td colspan="" rowspan="" headers="" style="color: white;vertical-align: center;">
                <div style="border-bottom: 25px solid #0ac8e0;border-right: 50px solid transparent;height: 0;width: 500px;">
                  <span style="padding-left: 20px">&nbsp;&nbsp;<?= $value1->nama_aspek; ?> </span>
                </div>
              </td>
            </tr>
        <?php
        if(isset($data['record']->id)){
          $dataArmadaElment = $this->m_model->selectcustom("select armada.id as armada_id, armada.name as armada_name, armada.cabang_id, armada.pelabuhan_id, armada_elements.id as armada_elments_id, armada_elements.url_canvas,  armada_elements.path_file, armada_elements.name from armada inner join armada_elements on armada.id=armada_elements.armada_id where armada.id=".$data['record']->id." order by armada_elements.id desc");

          if(count($dataArmadaElment) > 0){
            foreach ($dataArmadaElment as $k2 => $value2) {
              ?>
              
                  <tr>
                    <td colspan="" rowspan="" headers="" style="color: white;height: 40px;vertical-align: center;">
                      <div style="border-bottom: 25px solid #4a599f;border-right: 50px solid transparent;height: 0;width: 400px;">
                        <span style="padding-left: 30px;font-size: 13px;color:white">&nbsp;&nbsp;<?= $k2+1; ?>.&nbsp;<?= $value2->name;  ?></span>
                      </div>
                      <?php
                      $cekCanvas = $this->m_model->selectOneWhere3('id_armada',$value2->armada_id,'id_armada_elments',$value2->armada_elments_id,'id_jenis_aspek',$value1->id,'armada_canvas');
                      if($cekCanvas){
                        if(isset($cekCanvas->url_canvas)){
                          if(strlen($cekCanvas->url_canvas) > 15){
                            $imgCanvs=checkImgByBase($cekCanvas->url_canvas);
                          }else{
                            $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png'; 
                          }
                        }else{
                          if(isset($value2->path_file)){
                            if(strlen($value2->path_file) > 22){
                              $imgCanvs=checkImgByBase($value2->path_file);
                            }else{
                              $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                            }
                          }else{
                            $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                          }
                        }
                      }else{
                        if(isset($value2->path_file)){
                          if(strlen($value2->path_file) > 22){
                            $imgCanvs=checkImgByBase($value2->path_file);
                          }else{
                            $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                          }
                        }else{
                          $imgCanvs['path']= dirname($_SERVER['SCRIPT_FILENAME']).'/images/no-images.png';
                        }
                      }
                      ?>
                      <center style="padding-top: 2px"><img src="<?= $imgCanvs['path']; ?>" style="max-width: 780px;height: auto;width: auto;"></center>
                    </td>
                  </tr>
                </tbody>
              </table>
              <?php
              $subAspek = $this->m_model->selectas('jenis_aspek_id',$value1->id,'sub_aspeks','created_at','ASC');
              $cekNoSub = -1;
              if(count($subAspek) > 0){
                foreach ($subAspek as $k3 => $value3) {
                    $ArmdHsl = $this->m_model->selectcustom("select * from trans_armada_hasil where id_armada=".$data['record']->id." and id_armada_elments=".$value2->armada_elments_id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value3->id." group by icon_id");
                    if(count($ArmdHsl) > 0){
                      ?>
                      <table class="" style="font-size: 13px;width: 100%;border-collapse: collapse;page-break-before: always;">
                        <tbody>
                          <tr>
                            <td colspan="" rowspan="" headers="" style="width: 10px">
                              <label></label>
                            </td>
                            <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;color: black;">
                              <div style="border-bottom: 25px solid #ffd0a1;border-right: 50px solid transparent;height: 0;width: 500px;">
                                <span style="">&nbsp;&nbsp;<?= $value3->name; ?></span>
                              </div>
                              &nbsp;&nbsp;&nbsp;
                            </td>
                          </tr>
                        </tbody>
                        <?php
                      foreach ($ArmdHsl as $k4 => $value4) {
                        $cekNoSub++;
                        $getIcon = $this->m_model->selectOne('id',$value4->icon_id,'icon');
                        $HslArmdAllCEKS = $this->m_model->selectcustom("select * from trans_armada_hasil where id_armada=".$data['record']->id." and id_armada_elments=".$value2->armada_elments_id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value3->id." and icon_id=".$value4->icon_id."");
                        $HslArmdAll = $this->m_model->selectcustom("select * from trans_armada_hasil where id_armada=".$data['record']->id." and id_armada_elments=".$value2->armada_elments_id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value3->id." and icon_id=".$value4->icon_id." limit 3");
                        if(count($HslArmdAllCEKS) > 3){
                          $cekOneHslArmd = $this->m_model->selectOneWhere5('id_armada',$data['record']->id,'id_armada_elments',$value2->armada_elments_id,'id_jenis_aspek',$value1->id,'id_sub_jenis_aspek',$value3->id,'icon_id',$value4->icon_id,'trans_armada_hasil');
                          array_push($HslArmdAll,$cekOneHslArmd);
                        }
                        ?>
                        <tbody>
                          <?php
                            if($k4 > 2){
                              ?>
                                <tr>
                                  <td colspan="" rowspan="" headers="" style="width: 10px">
                                  </td>
                                  <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;color: black;height: 20px">
                                    <div style="border-right: 50px solid transparent;height: 0;width: 500px;height: 10px">
                                      <span style="">&nbsp;&nbsp;</span>
                                    </div>
                                  </td>
                                </tr> 
                              <?php
                            }
                          ?>
                          <tr>
                            <td colspan="" rowspan="" headers="" style="width: 10px">
                              <label></label>
                            </td>
                            <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;page-break-after: avoid;">
                              <ul style="list-style: square">
                                <li>
                                  <b>
                                    <?= $getIcon->name; ?>
                                  </b>  
                                </li>
                              </ul>
                            </td>
                          </tr>
                        </tbody>
                       
                        <tbody>
                          <tr>
                            <td colspan="" rowspan="" headers="">

                            </td>
                            <td colspan="2" rowspan="" headers="" style="background-color: #ebecec;">
                               <div style="background-color: white;">
                                    <div class="row" style="position: relative;left: 15px">
                                      <?php
                                      if(count($HslArmdAll) > 0){
                                        // $split = 4;
                                        // $HslArmdAll1 = array_chunk($HslArmdAll, $split);
                                        $HslArmdAll1 = $HslArmdAll;
                                        foreach ($HslArmdAll1 as $k6 => $value6) {
                                          ?>
                                        <div class="col-sm-2" style="background-color: #ebecec;margin-bottom: 10px;top:10px;margin-right: 10px;height: 190px;width:150px;font-size: 11px;max-width: 150px"
                                        >
                                        Nomor : <?= $value6->nomor; ?><br>
                                        Kondisi : <?= (strlen($value4->kondisi) > 60) ? substr($value4->kondisi,0,60).' ...' : $value4->kondisi; ?><br>
                                        Posisi : <?= (strlen($value4->posisi) > 60) ? substr($value4->posisi,0,60).' ...' : $value4->posisi; ?><br>
                                        Tahun Pengadaan : <?= $value6->tahun; ?><br>
                                        Lampiran Foto :<br>
                                        <?php
                                        $cekFiless = $this->m_model->selectOne('trans_id',$value6->id,'trans_armada_hasil_foto');
                                        if($cekFiless){
                                          if(isset($cekFiless->fileurl)){
                                            if(strlen($cekFiless->fileurl) > 13){
                                              $imgFf=checkImgByBase($cekFiless->fileurl);
                                              if(is_file($imgFf['path'])){
                                                if (file_exists($imgFf['path'])) {
                                                  ?>
                                                    <center style="padding-top:5px"><img src="<?= $imgFf['path']; ?>" width="150px" height="90px" style=""></center>
                                                  <?php
                                                }
                                              }else{
                                                $cekFiless = $this->m_model->selectas('trans_id',$value6->id,'trans_armada_hasil_foto');
                                                $imgFf['path'] = '';
                                                if(count($cekFiless) > 0){
                                                  foreach ($cekFiless as $kFiles => $vFiles) {
                                                    if(is_file(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$vFiles->fileurl)){
                                                      $imgFf=checkImgByBase($vFiles->fileurl);
                                                      break;
                                                    }
                                                  }
                                                  ?>
                                                    <center style="padding-top:5px"><img src="<?= $imgFf['path']; ?>" width="150px" height="90px" style=""></center>
                                                  <?php
                                                }
                                              }
                                            }
                                          }
                                        }
                                        ?>
                                      </div>
                                      <?php
                                      }
                                    }
                                    ?>
                                  </div>
                                </div>
                            </td>
                          </tr>
                        </tbody>
                      <?php
                    }
                    ?>
                  </table>
                  <?php
                }
              }
            }
            ?>
            <table style="page-break-after: always;">
            </table>
            <?php 
          }
        }
      }
    }
    ?>
    <!-- <div style="page-break-after: always;"></div> -->
    <?php
  }

  ?>
</table>
</main>
<hr>
<!-- BATAS LAPORAN BARU -->
<?php
if(isset($data['record']->id)){
  $dataArmadaElment = $this->m_model->selectcustom("select armada.id as armada_id, armada.name as armada_name, armada.cabang_id, armada.pelabuhan_id, armada_elements.id as armada_elments_id, armada_elements.url_canvas,  armada_elements.path_file, armada_elements.name from armada inner join armada_elements on armada.id=armada_elements.armada_id where armada.id=".$data['record']->id." order by armada_elements.id desc");
  if(count($dataArmadaElment) > 0){
    foreach ($dataArmadaElment as $k => $value) {
      $jenisAspek = $this->m_model->selectas('status','Armada','jenis_aspeks');

      ?>
      <table class="ui table bordered" style="font-size: 13px;width: 100%;page-break-after: always;">
          <tr>
            <td colspan="" rowspan="" headers="" style="background-color: #ffe5d3;">
              <center><b><h3><?= isset($value->name) ? $value->name: '-'; ?></h3></b></center>
            </td>
          </tr>
          <?php
              if(count($jenisAspek)){
                foreach ($jenisAspek as $k1 => $value1) {
                  ?>
                  <tr>
                  <td colspan="" rowspan="" headers="" style="">
                    <b><label style="position: relative;padding-left: 10px"><?= $k1+1; ?>. <?= $value1->nama_aspek; ?>  ( <?= isset($value->name) ? $value->name: '-'; ?> )</label></b>
                    <ul>
                      <?php
                      $subAspek = $this->m_model->selectas('jenis_aspek_id',$value1->id,'sub_aspeks');
                      if(count($subAspek) > 0){
                        foreach ($subAspek as $k2 => $value2) {
                          ?>
                          <li><?= $value2->name; ?></li>
                          <ul>
                            <?php
                            $ArmdHsl = $this->m_model->selectcustom("select * from trans_armada_hasil where id_armada=".$data['record']->id." and id_armada_elments=".$value->armada_elments_id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value2->id." group by icon_id");
                            if(count($ArmdHsl) > 0){
                              foreach ($ArmdHsl as $k3 => $value3) {
                                $getIcon = $this->m_model->selectOne('id',$value3->icon_id,'icon');
                                $HslArmdAll = $this->m_model->selectcustom("select * from trans_armada_hasil where id_armada=".$data['record']->id." and id_armada_elments=".$value->armada_elments_id." and id_jenis_aspek=".$value1->id." and id_sub_jenis_aspek=".$value2->id." and icon_id=".$value3->icon_id."");
                                ?>
                                <li><?= $getIcon->name; ?></li>
                                <ul style="list-style-type: lower-alpha;">
                                  <?php
                                  if(count($HslArmdAll) > 0){
                                    $nominal = 0;
                                    if(count($HslArmdAll) > 0){
                                      foreach ($HslArmdAll as $k => $valueS) {
                                        $nominal += (int)$valueS->total_icon;
                                      }
                                    }

                                    ?>
                                    <li>Jumlah : <?= count($HslArmdAll); ?></li>
                                    <li>Unit : <?= $nominal; ?></li>
                                    <?php
                                  }
                                  ?>
                                </ul>
                                <?php
                              }
                            }
                            ?>
                          </ul>
                          <?php
                        }
                      }
                      ?>
                    </ul>
                  </td>
                  </tr>
                  <?php
                }
              }
              ?>  
      </table>
      <?php
    }
  }
}
?>
</body>
</html>
