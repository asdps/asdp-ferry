<?php include 'header.php'; ?>

        <div class="row clearfix">
        <!-- Carousel -->
        <!-- <div class="col-lg-12 col-md-12"> -->
            <div class="card">
                <div id="demo2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <?php
                            if(count($slider) > 0){
                                foreach ($slider as $k => $value) {
                                    ?>
                                        <li data-target="#demo<?= $k ?>" data-slide-to="<?= $k ?>" class="<?= ($k == 0) ? 'active' : '' ?>"></li>
                                    <?php
                                }
                            }
                        ?>
                    </ul>
    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" style="width:100%;">
                        <?php
                            if(count($slider) > 0){
                                foreach ($slider as $k => $value) {
                                    $img=check_img($value->url);
                                    ?>
                                        <div class="carousel-item <?= ($k == 0) ? 'active' : '' ?>">
                                            <center>
                                                <img src="<?=$img['path'];?>" class="img-fluid" style="max-height:500px;max-width: 800px" alt="">
                                            </center>
                                            <!--<div class="carousel-caption">-->
                                            <!--  <h3>Chicago</h3>-->
                                            <!--  <p>Thank you, Chicago!</p>-->
                                            <!--</div>-->
                                        </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
    
                    <!-- Controls -->
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo2" data-slide="prev">
                      <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo2" data-slide="next">
                      <span class="carousel-control-next-icon"></span>
                    </a>
                  </div>
                </div>
            </div>    
        <!-- /Carousel -->

            <div class="col-lg-4 col-md-12" style="display: none">
                <!-- <div class="card">
                    <div class="body">

                        <h5 class="m-t-0">Lorem Ipsum</h5>
                        <p> ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis semper risus. In adipiscing ultrices tellus, in suscipit massa vehicula eu.</p>
                    </div>
                </div> -->
                <div class="row social-widget">
                <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                    <div class="card info-box-2 hover-zoom-effect instagram-widget" style="min-height: 0px">
                        <div class="icon"><i class="zmdi zmdi-instagram"></i></div>
                        <div class="content">
                            <div class="text"><a href="https://instagram.com/asdpdermpath/" target="_blank">Instagram</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                    <div class="card info-box-2 hover-zoom-effect facebook-widget" style="min-height: 0px">
                        <div class="icon"><i class="zmdi zmdi-facebook"></i></div>
                        <div class="content">
                            <div class="text"><a href="http://www.facebook.com/ASDermPath" target="_blank">Facebook</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                    <div class="card info-box-2 hover-zoom-effect twitter-widget" style="min-height: 0px">
                        <div class="icon"><i class="zmdi zmdi-twitter"></i></div>
                        <div class="content">
                            <div class="text"><a href="http://www.twitter.com/ASDPTweets" target="_blank">Twitter</a></div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                    <div class="card info-box-2 hover-zoom-effect youtube-widget" style="min-height: 0px">
                        <div class="icon"><i class="zmdi zmdi-youtube"></i></div>
                        <div class="content">
                                <div class="text"><a href="https://www.youtube.com/user/ASDPVideos">Youtube</a></div>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                    <div class="card info-box-2 hover-zoom-effect linkedin-widget" style="min-height: 0px">
                        <div class="icon"><i class="zmdi zmdi-linkedin"></i></div>
                        <div class="content">
                            <div class="text"><a href="http://www.linkedin.com/grp/home?gid=2003227" target="_blank">Linkedin</a></div>
                        </div>
                    </div>
                </div>
                </div>
            <!-- </div> -->
        </div>
        

<?php include 'footer.php'; ?>