<?php include 'header.php'; ?>

      <div class="row clearfix">
        <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
          <div class="card">
            <div class="body">
              <p>Page View</p>
              <input type="text" class="knob" value="42" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#00adef"
                readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
          <div class="card">
            <div class="body">
              <p>Storage</p>
              <input type="text" class="knob" value="81" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#49cdd0"
                readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
          <div class="card">
            <div class="body">
              <p>New User</p>
              <input type="text" class="knob" value="62" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#8f78db"
                readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
          <div class="card">
            <div class="body">
              <p>Total Visit</p>
              <input type="text" class="knob" value="38" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#f67a82"
                readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
          <div class="card">
            <div class="body">
              <p>Subscribers</p>
              <input type="text" class="knob" value="87" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#40b988"
                readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
          <div class="card">
            <div class="body">
              <p>Bounce Rate</p>
              <input type="text" class="knob" value="64" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#f7bb97"
                readonly>
            </div>
          </div>
        </div>
      </div>

        <div class="row clearfix">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>Recent Orders </h2>
                    </div>
                    <div class="body table-responsive members_profiles">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width:60px;">#</th>
                                    <th>Name</th>
                                    <th>Item</th>
                                    <th>Address</th>
                                    <th>Quantity</th>                                    
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><img src="http://via.placeholder.com/60x40"></td>
                                    <td>Hossein</td>
                                    <td>IPONE-7</td>
                                    <td>Porterfield 508 Virginia Street Chicago, IL 60653</td>
                                    <td>3</td>
                                    <td><span class="badge badge-success">DONE</span></td>
                                </tr>
                                <tr>
                                    <td><img src="http://via.placeholder.com/60x40"></td>
                                    <td>Camara</td>
                                    <td>NOKIA-8</td>
                                    <td>2595 Pearlman Avenue Sudbury, MA 01776 </td>
                                    <td>3</td>
                                    <td><span class="badge badge-success">DONE</span></td>
                                </tr>
                                <tr>
                                    <td><img src="http://via.placeholder.com/60x40"></td>
                                    <td>Maryam</td>
                                    <td>NOKIA-456</td>
                                    <td>Porterfield 508 Virginia Street Chicago, IL 60653</td>
                                    <td>4</td>
                                    <td><span class="badge badge-success">DONE</span></td>
                                </tr>
                                <tr>
                                    <td><img src="http://via.placeholder.com/60x40"></td>
                                    <td>Micheal</td>
                                    <td>SAMSANG PRO</td>
                                    <td>508 Virginia Street Chicago, IL 60653</td>
                                    <td>1</td>
                                    <td><span class="badge badge-success">DONE</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

<?php include 'footer.php'; ?>