<script src="<?=base_url();?>assets/frontend/js/jquery.js"></script> 
<script src="<?=base_url();?>assets/frontend/js/konva.min.js"></script>
<script src="<?=base_url();?>assets/frontend/js/html2canvas.min.js"></script>
<?php include 'header.php'; ?>
<style type="text/css">

</style>
<style type="text/css">
    #ceks {
      width: 100%;
      height: 100%;
    }
  </style>
<?php 
    $dataSes=$this->session->flashdata('sukses');
    if($dataSes!=""){ ?>
    <center><div class="alert alert-success"><strong>Sukses! </strong> <?=$dataSes;?></div></center>
    <?php } ?>
    <?php 
    $dataSes2=$this->session->flashdata('error');
    if($dataSes2!=""){ ?>
    <center><div class="alert alert-danger"><strong> Gagal! </strong> <?=$dataSes2;?></div></center>
<?php } ?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="card">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="btn-group">
              <a href="<?=base_url();?>backend/pelabuhan/show/show/<?= $record->id ?>/<?= $pelabuhanss->id ?>?page=<?= $page; ?>"  class="btn btn-primary btn-sm" style="color: #fff">Kembali</a>
              <a href="javascript:void(0)"  class="btn btn-danger btn-sm stopDrag" style="color: #fff">Stop Drag & Drop</a>
            </div>
          </div>
          <div class="col-md-12 alertLah">

          </div>
        </div>
        <div class="wrapper content">
          <div class="container-fluid">
            <h3><?= $record->nama_aspek;  ?> <?= $pelabuhanss->name; ?></h3>
            <div class="row">

                <?php
                $img=check_img($pelabuhanss->foto_drag);
                $cekIMG = isset($pelabuhanss->foto_drag) ? $pelabuhanss->foto_drag : 'images/images.png';
                if(file_exists(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$cekIMG)){
                  $cekIMG = $pelabuhanss->foto_drag;
                }else{
                  $cekIMG = 'images/images.png';
                }
                list($width, $height, $type, $attr) = getimagesize(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$cekIMG);
                $ratio = $width/$height; // width/height
                $widthR = 600;
                $heightR = 700/$ratio;
                ?>

                <div class="col-lg-8 mb-3 pr-0" id="ceks">
                <div style="background-image: url('<?=$img['path'];?>'); background-size: <?= $widthR; ?>px <?= $heightR; ?>px;background-repeat: no-repeat;" id="containers" data-id="containers"></div>

              </div>
              <div class="col-lg-4 mb-3 p-0" style="background: #fffafa">
                <div class="bg-warning text-center py-1">
                  <h3 class="text-white font-weight-bold"><span style="font-weight: 500"><?= $record->nama_aspek;  ?></span></h3>
                </div>
                <div class="px-3 my-3" style="max-width: 310px;overflow-y: hidden;overflow-x: scroll;">
                  <ul style="position: relative;left: -50px;font-size: 13px">
                    <?php
                    if(count($records) > 0){
                      $no = 0;
                      foreach ($records as $k => $value) {

                        if($value->name != ''){
                          $no++;

                          ?>              
                          <li style="list-style: none;font-size: 13px">
                            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block" style="padding-top: 2px;height: 50px;width: 300px;">
                              <div class="row">
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-lg-2">
                                      <span class="rounded-circle text-white bg-warning mr-1" style="padding: 3px 8px;"><?= $no; ?></span>
                                    </div>
                                    <div class="col-lg-9">
                                      <p class="text-warning">
                                        <?= $value->name; ?>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-2 pull-left" style="text-align: left;">
                                  <span class="text-warning" style="position: relative;right: 7px">Titik</span>
                                </div>
                                <div class="col-md-2">
                                  <span class="text-warning " style="position: relative;right: 7px">Unit Total</span>
                                  <br>
                                </div>
                              </div>
                            </a>
                            <ul class="ml-menu" style="display: none;font-size: 13px;position: relative;left: -5px">
                              <?php
                              if(count($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon'))){
                                foreach ($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon') as $keySubIco => $valueSubIco) {

                                  $cekReal = $this->m_model->getOne($valueSubIco->trans_icon_id,'icon');
                                  $selectOneTotal = $this->m_model->selectas4(
                                    'id_pelabuhan', $pelabuhanss->id, 
                                    'id_jenis_aspek', $record->id, 
                                    'id_sub_jenis_aspek', $value->id, 
                                    'icon_id', $cekReal['id'], 
                                    'trans_pelabuhans_hasil'
                                  );
                                  $imgs=check_img($cekReal['path_file']);
                                  ?>
                                  <?php 
                                  $coun = 0;
                                  $hasilCount = count($this->m_model->selectas4('id_pelabuhan',$pelabuhanss->id,'id_jenis_aspek',$record->id,'id_sub_jenis_aspek',$value->id,'icon_id',$cekReal['id'],'trans_pelabuhans_hasil'));

                                  $color = 'background-color: #f00;color: white;border-radius:20px;border:3px solid #f00 !important';
                                  if($hasilCount > 0){
                                    $coun = $hasilCount;
                                    $color = '-';
                                  }
                                  ?>
                                  <li style="font-size: 13px;padding-bottom:1px;width: 270px" id="drag-items">
                                    <div class="row">
                                      <div class="col-md-8">
                                        <img src="<?=$imgs['path'];?>" class="img-responsive drag drag-items" data-key="<?= $keySubIco + 1; ?>" data-id="<?= $cekReal['id']; ?>" data-aspek="<?= $value->name; ?>" data-name="<?= $cekReal['name']; ?>" data-sub="<?= $value->id; ?>" style="cursor: pointer; max-width: 50px; max-height:50px;width: 30px;padding-bottom: 1px;<?= $color; ?>" data-fancybox="images<?= $keySubIco + 1; ?>" href="<?=$imgs['path'];?>" draggable="true">&nbsp;
                                        <span style="font-size: 13px;">
                                          <?= $cekReal['name']; ?><br>
                                        </span>
                                      </div>
                                      <div class="col-md-2">
                                        <span style="position: relative;right: 20px"><?= $coun; ?></span>
                                      </div>
                                      <div class="col-md-2">
                                          <?php
                                          $nominal = 0;
                                          if(count($selectOneTotal) > 0){
                                            foreach ($selectOneTotal as $k => $valueS) {
                                              $nominal += (int)$valueS->total_icon;
                                            }
                                          }
                                          ?>
                                          <span style="position: relative;right: 20px"><?= $nominal; ?></span><p><?= $valueSubIco->units; ?></p>  
                                        </div>

                                      </div>
                                    </li>
                                    <hr style="width: 270px">
                                    <?php
                                  }
                                }
                                ?>
                              </ul>
                            </li>
                            <?php          
                          }
                        }
                      }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>

   
    var stage = new Konva.Stage({
      container: 'containers',
      width: <?= $widthR; ?>,
      height: <?= $heightR; ?>
    });
    function fitStageIntoParentContainer() {
        var container = document.querySelector('#containers');
        // now we need to fit stage into parent
        var containerWidth = container.offsetWidth;
        // to do this we need to scale the stage
        var scale = containerWidth / container.offsetWidth;

        stage.width(container.offsetWidth * scale);
        stage.height(container.offsetHeight * scale);
        stage.scale({ x: scale, y: scale });
        stage.draw();
      }

      fitStageIntoParentContainer();
      // adapt the stage on any window resize
      window.addEventListener('resize', fitStageIntoParentContainer);
    // stage.attrs.width = 100%;
    // console.log('stage',stage)
    // var canvas = new fabric.Canvas();
    // canvas.setDimensions({width: '100%', height: '100%'}, {cssOnly: true});
    var layer = new Konva.Layer();
    stage.add(layer);

      var widths = 20;
      $.ajax({
        url: '<?= site_url('backend/pelabuhan/getData'); ?>',
        type: 'post',
        data: {id_jenis_aspek: '<?= $record->id; ?>',id_pelabuhan:'<?= $pelabuhanss->id; ?>'},
        dataType: 'json',
        success:function(response){
          
          if(response.length > 0){
            $.each(response,function(k,v){
              Konva.Image.fromURL(v.url, function(yoda) {
                layer.add(yoda);

                var groups = new Konva.Group({
                  x: parseFloat(v.pointer_x),
                  y: parseFloat(v.pointer_y),
                });
                layer.add(groups);
                groups.add(yoda);
                
                if(v.width == null){
                  widths = 20
                  heights = 20
                }else{
                  widths = v.width;
                  heights = v.width;
                }

                yoda.draggable(true);
                yoda.setAttrs({
                  width: widths,
                  height: heights,
                  id_pelabuhan: v.id_pelabuhan,
                  id_jenis_aspek: v.id_jenis_aspek,
                  id_sub_jenis_aspek:v.id_sub_jenis_aspek,
                  primary_key: v.primary_key,
                  pointer_x: v.pointer_x,
                  pointer_y: v.pointer_y,
                  total_icon: v.total_icon,
                  cek_target:'true'
                });
                console.log('groups',groups)
                
                layer.draw(); // It
              });

            });  
          }
        },
        error: function() {
          $('.alertLah').html(`
            <div class="alert alert-danger">
            Refresh Kembali!
            </div>
            `);
        }
      });

      
      // END ADD FOTO DARI KUMPULAN ARRAY
      
      // what is url of dragging element?
      var itemURL = '';
      var dataIds = '';
      var dataAspek = '';
      var dataName = '';
      $(document).on('dragstart touchstart','.drag-items', function(e) {
        itemURL = e.target.src;
          // 
          dataIds = e.target.dataset.id;
          $('input[name="icon_id"]').val(dataIds);
          $('input[name="url"]').val(e.target.src);
          $('input[name="aspek"]').val(e.target.dataset.aspek);
          $('input[name="id_sub_jenis_aspek"]').val(e.target.dataset.sub);
          $('input[name="nama"]').val(e.target.dataset.name);
        });

      var con = stage.container();
      con.addEventListener('dragover', function(e) {
        e.preventDefault(); 
      });
      arrayHasil = [];
      con.addEventListener('drop', function(e) {
        e.preventDefault();
        stage.setPointersPositions(e);
        Konva.Image.fromURL(itemURL, function(image) {
        console.log('asd',stage.getPointerPosition())
          image.position(stage.getPointerPosition());
          image.width(20);
          image.height(20);
          image.draggable(true);
          arrayHasil.push(stage.getPointerPosition());
          imagesArr = image;

          imagesArr.setAttrs({
            icon_id:$('input[name="icon_id"]').val(),
            url:$('input[name="url"]').val(),
            aspek:$('input[name="aspek"]').val(),
            id_sub_jenis_aspek:$('input[name="id_sub_jenis_aspek"]').val(),
            nama:$('input[name="nama"]').val(),
            pointer_x: stage.getPointerPosition().x,
            pointer_y: stage.getPointerPosition().y,
            // x:156,
            // y:319.46665954589844,
          });
          layer.add(image);
          
          layer.draw();

          $('input[name="pointer_x"]').val(stage.getPointerPosition().x);
          $('input[name="pointer_y"]').val(stage.getPointerPosition().y);
          $('input[name="primary_key"]').val(arrayHasil.length);
          $('.deletesData').hide();
          var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:20px;" class="imgResze"></center>`;
          $('.appendImg').html(imgApp);
        });
      });

      con.addEventListener('touchend',function(e) {
        console.log('touchend',e,itemURL);
          e.preventDefault();
        if(itemURL){
          stage.setPointersPositions(e);
          Konva.Image.fromURL(itemURL, function(image) {
            console.log('stage.getPointerPosition()',stage.getPointerPosition());

            image.position(stage.getPointerPosition());
            image.width(20);
            image.height(20);
            image.draggable(true);
            arrayHasil.push(stage.getPointerPosition());
            imagesArr = image;

            imagesArr.setAttrs({
              icon_id:$('input[name="icon_id"]').val(),
              url:$('input[name="url"]').val(),
              aspek:$('input[name="aspek"]').val(),
              id_sub_jenis_aspek:$('input[name="id_sub_jenis_aspek"]').val(),
              nama:$('input[name="nama"]').val(),
              pointer_x: stage.getPointerPosition().x,
              pointer_y: stage.getPointerPosition().y,
              // x:156,
              // y:319.46665954589844,
            });

            $('input[name="pointer_x"]').val(stage.getPointerPosition().x);
            $('input[name="pointer_y"]').val(stage.getPointerPosition().y);
            $('input[name="primary_key"]').val(arrayHasil.length);
            $('.deletesData').hide();
            var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:20px;" class="imgResze"></center>`;
            $('.appendImg').html(imgApp);
            layer.add(image);
            layer.draw();

          });
        }
      });


      layer.addEventListener('dragmove', function(e) {
       
      });
      
           
      stage.on('click tap', function(e) {
        console.log('click')
        // $('.modal-dialog').addClass('ampase');
        imagesArr = e.target;
        if(e.target.attrs.cek_target === 'true'){
           $.ajax({
            url: '<?= site_url('backend/pelabuhan/getDataOne/'); ?>',
            type: 'post',
            data: {id_jenis_aspek: e.target.attrs.id_jenis_aspek, id_pelabuhan:e.target.attrs.id_pelabuhan, primary_key:e.target.attrs.primary_key,pointer_x:e.target.attrs.pointer_x,pointer_y:e.target.attrs.pointer_y},
            dataType: 'json',
            success:function(response){
              console.log('e',response)
              if(e.target._lastPos == null){
                var points = {x:response.record.pointer_x,y:response.record.pointer_y};
              }else{
                var points = {x:e.target._lastPos.x,y:e.target._lastPos.y};
              }

              if(response){
                imagesArr = e.target;
                $("#add-panel").modal("show");
                $('.modal-backdrop').removeClass();


                $('input[name="id"]').val(response.record.id);
                $('input[name="id_pelabuhan"]').val(response.record.id_pelabuhan);
                $('input[name="id_jenis_aspek"]').val(response.record.id_jenis_aspek);
                $('input[name="id_sub_jenis_aspek"]').val(response.record.id_sub_jenis_aspek);
                $('input[name="icon_id"]').val(response.record.icon_id);
                $('input[name="url"]').val(response.record.url);
                $('input[name="pointer_x"]').val(points.x);
                $('input[name="pointer_y"]').val(points.y);
                $('input[name="primary_key"]').val(response.record.primary_key);
                $('input[name="kategori"]').val(response.record.kategori);
                $('input[name="nama"]').val(response.record.nama);
                $('input[name="aspek"]').val(response.record.aspek);
                $('input[name="nomor"]').val(response.record.nomor);
                $('input[name="kondisi"]').val(response.record.kondisi);
                $('input[name="posisi"]').val(response.record.posisi);
                $('input[name="tahun"]').val(response.record.tahun);
                $('input[name="total_icon"]').val(response.record.total_icon);
                $('input[name="height"]').val(e.target.attrs.height);
                $('input[name="width"]').val(e.target.attrs.width);
                var imgApp = `<center><img src="`+response.record.url+`" alt="" style="width:30px;" class="imgResze"></center>`;
                $('.appendImg').html(imgApp);
                $('.deletesData').show();
                if(response.record_foto){
                  $('.ReadshowImg').show();
                  $('.showImg').html('');
                  var cekNo = 0;
                  $.each(response.record_foto,function(k,v){
                      if(v.fileurl.length > 12){
                        cekNo++;
                        var kkNo = (cekNo == 1) ? 'active' : '';
                        $('.showImg').append(`
                          <div class="carousel-item `+kkNo+`">
                            <img src="<?php echo base_url(); ?>`+v.fileurl+`" class="img-fluid" style="width:100%;height:420px;" alt="">
                          </div>
                        `);
                      }
                  })
                }
              }
            },
            error: function() {
              $('.alertLah').html(`
                <div class="alert alert-danger">
                Terjadi Kesalahan!
                </div>
                `);
            }
          });
        }else{
          // console.log('e',e.target._lastPos.x,e.target._lastPos.y)
          var itemURL = $(e.target.attrs.image).attr('src');
          if(itemURL){
            stage.setPointersPositions(e);
            Konva.Image.fromURL(itemURL, function(image) {
              if(e.target._lastPos == null){
                var points = {x:e.target.attrs.pointer_x,y:e.target.attrs.pointer_y};;
              }else{
                var points = {x:e.target._lastPos.x,y:e.target._lastPos.y};
              }
             
              image.draggable(true);
              arrayHasil.push(points);
              layer.draw();
              $("#add-panel").modal("show");
              $('.modal-backdrop').removeClass();
              $('input[name="width"]').val(e.target.attrs.width);
              $('input[name="height"]').val(e.target.attrs.height);
              $('input[name="pointer_x"]').val(points.x);
              $('input[name="pointer_y"]').val(points.y);
              $('input[name="primary_key"]').val(arrayHasil.length);
              $('.deletesData').hide();

              $.ajax({
                url: '<?= site_url('backend/pelabuhan/getDataNumWithUrl'); ?>',
                type: 'post',
                data: {
                  id_jenis_aspek: '<?= $record->id; ?>',
                  id_pelabuhan:'<?= $pelabuhanss->id; ?>',
                  id_sub_jenis_aspek:e.target.attrs.id_sub_jenis_aspek,
                  icon_id:e.target.attrs.icon_id,
                },
                dataType: 'json',
                success:function(response){
                  // 
                  console.log('response12',response)
                  $('input[name="nomor"]').val(response.count);
                  $('input[name="icon_id"]').val(e.target.attrs.icon_id);
                  $('input[name="url"]').val(itemURL);
                  $('input[name="nama"]').val(e.target.attrs.nama);
                  $('input[name="aspek"]').val(e.target.attrs.aspek);
                  $('input[name="id_sub_jenis_aspek"]').val(e.target.attrs.id_sub_jenis_aspek);
                },
                error: function() {
                  
                }
              });
              
              var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:30px;" class="imgResze"></center>`;
              $('.appendImg').html(imgApp);
            });
          }
        }
      });
      
      $(document).on('click','.stopDrag',function(){
          itemURL = null;
       });

  </script>
  <style type="text/css">
    .ampase{
      margin:140px auto;
    }
  </style>
  <div class="modal fade " id="add-panel" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-md ampase" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Keterangan Data Jenis Aspek</h4>
        </div>
        <div class="modal-body">
          <form id="formModals" action="<?= base_url('backend/pelabuhan/store'); ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
              <input type="hidden" name="id">
              <input type="hidden" name="id_pelabuhan" value="<?= $pelabuhanss->id; ?>">
              <input type="hidden" name="id_jenis_aspek" value="<?= $record->id; ?>">
              <input type="hidden" name="id_sub_jenis_aspek">
              <input type="hidden" name="icon_id">
              <input type="hidden" name="url">
              <input type="hidden" name="pointer_x">
              <input type="hidden" name="pointer_y">
              <input type="hidden" name="primary_key">
              <input type="hidden" name="kategori" value="Pelabuhan">
              <input type="hidden" name="image">
              <input type="hidden" name="height">
              <div class="col-md-8">
                <div class="form-group form-line">
                  <label>Resize Width Image</label>
                  <input name="width" id="resize" placeholder="Width / Pixels" type="number" class="form-control" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')" value="30" />
                  <label class="label-error" style="color: red;"></label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <div class="btn btn-sm btn-success resizeNow" style="position: relative;top: 12px;">
                    Resize
                  </div>
                </div>
              </div>
              <div class="col-md-12 appendImg">

              </div>
              <textarea name="url_canvas" id="url_canvas" style="display: none;"></textarea>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Nama</label>*
                  <input name="nama" placeholder="Nama" type="text" class="form-control"/>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Aspek</label>*
                  <input name="aspek" placeholder="Aspek" type="text" class="form-control"/>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Nomor</label>*
                  <input name="nomor" placeholder="Nomor" type="text" class="form-control"  />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Kondisi</label>*
                  <input name="kondisi" placeholder="Kondisi" type="text" class="form-control" maxlength="20" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Posisi</label>*
                  <input name="posisi" placeholder="Posisi" type="text" class="form-control" maxlength="20" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Tahun Pengadaan</label>
                  <input name="tahun" placeholder="Tahun Pengadaan" type="text" class="form-control" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')"/>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Deskripsi Total</label>
                  <div class="form-line">
                    <input name="total_icon" placeholder="Deskripsi Total" type="text" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')" class="form-control" />
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-line">
                  <div class="clearfix"></div>
                  <label>*click below to browse file</label>
                  <input name="icon[]" type="file" class="form-control" style="cursor: pointer;" accept="image/*" multiple="">
                </div>
              </div>

              <div class="card ReadshowImg" style="display: none;">
                <div id="demo2" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                        <!-- <ul class="carousel-indicators">
                          <li data-target="#demo2" data-slide-to="0" class=""></li>
                          <li data-target="#demo2" data-slide-to="1" class=""></li>
                          <li data-target="#demo2" data-slide-to="2" class=""></li>
                        </ul> -->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner showImg" style="width:100%;">


                        </div>

                        <!-- Controls -->
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo2" data-slide="prev" >
                          <span class="carousel-control-prev-icon" style="background-color: #1f91f3"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo2" data-slide="next">
                          <span class="carousel-control-next-icon" style="background-color: #1f91f3"></span>
                        </a>
                      </div>
                    </div><br>
                    <div class="col-md-12 pull-right" style="text-align: right;">
                      <button type="button" class="btn btn-default" id="cancel-button" >Cancel</button>
                      <button type="button" class="btn btn-danger deleteDatak deletesData" id="cancel-button" data-dismiss="modal" style="display: none">Delete</button>
                      <button type="button" class="btn btn-primary saveBtn" id="confirm-button">Save</button>
                      <button type="submit" class="btn btn-primary" id="bnke_btn" style="display: none;">Save</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">

              </div>
            </div>
          </div>

          <!-- EDIT MODAL -->


          <script type="text/javascript">
            $(document).on('click','.resizeNow',function(e){
              console.log('imagesArr',imagesArr)
              var width = $('input[name="width"]').val();
              var id = $('input[name="id"]').val();
              if(parseFloat(width) < 10){
                $('.label-error').text('Tidak Boleh Kurang Dari 10');
              }else if(parseFloat(width) > 50){
                $('.label-error').text('Tidak Boleh Lebih Dari 20');
              }else{
                if(width == ''){
                  width = 20;
                }
                  $('.label-error').text('');
                  $('.imgResze').css("width",width);
                  $('.imgResze').css("height",width);
                  if(id){
                // 
                imagesArr.setAttrs({
                  width : width,
                  height : width,
                  pointer_x : $('input[name="pointer_x"]').val(),
                  pointer_y : $('input[name="pointer_y"]').val(),
                });
              }else{
                  imagesArr.setAttrs({
                    width : width,
                    height : width,
                    pointer_x : $('input[name="pointer_x"]').val(),
                    pointer_y : $('input[name="pointer_y"]').val(),
                  });
              }
              layer.draw();
            }
        });

            $(document).on('click','#cancel-button',function(){
              window.location.reload();
            });

            $(document).on('keypress', function (e) {
              // console.log('asd')
              // e.preventDefault();
                 if(e.which === 13){
                    $('.saveBtn').trigger('click');
                 }
            });
            
            $(document).on('click','.saveBtn',function(){
              var element = $('#containers');
              var getCanvas= '';
              html2canvas(element, {
               onrendered: function (canvas) {
                getCanvas = canvas;
                var imgageData = getCanvas.toDataURL("image/png");
                var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                  // 
                  $('#url_canvas').val(imgageData);
                }
              });
          // 
          $('.saveBtn').attr('disabled','disabled');
          $('.saveBtn').text('Wait for a while');
          var data = $('#formModals').serializeArray();
          // 
          time = 5;
          interval = setInterval(function(){
            time--;
            if(time == 0){
              clearInterval(interval);
              $( "#bnke_btn" ).trigger('click');
              
            }
          },1000);
        });

            $(document).on('click','.deleteDatak',function(){
              
              var element = $('#containers');
              var getCanvas= '';
              html2canvas(element, {
               onrendered: function (canvas) {
                getCanvas = canvas;
                var imgageData = getCanvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                  // 
                }
              });

              var data = $('#formModals').serializeArray();
          // 
          $.ajax({
            url: '<?= site_url('backend/pelabuhan/delete'); ?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success:function(response){
              if(response.status == true){
                window.location.reload();
              }else{
                $('.alertLah').html(`
                  <div class="alert alert-danger">
                  Gagal Menghapus Data
                  </div>
                  `);
              }
              $("#add-panel").modal("hide");

            },
            error: function() {
              $('.alertLah').html(`
                <div class="alert alert-danger">
                Terjadi Kesalahan!
                </div>
                `);
              $("#add-panel").modal("hide");

            }
          });
        });

          </script>
          <?php include 'footer.php'; ?>