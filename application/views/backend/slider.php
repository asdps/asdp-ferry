<?php include 'header.php'; ?>
<?php 
    $dataSes=$this->session->flashdata('sukses');
    if($dataSes!=""){ ?>
    <center><div class="alert alert-success"><strong>Sukses! </strong> <?=$dataSes;?></div></center>
    <?php } ?>
    <?php 
    $dataSes2=$this->session->flashdata('error');
    if($dataSes2!=""){ ?>
    <center><div class="alert alert-danger"><strong> Gagal! </strong> <?=$dataSes2;?></div></center>
<?php } ?>
    <?php if (!$this->input->get('add') && !$this->input->get('edit')) { ?>
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <div class="row">
                            <div class="col-lg-10">
                                <h2>List Slider</h2>
                                
                            </div>
                            <div class="col-lg-2">
                               <a class="btn btn-primary" href="<?= site_url('backend/slider/create'); ?>">Add Slider</a>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                            <thead>
                                <tr>
                                    <th style="width: 15px">#</th>
                                    <th>Judul</th>                             
                                    <th>Slider</th>                             
                                    <th style="width: 80px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = $this->m_model->selectcustom('select * from slider');
                            if (count($data) > 0) {
                                foreach ($data as $key => $value) {
                                $img=check_img($value->url);
                            ?>
                                <tr>
                                    <td><?= $key + 1; ?></td>
                                    <td><?= $value->name; ?></td>
                                    <td>
                                        <center>
                                            <img src="<?=$img['path'];?>" class="img-fluid" style="max-height: 100px;max-width: 150px">
                                        </center>
                                    </td>
                                    <td>
                                        <a class="confirm badge badge-info" msg="Do you want to Edit data?" href="<?= site_url('backend/slider/edit/').$value->id; ?>">Edit</a>
                                   
                                        <a class="confirm badge badge-warning" msg="Are you sure to Delete data?" href="<?= site_url('backend/slider/destroy/').$value->id; ?>">Delete</a>
                                    </td>
                                </tr>
                            <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

<script>
$(function(){
    $('#product_store').select2();
    $('.select2').select2();
})
</script>

<?php include 'footer.php'; ?>