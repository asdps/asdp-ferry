<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>ASDP | Dashboard</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?=base_url();?>assets/backend/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Bootstrap Select Css -->
    <link href="<?=base_url();?>assets/backend/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= site_url('assets/backend/plugins/jquery-datatable/dataTables.bootstrap4.min.css'); ?>">

    <link rel="stylesheet" href="<?=base_url();?>assets/backend/plugins/jvectormap/jquery-jvectormap-2.0.3.css"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/backend/plugins/morrisjs/morris.css" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="<?=base_url();?>assets/backend/css/main.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/backend/css/color_skins.css">
    <link rel="stylesheet" href="<?= site_url('assets/backend/plugins/jquery-datatable/dataTables.bootstrap4.min.css'); ?>">
    <script type="text/javascript" src="<?= site_url('assets/ckeditor/ckeditor.js'); ?>"></script>
    <script type="text/javascript" src="<?= site_url('assets/justify/plugin.js'); ?>"></script>

    <style type="text/css">
        .theme-orange .navbar{
            background-color: #fff0;
        }
    </style>
    <script type="text/javascript">
        function goBack() {
            console.log('asd')
            window.history.back();
        }

        
    </script>
</head>
<body class="theme-orange">
<!-- Overlay For Sidebars -->
<div class="overlay"></div><!-- Search  -->
<div class="search-bar">
    <div class="search-icon"> <i class="material-icons">search</i> </div>
    <input type="text" placeholder="Explore Nexa...">
    <div class="close-search"> <i class="material-icons">close</i> </div>
</div>
<nav class="navbar" style="width: 0px">
    <div class="col-12">
        
        <div class="navbar-header" style="width: 250px;">
            <a href="javascript:void(0);" class="bars"></a>
            <center>
                <a class="navbar-brand" href="<?= base_url(); ?>" >
                    <img src="<?= site_url('assets/images/logo-dark.png') ?>" class="img-responsive" style="width:84px">
                </a>
            </center>
        </div>
        <!-- <ul class="nav navbar-nav navbar-left">
            <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
        </ul> -->
    </div> 
</nav>
<?php //print_r($this->session->userdata('admin_data')); die();?>

<aside id="leftsidebar" class="sidebar">
     <!-- User Info -->
    <div class="user-info d-flex align-items-center" style="height: 130px;">
        <div class="col-12">     
            <?php
                $fotoUser = $this->session->userdata('admin_data')->foto_user;
                if(isset($fotoUser)){
                    $img=check_img($fotoUser);
                    ?>
                        <center class="image" style="float: none;margin: 0px;">
                            <img src="<?=$img['path'];?>" width="48" height="48" alt="User"/>
                        </center>
                    <?php      
                }else{
            ?>
                    <center class="image" style="float: none;margin: 0px;">
                        <img src="<?=base_url();?>images/users.png" width="48" height="48" alt="User"/>
                    </center>
            <?php
                }
            ?>
            <center class="info-container" style="float:none;">
                <center><div class="name" data-toggle="dropdown"><?=$this->session->userdata('admin_data')->username;?></div></center>
                <?php
                    if(!isset($this->session->userdata('admin_data')->id_cabang)){
                ?>
                <div class="email" style="font-size: 9px"><?= $this->session->userdata('admin_data')->email;?></div>
                <?php
                    }
                ?>
            </center>
        </div>    
    </div>
    <div class="clearfix">
        
    </div>
    <!-- #User Info --> 
    <!-- Menu -->
     <?php
        $array = [];
            
            $getUser = $this->m_model->getOne($this->session->userdata('admin_data')->id,'users');
            // print_r($this->session->userdata('admin_data'));
            // die();
            if(($getUser['roles'] == 1) || ($getUser['roles'] == 2)){
                $trueAdm = 'salah';
                $photo = $this->m_model->all('photo_log');
                if(count($photo) > 0){
                    foreach ($photo as $k => $value) {
                        array_push($array,$value);
                    }
                }

                $video = $this->m_model->all('video_log');
                if(count($video) > 0){
                    foreach ($video as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $armada = $this->m_model->all('armada_log');
               
                if(count($armada) > 0){
                    foreach ($armada as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhan = $this->m_model->all('pelabuhans_log');
                if(count($pelabuhan) > 0){
                    foreach ($pelabuhan as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhanHasil = $this->m_model->all('trans_pelabuhans_hasil_log');
                if(count($pelabuhanHasil) > 0){
                    foreach ($pelabuhanHasil as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $armadaHasil = $this->m_model->all('trans_armada_hasil_log');
                if(count($armadaHasil) > 0){
                    foreach ($armadaHasil as $k => $value) {
                        array_push($array,$value);   
                    }
                }
            }else{
                $trueAdm = 'benar';
                $photo = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'photo_log');
                if(count($photo) > 0){
                    foreach ($photo as $k => $value) {
                        array_push($array,$value);
                    }
                }

                $video = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'video_log');
                if(count($video) > 0){
                    foreach ($video as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $armada = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'armada_log');
                if(count($armada) > 0){
                    foreach ($armada as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhan = $this->m_model->selectwhere('cabang_id',$this->session->userdata('admin_data')->id_cabang,'pelabuhans_log');
                if(count($pelabuhan) > 0){
                    foreach ($pelabuhan as $k => $value) {
                        array_push($array,$value);   
                    }
                }

                $pelabuhanHasil = $this->m_model->all('trans_pelabuhans_hasil_log');
                if(count($pelabuhanHasil) > 0){
                    foreach ($pelabuhanHasil as $k => $value) {
                        $selOne = $this->m_model->selectOne('id',$value->id_pelabuhan,'pelabuhans');
                        if((isset($selOne)) && (isset($this->session->userdata('admin_data')->id_cabang)) && ($selOne->cabang_id == $this->session->userdata('admin_data')->id_cabang)){
                            array_push($array,$value);   
                        }
                    }
                }

                $ArmadaHasil = $this->m_model->all('trans_armada_hasil_log');
                if(count($ArmadaHasil) > 0){
                    foreach ($ArmadaHasil as $k => $value) {
                        $selOne = $this->m_model->selectOne('id',$value->id_armada,'armada');
                        if((isset($selOne)) && (isset($this->session->userdata('admin_data')->id_cabang)) && ($selOne->cabang_id == $this->session->userdata('admin_data')->id_cabang)){
                            array_push($array,$value);   
                        }
                    }
                }
            }
    ?>
    <div class="menu">
        <?php
            if(isset($getUser['roles']) && !is_null($getUser['roles']) && ($getUser['roles'] == 1 || $getUser['roles'] == 2)){
                ?>
                    <ul class="list">
                        <li><a href="<?=base_url();?>"><i class="zmdi zmdi-home"></i><span>Beranda</span> </a> </li>
                        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-folder-outline"></i><span>Master Data</span></a>
                            <ul class="ml-menu">
                                <li><a href="<?=base_url();?>panel/icon">Icon</a></li>
                                <li><a href="<?=base_url();?>panel/cabang">Cabang</a></li>
                                <li>
                                    <a href="javascript:void(0)" class="menu-toggle">Aspek</a>
                                    <ul class="ml-menu">
                                        <li><a href="<?=base_url();?>panel/aspekArmada" title="">Armada</a></li>
                                        <li><a href="<?=base_url();?>panel/aspekPelabuhan" title="">Pelabuhan</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?=base_url();?>panel/pelabuhan">Pelabuhan</a></li>
                                <li><a href="<?=base_url();?>panel/armada">Armada</a></li>
                            </ul>
                        </li>
                        <li> <a href="<?=base_url();?>panel/photo"><i class="zmdi zmdi-collection-image-o"></i><span>Foto</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/video"><i class="zmdi zmdi-collection-video"></i><span>Video</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/file"><i class="zmdi zmdi-file"></i><span>Standarisasi</span> </a> </li>
                        <li> <a href="<?=base_url();?>backend/notifikasi"><i class="zmdi zmdi-notifications-active"></i><span>Notifikasi <span class="rounded-circle text-white bg-warning mr-1" style="padding: 1px 8px;"><?= count($array); ?></span></span> </a> </li>
                        <li> 
                            <a href="javascript:void(0)" class="menu-toggle"><i class="zmdi zmdi-file"></i><span>Laporan</span> </a> 
                            <ul class="ml-menu">
                                <li><a href="<?=base_url();?>backend/laporan">Pelabuhan</a></li>
                                <li><a href="<?=base_url();?>backend/laporan/armada">Armada</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url();?>panel/logout"><i class="zmdi zmdi-tag-close"></i><span>Logout</span></a></li>
                    </ul>
                <?php
            }else if((isset($getUser['roles']) && !is_null($getUser['roles'])) && ($getUser['roles'] == 3)){
                ?>
                    <ul class="list">
                        <li><a href="<?=base_url();?>"><i class="zmdi zmdi-home"></i><span>Beranda </span> </a> </li>
                        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-folder-outline"></i><span>Master Data</span></a>
                            <ul class="ml-menu">
                                <li><a href="<?=base_url();?>panel/pelabuhan">Pelabuhan</a></li>
                                <li><a href="<?=base_url();?>panel/armada">Armada</a></li>
                            </ul>
                        </li>
                        <li> <a href="<?=base_url();?>panel/photo"><i class="zmdi zmdi-collection-image-o"></i><span>Foto</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/video"><i class="zmdi zmdi-collection-video"></i><span>Video</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/file"><i class="zmdi zmdi-file"></i><span>Standarisasi</span> </a> </li>
                        <li> <a href="<?=base_url();?>backend/notifikasi"><i class="zmdi zmdi-notifications-active"></i><span>Notifikasi <span class="rounded-circle text-white bg-warning mr-1" style="padding: 1px 8px;"><?= count($array); ?></span></span> </a> </li>
                        <li> 
                            <a href="javascript:void(0)" class="menu-toggle"><i class="zmdi zmdi-file"></i><span>Laporan</span> </a> 
                            <ul class="ml-menu">
                                <li><a href="<?=base_url();?>backend/laporan">Pelabuhan</a></li>
                                <li><a href="<?=base_url();?>backend/laporan/armada">Armada</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url();?>panel/logout"><i class="zmdi zmdi-tag-close"></i><span>Logout</span></a></li>
                    </ul>
                <?php
            }else if((isset($getUser['roles']) && !is_null($getUser['roles'])) && ($getUser['roles'] == 4)){
                ?>
                    <ul class="list">
                        <li><a href="<?=base_url();?>"><i class="zmdi zmdi-home"></i><span>Beranda</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/photo"><i class="zmdi zmdi-collection-image-o"></i><span>Foto</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/video"><i class="zmdi zmdi-collection-video"></i><span>Video</span> </a> </li>
                        <li> <a href="<?=base_url();?>panel/file"><i class="zmdi zmdi-file"></i><span>Standarisasi</span> </a> </li>
                        <li><a href="<?=base_url();?>panel/pelabuhan"><i class="zmdi zmdi-navigation"></i> <span>Pelabuhan</span></a></li>
                        <li><a href="<?=base_url();?>panel/armada"><i class="zmdi zmdi-pin-drop"></i> <span>Armada</span></a></li>
                        <li><a href="<?=base_url();?>panel/logout"><i class="zmdi zmdi-tag-close"></i><span>Logout</span></a></li>
                    </ul>
                <?php
            }else if((isset($getUser['roles']) && !is_null($getUser['roles'])) && ($getUser['roles'] == 5)){
                ?>  
                    <ul class="list">
                        <li><a href="<?=base_url();?>"><i class="zmdi zmdi-home"></i><span>Beranda</span> </a> </li>
                        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-folder-outline"></i><span>Master Data</span></a>
                            <ul class="ml-menu">
                                <li><a href="<?=base_url();?>backend/slider">Slider</a></li>
                                <li><a href="<?=base_url();?>panel/roles">Roles</a></li>
                                <li><a href="<?=base_url();?>panel/users">User</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url();?>panel/logout"><i class="zmdi zmdi-tag-close"></i><span>Logout</span></a></li>
                    </ul>
                <?php
            }
        ?>
    </div>
    <!-- #Menu -->
</aside>

<section class="content ecommerce-page" style="margin: 0px 0px 15px 250px;">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><?php echo isset($title) ? $title : 'Dashboard'; ?>
                <small class="text-muted">Welcome to Asdp Application Version 1.0.2</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo isset($pageUrl) ? $pageUrl : base_url(); ?>"><i class="zmdi zmdi-home"></i> <?php echo isset($title) ? $title : 'Dashboard'; ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">