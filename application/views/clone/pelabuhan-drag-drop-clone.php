<script src="<?=base_url();?>assets/frontend/js/jquery.js"></script> 
<script src="<?=base_url();?>assets/frontend/js/konva.min.js"></script>
<script src="<?=base_url();?>assets/frontend/js/html2canvas.min.js"></script>
<?php include 'header.php'; ?>
<style type="text/css">

</style>
<style type="text/css">
    

    #ceks {
      width: 100%;
      height: 100%;
    }
  </style>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="card">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="btn-group">
              <a href="<?=base_url();?>backend/pelabuhan/show/show/<?= $record->id ?>/<?= $pelabuhanss->id ?>"  class="btn btn-primary btn-sm" style="color: #fff">Kembali</a>


            </div>
          </div>
          <div class="col-md-12 alertLah">

          </div>
        </div>

        <?php
        $img=check_img($pelabuhanss->foto_drag);
        // $image_info = getimagesize($img['path']);

        ?>
        <!-- <input type="hidden" name="imgWidth" value="<?= $image_info[0]; ?>">
        <input type="hidden" name="imgHeight" value="<?= $image_info[1]; ?>"> -->
        <div class="wrapper content">
          <div class="container-fluid">
            <h3><?= $record->nama_aspek;  ?> <?= $pelabuhanss->name; ?></h3>
            <div class="row">
              <div class="col-lg-7 mb-3 pr-0" id="ceks">
                <div style="background-image: url('<?=$img['path'];?>'); background-size: 100%;background-repeat: no-repeat;" id="containers" data-id="containers" class="img-fluid"></div>

              </div>
              <div class="col-lg-5 mb-3 p-0" style="background: #fffafa">
                <div class="bg-warning text-center py-1">
                  <h3 class="text-white font-weight-bold"><span style="font-weight: 1"><?= $record->nama_aspek;  ?></span></h3>
                </div>
                <div class="px-3 my-3">
                  <ul style="position: relative;left: -50px;font-size: 13px">
                    <?php
                    if(count($records) > 0){
                      $no = 0;
                      foreach ($records as $k => $value) {

                        if($value->name != ''){
                          $no++;

                          ?>              
                          <li style="list-style: none;font-size: 13px">
                            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block" style="padding-top: 2px;height: 50px;width: 350px;">
                              <div class="row">
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-lg-2">
                                      <span class="rounded-circle text-white bg-warning mr-1" style="padding: 3px 8px;"><?= $no; ?></span>
                                    </div>
                                    <div class="col-lg-9">
                                      <p class="text-warning">
                                        <?= $value->name; ?>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-2" style="text-align: right;">
                                  <span class="text-warning">Titik</span>
                                </div>
                                <div class="col-md-2">
                                  <span class="text-warning ">Unit Total</span>
                                  <br>
                                </div>
                              </div>
                            </a>
                            <ul class="ml-menu" style="display: none;font-size: 13px;position: relative;left: -5px">
                              <?php
                              if(count($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon'))){
                                foreach ($this->m_model->selectWhere2('trans_sub_id',$value->id,'status','Active','sub_aspeks_icon') as $keySubIco => $valueSubIco) {

                                  $cekReal = $this->m_model->getOne($valueSubIco->trans_icon_id,'icon');
                                  $selectOneTotal = $this->m_model->selectas4(
                                    'id_pelabuhan', $pelabuhanss->id, 
                                    'id_jenis_aspek', $record->id, 
                                    'id_sub_jenis_aspek', $value->id, 
                                    'icon_id', $cekReal['id'], 
                                    'trans_pelabuhans_hasil'
                                  );
                                  $imgs=check_img($cekReal['path_file']);
                                  ?>
                                  <?php 
                                  $coun = 0;
                                  $color = 'background-color: #f00;color: white;border-radius:20px;border:3px solid #f00 !important';
                                  if(count($this->m_model->selectas3('id_pelabuhan',$pelabuhanss->id,'id_jenis_aspek',$record->id,'icon_id',$cekReal['id'],'trans_pelabuhans_hasil')) > 0){
                                    $coun = count($this->m_model->selectas3('id_pelabuhan',$pelabuhanss->id,'id_jenis_aspek',$record->id,'icon_id',$cekReal['id'],'trans_pelabuhans_hasil'));
                                    $color = '-';
                                  }
                                  ?>
                                  <li style="font-size: 13px;padding-bottom:1px;width: 315px" id="drag-items">
                                    <div class="row">
                                      <div class="col-md-8">
                                        <img src="<?=$imgs['path'];?>" class="img-responsive drag" data-key="<?= $keySubIco + 1; ?>" data-id="<?= $cekReal['id']; ?>" data-aspek="<?= $value->name; ?>" data-name="<?= $cekReal['name']; ?>" data-sub="<?= $value->id; ?>" style="cursor: pointer; max-width: 50px; max-height:50px;width: 30px;padding-bottom: 1px;<?= $color; ?>" data-fancybox="images<?= $keySubIco + 1; ?>" href="<?=$imgs['path'];?>" draggable="true">&nbsp;
                                        <span style="font-size: 13px;">
                                          <?= $cekReal['name']; ?>
                                        </span>
                                      </div>
                                      <div class="col-md-2">
                                        <span class="" style="padding: 5px 8px;"><?= $coun; ?></span>    
                                      </div>
                                      <div class="col-md-2">
                                        <span class="" style="padding: 5px 8px;">
                                          <?php
                                          $nominal = 0;
                                          if(count($selectOneTotal) > 0){
                                            foreach ($selectOneTotal as $k => $valueS) {
                                              $nominal += (int)$valueS->total_icon;
                                            }
                                          }
                                          ?>
                                          <?= $nominal; ?></span> <p><?= $valueSubIco->units; ?></p>  
                                        </div>

                                      </div>
                                    </li>
                                    <hr style="width: 350px">
                                    <?php
                                  }
                                }
                                ?>
                              </ul>
                            </li>
                            <?php          
                          }
                        }
                      }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>

   
    
    var stage = new Konva.Stage({
      container: 'containers',
      width: 720,
      height: 1200
    });
    function fitStageIntoParentContainer() {
        var container = document.querySelector('#containers');

        // now we need to fit stage into parent
        var containerWidth = container.offsetWidth;
        // to do this we need to scale the stage
        var scale = containerWidth / 720;

        stage.width(720 * scale);
        stage.height(1200 * scale);
        stage.scale({ x: scale, y: scale });
        stage.draw();
      }

      fitStageIntoParentContainer();
      // adapt the stage on any window resize
      window.addEventListener('resize', fitStageIntoParentContainer);
    // stage.attrs.width = 100%;
    // console.log('stage',stage)
    // var canvas = new fabric.Canvas();
    // canvas.setDimensions({width: '100%', height: '100%'}, {cssOnly: true});
    var layer = new Konva.Layer();
    stage.add(layer);

      var widths = 20;
      $.ajax({
        url: '<?= site_url('backend/pelabuhan/getData'); ?>',
        type: 'post',
        data: {id_jenis_aspek: '<?= $record->id; ?>',id_pelabuhan:'<?= $pelabuhanss->id; ?>'},
        dataType: 'json',
        success:function(response){
          
          if(response.length > 0){
            $.each(response,function(k,v){
              Konva.Image.fromURL(v.url, function(yoda) {
                layer.add(yoda);

                var groups = new Konva.Group({
                  x: parseFloat(v.pointer_x),
                  y: parseFloat(v.pointer_y),
                  draggable:true
                });
                layer.add(groups);
                groups.add(yoda);
                addAnchor(groups, 0, 0, 'topLeft');
                addAnchor(groups, parseFloat(v.width), 0, 'topRight');
                addAnchor(groups, parseFloat(v.width), parseFloat(v.height), 'bottomRight');
                addAnchor(groups, 0, parseFloat(v.height), 'bottomLeft');

                if(v.width == null){
                  widths = 20
                }else{
                  widths = v.width;
                }

                if(v.height == null){
                  heights = v.width
                }else{
                  heights = v.height;
                }
                yoda.draggable(true);
                yoda.setAttrs({
                  width: widths,
                  height: heights,
                  id_pelabuhan: v.id_pelabuhan,
                  id_jenis_aspek: v.id_jenis_aspek,
                  id_sub_jenis_aspek:v.id_sub_jenis_aspek,
                  primary_key: v.primary_key,
                  pointer_x: v.pointer_x,
                  pointer_y: v.pointer_y,
                  total_icon: v.total_icon,
                  cek_target:'true'
                });
                  groups.draggable(true);
                console.log('groups',groups)
                
                layer.draw(); // It
              });

            });  
          }
        },
        error: function() {
          $('.alertLah').html(`
            <div class="alert alert-danger">
            Refresh Kembali!
            </div>
            `);
        }
      });

      
      // END ADD FOTO DARI KUMPULAN ARRAY
      
      // what is url of dragging element?
      var itemURL = '';
      var dataIds = '';
      var dataAspek = '';
      var dataName = '';
      $(document).on('dragstart','#drag-items', function(e) {
        itemURL = e.target.src;
          // 
          dataIds = e.target.dataset.id;
          $('input[name="icon_id"]').val(dataIds);
          $('input[name="url"]').val(e.target.src);
          $('input[name="aspek"]').val(e.target.dataset.aspek);
          $('input[name="id_sub_jenis_aspek"]').val(e.target.dataset.sub);
          $('input[name="nama"]').val(e.target.dataset.name);
        });

      var con = stage.container();
      con.addEventListener('dragover', function(e) {
        e.preventDefault(); 
      });

      arrayHasil = [];
      con.addEventListener('drop', function(e) {
        console.log('asd')
        e.preventDefault();
        stage.setPointersPositions(e);
        Konva.Image.fromURL(itemURL, function(image) {
          layer.add(image);
          var darthVaderGroup = new Konva.Group({
            x: 49,
            y: 49,
            draggable: true
          });
          layer.add(darthVaderGroup);
          darthVaderGroup.add(image);
          addAnchor(darthVaderGroup, 0, 0, 'topLeft');
          addAnchor(darthVaderGroup, 20, 0, 'topRight');
          addAnchor(darthVaderGroup, 20, 20, 'bottomRight');
          addAnchor(darthVaderGroup, 0, 20, 'bottomLeft');
          
          // image.position(stage.getPointerPosition());
          image.width(20);
          image.height(20);
          image.draggable(true);
          arrayHasil.push(stage.getPointerPosition());
          layer.draw();
          imagesArr = image;
          console.log('stage.getPointerPosition()',stage.getPointerPosition());
          imagesArr.setAttrs({
            icon_id:$('input[name="icon_id"]').val(),
            url:$('input[name="url"]').val(),
            aspek:$('input[name="aspek"]').val(),
            id_sub_jenis_aspek:$('input[name="id_sub_jenis_aspek"]').val(),
            nama:$('input[name="nama"]').val(),
            pointer_x:49,
            pointer_y:49,
          });
          console.log('darthVaderGroup',darthVaderGroup)
          $('input[name="pointer_x"]').val(stage.getPointerPosition().x);
          $('input[name="pointer_y"]').val(stage.getPointerPosition().y);
          $('input[name="primary_key"]').val(arrayHasil.length);
          $('.deletesData').hide();
          var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:20px;" class="imgResze"></center>`;
          $('.appendImg').html(imgApp);
        });
      });

      layer.addEventListener('dragmove', function(e) {
       
      });
      
      stage.on('click', function(e) {
        
        if(e.target.attrs.cek_target === 'true'){
           $.ajax({
            url: '<?= site_url('backend/pelabuhan/getDataOne/'); ?>',
            type: 'post',
            data: {id_jenis_aspek: e.target.attrs.id_jenis_aspek, id_pelabuhan:e.target.attrs.id_pelabuhan, primary_key:e.target.attrs.primary_key,pointer_x:e.target.attrs.pointer_x,pointer_y:e.target.attrs.pointer_y},
            dataType: 'json',
            success:function(response){
              console.log('e',e)
              if(e.target._lastPos == null){
                var points = {x:response.record.pointer_x,y:response.record.pointer_y};
              }else{
                var points = {x:e.target._lastPos.x,y:e.target._lastPos.y};
              }

              if(response){
                imagesArr = e.target;
                $("#add-panel").modal("show");
                $('.modal-backdrop').removeClass();


                $('input[name="id"]').val(response.record.id);
                $('input[name="id_pelabuhan"]').val(response.record.id_pelabuhan);
                $('input[name="id_jenis_aspek"]').val(response.record.id_jenis_aspek);
                $('input[name="id_sub_jenis_aspek"]').val(response.record.id_sub_jenis_aspek);
                $('input[name="icon_id"]').val(response.record.icon_id);
                $('input[name="url"]').val(response.record.url);
                $('input[name="pointer_x"]').val(points.x);
                $('input[name="pointer_y"]').val(points.y);
                $('input[name="primary_key"]').val(response.record.primary_key);
                $('input[name="kategori"]').val(response.record.kategori);
                $('input[name="nama"]').val(response.record.nama);
                $('input[name="aspek"]').val(response.record.aspek);
                $('input[name="nomor"]').val(response.record.nomor);
                $('input[name="kondisi"]').val(response.record.kondisi);
                $('input[name="posisi"]').val(response.record.posisi);
                $('input[name="tahun"]').val(response.record.tahun);
                $('input[name="total_icon"]').val(response.record.total_icon);
                $('input[name="height"]').val(e.target.attrs.height);
                $('input[name="width"]').val(e.target.attrs.width);
                var imgApp = `<center><img src="`+response.record.url+`" alt="" style="width:30px;" class="imgResze"></center>`;
                $('.appendImg').html(imgApp);
                $('.deletesData').show();
                if(response.record_foto){
                  $('.ReadshowImg').show();
                  $('.showImg').html('');
                  $.each(response.record_foto,function(k,v){
                    if(k == 0){
                      $('.showImg').append(`
                        <div class="carousel-item active">
                        <img src="<?php echo base_url(); ?>`+v.fileurl+`" class="img-fluid" style="width:100%;height:420px;" alt="">
                        </div>
                        `);
                    }else{
                      $('.showImg').append(`
                        <div class="carousel-item">
                        <img src="<?php echo base_url(); ?>`+v.fileurl+`" class="img-fluid" style="width:100%;height:420px;" alt="">
                        </div>
                        `);
                    }
                  });
                }
              }
            },
            error: function() {
              $('.alertLah').html(`
                <div class="alert alert-danger">
                Terjadi Kesalahan!
                </div>
                `);
            }
          });
        }else{
          console.log('e',e.target._lastPos.x,e.target._lastPos.y)
          var itemURL = $(e.target.attrs.image).attr('src');
          if(itemURL){
            stage.setPointersPositions(e);
            Konva.Image.fromURL(itemURL, function(image) {
              var points = {x:e.target.attrs.pointer_x,y:e.target.attrs.pointer_y};
              // layer.add(image);
              // image.position(stage.getPointerPosition());
              // image.width(20);
              // image.height(20);
              image.draggable(true);
              arrayHasil.push(points);
              layer.draw();
              imagesArr = image;
              $("#add-panel").modal("show");
              $('.modal-backdrop').removeClass();
              $('input[name="width"]').val(e.target.attrs.width);
              $('input[name="height"]').val(e.target.attrs.height);
              $('input[name="pointer_x"]').val(e.target._lastPos.x);
              $('input[name="pointer_y"]').val(e.target._lastPos.y);
              $('input[name="primary_key"]').val(arrayHasil.length);
              $('.deletesData').hide();

              $.ajax({
                url: '<?= site_url('backend/pelabuhan/getDataNumWithUrl'); ?>',
                type: 'post',
                data: {
                  id_jenis_aspek: '<?= $record->id; ?>',
                  id_pelabuhan:'<?= $pelabuhanss->id; ?>',
                  id_sub_jenis_aspek:e.target.attrs.id_sub_jenis_aspek,
                  icon_id:e.target.attrs.icon_id,
                },
                dataType: 'json',
                success:function(response){
                  // 
                  $('input[name="nomor"]').val(response.count);
                  $('input[name="icon_id"]').val(e.target.attrs.icon_id);
                  $('input[name="url"]').val(itemURL);
                  $('input[name="nama"]').val(e.target.attrs.nama);
                  $('input[name="aspek"]').val(e.target.attrs.aspek);
                  $('input[name="id_sub_jenis_aspek"]').val(e.target.attrs.id_sub_jenis_aspek);
                },
                error: function() {
                  
                }
              });
              
              var imgApp = `<center><img src="`+itemURL+`" alt="" style="width:30px;" class="imgResze"></center>`;
              $('.appendImg').html(imgApp);
            });
          }
        }
    });

    function update(activeAnchor) {
        var group = activeAnchor.getParent();

        var topLeft = group.get('.topLeft')[0];
        var topRight = group.get('.topRight')[0];
        var bottomRight = group.get('.bottomRight')[0];
        var bottomLeft = group.get('.bottomLeft')[0];
        var image = group.get('Image')[0];

        var anchorX = activeAnchor.getX();
        var anchorY = activeAnchor.getY();

        // update anchor positions
        switch (activeAnchor.getName()) {
          case 'topLeft':
            topRight.y(anchorY);
            bottomLeft.x(anchorX);
            break;
          case 'topRight':
            topLeft.y(anchorY);
            bottomRight.x(anchorX);
            break;
          case 'bottomRight':
            bottomLeft.y(anchorY);
            topRight.x(anchorX);
            break;
          case 'bottomLeft':
            bottomRight.y(anchorY);
            topLeft.x(anchorX);
            break;
        }

        image.position(topLeft.position());

        var width = topRight.getX() - topLeft.getX();
        var height = bottomLeft.getY() - topLeft.getY();
        if (width && height) {
          if(width >= 40){
            width = 40;
          }else if((width <= 40) && (width >= 30)){
            width = 30;
          }else if((width <= 30) && (width >= 20)){
            width = 20;
          }else if((width <= 20) && (width >= 10)){
            width = 10;
          }else if(width <= 10){
            width = 10;
          }

          image.width(width);
          image.height(width);
        }
    }
    function addAnchor(group, x, y, name) {
      var stage = group.getStage();
      var layer = group.getLayer();

      if(name==='bottomRight'){
        var anchor = new Konva.Circle({
          x: x,
          y: y,
          stroke: '#f0ad4e',
          fill: '#f0ad4e',
          strokeWidth: 1,
          radius: 5,
          name: name,
          draggable: true,
          dragOnTop: false
        });
      }else{
        var anchor = new Konva.Circle({
          x: x,
          y: y,
          // stroke: '#f0ad4e',
          // fill: '#f0ad4e',
          // strokeWidth: 1,
          // radius: 5,
          name: name,
          draggable: true,
          dragOnTop: false
        });
      }

      anchor.on('dragmove', function() {
        update(this);
        layer.draw();
      });
      anchor.on('mousedown touchstart', function() {
        group.draggable(false);
        this.moveToTop();
      });
      anchor.on('dragend', function() {
        group.draggable(true);
        layer.draw();
      });
      // add hover styling
      anchor.on('mouseover', function() {
        var layer = this.getLayer();
        document.body.style.cursor = 'pointer';
        this.strokeWidth(4);
        layer.draw();
      });
      anchor.on('mouseout', function() {
        var layer = this.getLayer();
        document.body.style.cursor = 'default';
        this.strokeWidth(2);
        layer.draw();
      });

      group.add(anchor);
    }
  </script>
  <div class="modal fade " id="add-panel" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-md" role="document" style="margin:140px auto;position: relative;left: -50px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Keterangan Data Jenis Aspek</h4>
        </div>
        <div class="modal-body">
          <form id="formModals" action="<?= base_url('backend/pelabuhan/store'); ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
              <input type="hidden" name="id">
              <input type="hidden" name="id_pelabuhan" value="<?= $pelabuhanss->id; ?>">
              <input type="hidden" name="id_jenis_aspek" value="<?= $record->id; ?>">
              <input type="hidden" name="id_sub_jenis_aspek">
              <input type="hidden" name="icon_id">
              <input type="hidden" name="url">
              <input type="hidden" name="pointer_x">
              <input type="hidden" name="pointer_y">
              <input type="hidden" name="primary_key">
              <input type="hidden" name="kategori" value="Pelabuhan">
              <input type="hidden" name="image">
              <input type="hidden" name="width">
              <input type="hidden" name="height">
              <textarea name="url_canvas" id="url_canvas" style="display: none;"></textarea>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Nama</label>*
                  <input name="nama" placeholder="Nama" type="text" class="form-control" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Aspek</label>*
                  <input name="aspek" placeholder="Aspek" type="text" class="form-control" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Nomor</label>*
                  <input name="nomor" placeholder="Nomor" type="text" class="form-control"  />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Kondisi</label>*
                  <input name="kondisi" placeholder="Kondisi" type="text" class="form-control"  />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Posisi</label>*
                  <input name="posisi" placeholder="Posisi" type="text" class="form-control"  />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Tahun Pengadaan</label>
                  <input name="tahun" placeholder="Tahun Pengadaan" type="text" class="form-control" />
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Deskripsi Total</label>
                  <div class="form-line">
                    <input name="total_icon" placeholder="Deskripsi Total" type="number" class="form-control" oninput="this.value= this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\.,/g, '$1')" />
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-line">
                  <div class="clearfix"></div>
                  <label>*click below to browse file</label>
                  <input name="icon[]" type="file" class="form-control" style="cursor: pointer;" accept="image/*" multiple="">
                </div>
              </div>

              <div class="card ReadshowImg" style="display: none;">
                <div id="demo2" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                        <!-- <ul class="carousel-indicators">
                          <li data-target="#demo2" data-slide-to="0" class=""></li>
                          <li data-target="#demo2" data-slide-to="1" class=""></li>
                          <li data-target="#demo2" data-slide-to="2" class=""></li>
                        </ul> -->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner showImg" style="width:100%;">


                        </div>

                        <!-- Controls -->
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo2" data-slide="prev" >
                          <span class="carousel-control-prev-icon" style="background-color: #1f91f3"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo2" data-slide="next">
                          <span class="carousel-control-next-icon" style="background-color: #1f91f3"></span>
                        </a>
                      </div>
                    </div><br>
                    <div class="col-md-12 pull-right" style="text-align: right;">
                      <button type="button" class="btn btn-default" id="cancel-button" >Cancel</button>
                      <button type="button" class="btn btn-danger deleteDatak deletesData" id="cancel-button" data-dismiss="modal" style="display: none">Delete</button>
                      <button type="button" class="btn btn-primary saveBtn" id="confirm-button">Save</button>
                      <button type="submit" class="btn btn-primary" id="bnke_btn" style="display: none;">Save</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">

              </div>
            </div>
          </div>

          <!-- EDIT MODAL -->


          <script type="text/javascript">
        //     $(document).on('click','.resizeNow',function(e){
        //       var width = $('input[name="width"]').val();
        //       var id = $('input[name="id"]').val();
        //       if(parseFloat(width) < 10){
        //         $('.label-error').text('Tidak Boleh Kurang Dari 10');
        //       }else if(parseFloat(width) > 20){
        //         $('.label-error').text('Tidak Boleh Lebih Dari 20');
        //       }else{
        //         if(width == ''){
        //           width = 20;
        //         }
        //           $('.label-error').text('');
        //           $('.imgResze').css("width",width);
        //           $('.imgResze').css("height",width);
        //           if(id){
        //         // 
        //         imagesArr.setAttrs({
        //           width : width,
        //           height : width,
        //           pointer_x : $('input[name="pointer_x"]').val(),
        //           pointer_y : $('input[name="pointer_y"]').val(),
        //         });
        //       }else{
                

        //         // Konva.Image.fromURL(itemURL, function(image) {
        //           // 
        //           layer.add(imagesArr);
        //           imagesArr.setAttrs({
        //             width : width,
        //             height : width,
        //             pointer_x : $('input[name="pointer_x"]').val(),
        //             pointer_y : $('input[name="pointer_y"]').val(),
        //           });
        //           // imagesArr.width(width);
        //           // imagesArr.height(width);
        //           // imagesArr.pointer_x($('input[name="pointer_x"]').val());
        //           // imagesArr.pointer_y($('input[name="pointer_y"]').val());
        //         // });
        //       }
        //       layer.draw();
        //     }
        // });

            $(document).on('click','#cancel-button',function(){
              window.location.reload();
            });

            $(document).on('click','.saveBtn',function(){
              var element = $('#containers');
              var getCanvas= '';
              html2canvas(element, {
               onrendered: function (canvas) {
                getCanvas = canvas;
                var imgageData = getCanvas.toDataURL("image/png");
                var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                  // 
                  $('#url_canvas').val(imgageData);
                }
              });
          // 
          $('.saveBtn').attr('disabled','disabled');
          $('.saveBtn').text('Wait for a while');
          var data = $('#formModals').serializeArray();
          // 
          time = 5;
          interval = setInterval(function(){
            time--;
            if(time == 0){
              clearInterval(interval);
              $( "#bnke_btn" ).trigger('click');
              
            }
          },1000);
        });

            $(document).on('click','.deleteDatak',function(){
              
              var element = $('#containers');
              var getCanvas= '';
              html2canvas(element, {
               onrendered: function (canvas) {
                getCanvas = canvas;
                var imgageData = getCanvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                  // 
                }
              });

              var data = $('#formModals').serializeArray();
          // 
          $.ajax({
            url: '<?= site_url('backend/pelabuhan/delete'); ?>',
            type: 'post',
            data: data,
            dataType: 'json',
            success:function(response){
              if(response.status == true){
                window.location.reload();
              }else{
                $('.alertLah').html(`
                  <div class="alert alert-danger">
                  Gagal Menghapus Data
                  </div>
                  `);
              }
              $("#add-panel").modal("hide");

            },
            error: function() {
              $('.alertLah').html(`
                <div class="alert alert-danger">
                Terjadi Kesalahan!
                </div>
                `);
              $("#add-panel").modal("hide");

            }
          });
        });

            $(document).on('click','#appendAdd',function(){
          // 
          // console.log($(this).data('id1'),$(this).data('cekreal'));
          var id = '.appendChildSub'+$(this).data('id1')+'-'+$(this).data('cekreal');
          // 
          $('.appendChildSub'+$(this).data('id1')+'-'+$(this).data('cekreal')).append(`
            <tr>
            <td colspan="" rowspan="" headers="">
            <input type="text" name="sub_text[]" id="subText" data-pelabuhanid="<?= $pelabuhanss->id; ?>" data-idaspek="<?= $record->id; ?>" data-icon="<?= $cekReal['id']; ?>" class="form-control" style="width: 150px;height:25px;border:1px solid black !important;font-size: 11px">
            </td>
            <td colspan="" rowspan="" headers="">
            <input type="text" name="sub_value[]" class="form-control" style="width: 100px;height:25px;border:1px solid black !important;font-size: 11px" id="subValue" data-pelabuhanid="<?= $pelabuhanss->id; ?>" data-idaspek="<?= $record->id; ?>" data-icon="<?= $cekReal['id']; ?>">
            </td>
            <td colspan="" rowspan="" headers="">
            <a href="javascript:void(0)" class="btn btn-sm btn-danger appendDelete" id="appendDelete" style="position:relative;top:-3px;height:25px;width: 15px">
            <i class="zmdi zmdi-close" style="position: relative;right: 5px;top: -2px;"></i>
            </a>
            </td>

            </tr>
            `);
        });

            $(document).on('click','#appendDelete',function(){
              $(this).closest('tr').remove();
            });

            $(document).on('keypress keydown keyup','#subText',function(){
              var data = {title:$(this).val(),id_pelabuhan:$(this).data('pelabuhanid'),idaspek:$(this).data('idaspek'),id_icon:$(this).data('icon')};

              $.ajax({
                url: '<?= site_url('backend/pelabuhan/subtitle'); ?>',
                type: 'post',
                data: data,
                dataType: 'json',
                success:function(response){
                  
                },
                error: function() {
                  
                }
              });

            });

            $(document).on('keypress keydown keyup','#subValue',function(){
              var data = {value:$(this).val(),id_pelabuhan:$(this).data('pelabuhanid'),idaspek:$(this).data('idaspek'),id_icon:$(this).data('icon')};

              $.ajax({
                url: '<?= site_url('backend/pelabuhan/subvalue'); ?>',
                type: 'post',
                data: data,
                dataType: 'json',
                success:function(response){
                  
                },
                error: function() {
                  
                }
              });
            });
          </script>
          <?php include 'footer.php'; ?>